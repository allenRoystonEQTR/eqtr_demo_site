# **Equator Front-end Component Library**

Welcome to the documentation for our front-end component library.

---

## **High-level concepts**
  
  
### *Importing atlas modules*
Use of this syntax reduce lookups and complexity within the transpiled output.

> import { `Module` } from `"shared/Module"`
  
  
  
  
### *Importing node_modules*
The difference in this syntax allows external modules to be quickly distinguished from our production code.

> import `animejs` from `"animejs"`
  
  
---

## **Dependencies**
- Your command line
- [Nodejs](https://nodejs.org/en/) (**v6.11.3+**)
- [NPM](https://www.npmjs.com/) (**v5.3.0+**)  

---

## **Getting started**
To build your source for __development__:

> `npm run dev`

To build your source for __production__:

> `npm run prod`

To build our __documentation__:

> `npm run docs`

To manually start the __watch task__:

> `npm run watch`

---

## **Testing**
TBD

---

## **Points of contact**
* [Graeme Clark](mailto:graeme.clark@eqtr.com)
* [Paciencia Canda](mailto:paciencia.canda@eqtr.com)
* [Lawrie Cape](mailto:lawrie.cape@eqtr.com)
* [Rob Foxx](mailto:rob.foxx@eqtr.com)

---

