// //==================================================================================================
// //	Dependencies
// //==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { BfinanceMultiItemCarousel } from "objects/BfinanceMultiItemCarousel";
import { toArray } from "helpers/shared/toArray";
import { getBreakpoint } from "helpers/shared/getBreakpoint";
import { Resize } from "objects/shared/Resize";

//==================================================================================================
//
//  Multi Item Carousel Implementations
//	What We Do and Latest component carousels
//
//==================================================================================================


//==================================================================================================
//	Private variables
//==================================================================================================
const delay = 400;
const triggerBreakpoint = 768;
const carousels = [];
let attached = false;
let pending = false;


//==================================================================================================
//	Private methods
//==================================================================================================
function createCarousels() {
    const collection = toArray(document.querySelectorAll("[data-carousel='what-we-do'], [data-carousel='latest']"));

    collection.forEach(function iterateCarousels(element) {

        const next = element.querySelector(".carousel__button--next");
        const prev = element.querySelector(".carousel__button--prev");
        const content = element.querySelector(".carousel__content");

        const carousel = new BfinanceMultiItemCarousel(content, next, prev);
        carousel.bindAll();
        carousels.push(carousel);

    });
}


function reInitialiseCarousels() {
    carousels.forEach(function iterateCarousels(carousel) {
        carousel.bindAll();
    });
}

function calculateWidths(breakpoint) {
    carousels.forEach(function iterateCarousels(carousel) {
        if (breakpoint === 600) {
            carousel.visible = 0.5;
        }
        else {
            carousel.visible = 0.75;
        }
        carousel.setWidths(true);
    });
}


//==================================================================================================
//	Resize listener
//==================================================================================================
function resizeListener() {
    if (pending === false) {

        pending = true;

        return setTimeout(function bounce() {
            const breakpoint = getBreakpoint();

            //  Max breakpoint is 768 (triggerBreakpoint)
            //  Check whether the screen size is smaller than max breakpoint and instantiate carousel if it is
            if (breakpoint < triggerBreakpoint && attached === false) {
                if (carousels.length > 0) {
                    reInitialiseCarousels();
                    calculateWidths(breakpoint);
                }
                else {
                    createCarousels();
                    calculateWidths(breakpoint);
                }

                attached = true;
            }

            else if (breakpoint >= triggerBreakpoint && attached === true) {
                if (carousels.length > 0) {
                    carousels.forEach(function iterateCarousels(carousel) {
                        carousel.reset();
                    });
                }

                attached = false;
            }

            //  Reset our pending state
            pending = false;


        }, delay);
    }
    else {
        return false;
    }
}

//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function multiItemCarouselDOMReady() {

    const initialBreakpoint = getBreakpoint();

    if (initialBreakpoint && initialBreakpoint < triggerBreakpoint) {
        createCarousels();
        calculateWidths(initialBreakpoint);
        attached = true;
    }

    const resize = new Resize(resizeListener);
    return resize;
});
