//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { BfinanceBasicCarouselJS } from "objects/BfinanceBasicCarouselJS";
import { CarouselPagination } from "objects/shared/CarouselPagination";
import { toArray } from "helpers/shared/toArray";



//==================================================================================================
//
//	Basic carousel
//
//==================================================================================================
new DOMReady(function testimoniallDOMReady() {

    const collection = toArray(document.querySelectorAll("[data-carousel='testimonial']"));

    collection.forEach(function iterateCarousels(element) {

        const next = element.querySelector(".carousel__button--next");
        const prev = element.querySelector(".carousel__button--prev");
        const content = element.querySelector(".carousel__content");

        const pagination = element.querySelector(".carousel-pagination");
        const template = element.querySelector(".carousel-pagination__button");

        const testimonial = new BfinanceBasicCarouselJS(content, next, prev);
        testimonial.bindAll();
        testimonial.pagination = new CarouselPagination(testimonial, pagination, template);

        return testimonial;

    });

});