//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { Tooltips } from "objects/shared/Tooltips";
import { toArray } from "helpers/shared/toArray";

//==================================================================================================
//	Private methods
//==================================================================================================
function listener(parent, containers, index, shown) {
    const exception = parent.querySelector("[data-control-panel='mail']");

    return function eventListener(event) {
        const isMobile = (exception.offsetWidth === 0);

        containers[index].setAttribute("data-show-divider", shown);

        //  Check for mobile
        if (isMobile && containers[index - 1] && containers[index - 1] === exception) {
            containers[index - 2].setAttribute("data-show-divider", shown);
        }

        else if (containers[index - 1]) {
            containers[index - 1].setAttribute("data-show-divider", shown);
        }
        return event.preventDefault();
    };
}



//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function tooltipReady() {

    //==================================================================================================
    //	Elements
    //==================================================================================================
    const parent = document.querySelector("[data-tooltips='control-panel']");


    //==================================================================================================
    //	Map to show previous elements
    //==================================================================================================
    if (parent) {

        const tooltips = new Tooltips(parent);
        tooltips.bind();

        const containers = toArray(parent.querySelectorAll("[data-show-divider]"));
        containers.forEach(function iterateControlPanelContainers(container, index) {
            container.addEventListener("mouseover", listener(parent, containers, index, false));
            container.addEventListener("mouseout", listener(parent, containers, index, true));
        });

    }

});