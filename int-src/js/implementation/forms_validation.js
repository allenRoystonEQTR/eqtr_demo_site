//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { AjaxService } from "objects/shared/AjaxService";
import { ValueValidation } from "objects/shared/ValueValidation";
import { FieldValidation } from "objects/shared/FieldValidation";
import { FieldValidationMessages } from "objects/shared/FieldValidationMessages";
import { FormValidation } from "objects/shared/FormValidation";
import { toArray } from "helpers/shared/toArray";

//==================================================================================================
//	Private methods
//==================================================================================================
function fulfill(form) {
    return function fullfillPost(response) {
        console.log(form);
        console.log(response);
    };
}

function reject(form) {
    return function rejectPost(reason) {
        console.log(form);
        console.log(reason);
    };
}

function asyncSubmit(service, form) {
    const post = service.post(fulfill(form), reject(form));
    return function submitForm() {
        const payload = form.json();
        if (payload) {
            return post(payload);
        }

        return false;
    };
}

function asyncClick(form) {
    const send = asyncSubmit(new AjaxService(form.element.getAttribute("action")), form);
    return function clickEvent(event) {

        form.check();
        if (form.ready) {
            send();
        }

        event.preventDefault();
    };
}

function click(form) {
    return function clickEvent(event) {

        form.check();
        if (form.ready) {
            form.submit();
        }

        event.preventDefault();
    };
}


//==================================================================================================
//
//	Apply our field validation
//
//==================================================================================================
new DOMReady(function formValidationReady() {

    const forms = toArray(document.querySelectorAll("[data-validate-form='true']"));
    let form;

    if (forms.length > 0) {
        forms.forEach(function iterateForms(element) {
            form = new FormValidation(element);
            form.setValidation(FieldValidation, ValueValidation, FieldValidationMessages);

            form.button.addEventListener("click", click(form));
        });
    }

    const asyncForms = toArray(document.querySelectorAll("[data-validate-async-form='true']"));
    let asyncForm;

    if (asyncForms.length > 0) {
        asyncForms.forEach(function iterateForms(element) {
            asyncForm = new FormValidation(element);
            asyncForm.setValidation(FieldValidation, ValueValidation, FieldValidationMessages);

            asyncForm.button.addEventListener("click", asyncClick(asyncForm));
        });
    }

});