// //==================================================================================================
// //	Dependencies
// //==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { AjaxService } from "objects/shared/AjaxService";
import { ValueValidation } from "objects/shared/ValueValidation";
import { FieldValidation } from "objects/shared/FieldValidation";
import { FieldValidationMessages } from "objects/shared/FieldValidationMessages";
import { FormValidation } from "objects/shared/FormValidation";
import { toArray } from "helpers/shared/toArray";

//==================================================================================================
//	Private methods
//==================================================================================================
function fulfill(results) {
    return function fullfillPost() {
        results["form"].setAttribute("data-newsletter-state", "inactive");
        results["fulfill"].setAttribute("data-newsletter-state", "active");

        setTimeout(function revertSatates() {
            results["form"].setAttribute("data-newsletter-state", "active");
            results["fulfill"].setAttribute("data-newsletter-state", "inactive");
        }, 5000);
    };
}

function reject(results) {
    return function rejectPost(reason) {
        let text = "";
        if (reason.responseText) {
            text = JSON.parse(reason.responseText).Message;
        }
        else {
            text = "Sorry, we encountered an issue. Please wait a moment and try again.";
        }
        results["form"].setAttribute("data-newsletter-state", "inactive");
        results["reject"].querySelector(".newsletter__text").innerHTML = text;
        results["reject"].setAttribute("data-newsletter-state", "active");

        setTimeout(function revertSatates() {
            results["form"].setAttribute("data-newsletter-state", "active");
            results["reject"].querySelector(".newsletter__text").innerHTML = "";
            results["reject"].setAttribute("data-newsletter-state", "inactive");
        }, 5000);
    };
}

function submit(form, results) {
    const payload = {
        NodeId: form.querySelector("[id^='newsletter-node']").value,
        EmailAddress: form.querySelector("[id^='newsletter-email']").value
    };

    const service = new AjaxService(form.getAttribute("data-form-action"));
    const post = service.post(fulfill(results), reject(results), "application/json");

    return post(JSON.stringify(payload));
}

function click(form, results) {
    return function clickHandler(event) {
        submit(form, results);
        event.preventDefault();
        return false;
    };
}



//==================================================================================================
//
//	Newsletter signup form submission
//
//==================================================================================================
new DOMReady(function newsletterSignupDOMReady() {

    const newsletterComponents = toArray(document.querySelectorAll("[data-component='newsletter']"));

    if (newsletterComponents.length > 0) {
        newsletterComponents.forEach(function iterateNewsletterComponents(newsletterComponent){
            const results = {};
            const elements = toArray(newsletterComponent.querySelectorAll("[data-newsletter]"));
            elements.forEach(function iterateElements(element){
                const attribute = element.getAttribute("data-newsletter");
                return results[attribute] = element;
            });

            const formElement = newsletterComponent.querySelector("form");
            formElement.addEventListener("submit", click(formElement, results));

            const form = results["form"].querySelector("[data-form^='newsletter']");

            const validation = new FormValidation(form);
            validation.setValidation(FieldValidation, ValueValidation, FieldValidationMessages);

            const button = form.querySelector("[data-form-submit]");
            button.addEventListener("click", click(form,results));

        });
    }
});
