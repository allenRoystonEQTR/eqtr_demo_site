//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//
//	Upload Dropdown
//
//==================================================================================================


//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function selectDropdownDOMReady() {
    const uploadItems = toArray(document.querySelectorAll("[data-form-field='file-upload']"));

    uploadItems.forEach(function iterateUploadItems(element) {
        const fileInput = element.querySelector("input[type='file']");
        const fileMessageDefault = element.querySelector(".form__label-default-text");
        const fileMessageUploaded = element.querySelector(".form__label-valid-text");
        const uploadedFileName = element.querySelector("[data-dynamic-value='filename']");


        fileInput.addEventListener('change', function() {

            if(fileInput.value !== null && fileInput.value !== "") {
                const diskPath = fileInput.value.split("\\");
                const lastItemIndex = diskPath.length-1;
                const lastItem = diskPath[lastItemIndex];

                uploadedFileName.innerHTML = lastItem;
                fileMessageUploaded.setAttribute('aria-hidden', 'false');
                fileMessageDefault.setAttribute('aria-hidden', 'true');
            }
            else {
                fileMessageUploaded.setAttribute('aria-hidden', 'true');
                fileMessageDefault.setAttribute('aria-hidden', 'false');
            }
        });

    });
});
