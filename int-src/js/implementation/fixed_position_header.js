import { DOMReady } from "objects/shared/DOMReady";
import { Scroll } from "objects/shared/Scroll";

//==================================================================================================
//
//	Hide region when user is scrolling
//
//==================================================================================================


//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function fixedPositionNav() {

    const region = document.querySelector(".region--mobile");
    const element = document.scrollingElement | document;


    if (region) {

        let hidden = element.scrollTop > 0;
        region.setAttribute("aria-hidden", hidden);

        let pending = false;

        return new Scroll(function toggleMobileRegionSelector() {

            if (pending === false) {
                pending = true;

                return setTimeout(function bounceScroll() {

                    if (element.scrollTop > 0 && hidden === false) {
                        hidden = true;
                        region.setAttribute("aria-hidden", true);
                    }

                    else if (element.scrollTop === 0 && hidden === true) {
                        hidden = false;
                        region.setAttribute("aria-hidden", false);
                    }

                    return pending = false;

                }, 100);

            }

            return false;

        });

    }

    return true;

});