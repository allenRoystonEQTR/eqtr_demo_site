//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { Resize } from "objects/shared/Resize";


//==================================================================================================
//
//	Resize search bar
//
//==================================================================================================

//==================================================================================================
//	Constants
//==================================================================================================



//==================================================================================================
//	Variables & object instances
//==================================================================================================
const delay = 500;
const controlPanel = document.querySelector('[data-tooltips="control-panel"]');
const searchTooltip = controlPanel.querySelector('[data-control-panel="search"] [role="tooltip"]');
let pending = false;

//==================================================================================================
//	Resize handler
//==================================================================================================
function resizeListener() {
    if (pending === false) {
        pending = true;
        return setTimeout(function setNewSearchBarSize() {
            setSearchBarSize();
            //  Reset our pending state
            pending = false;

        }, delay);
    }
    else {
        return false;
    }
}

function setSearchBarSize() {
    if(controlPanel && searchTooltip){
        searchTooltip.style.width = controlPanel.offsetWidth + 'px';
    }
}


//==================================================================================================
//	Set initial size of search bar on DOM ready
//==================================================================================================
new DOMReady(function searchBarDomReady(){
    setSearchBarSize();
    //  Set up our monitoring
    const resize = new Resize(resizeListener);
    return resize;
});