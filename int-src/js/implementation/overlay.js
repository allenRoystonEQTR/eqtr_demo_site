//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { Overlay } from "objects/shared/Overlay";
import { toArray } from "helpers/shared/toArray";
import { dataStore } from "helpers/globals";


//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function overlayDOMReady() {

    const buttons = toArray(document.querySelectorAll("[data-overlay]"));

    if (buttons.length > 0) {

        buttons.forEach(function iterateButtons(button) {
            const overlay = new Overlay(button);
            overlay.bind();
            dataStore.set(overlay, button);
            return overlay;
        });

    }

});