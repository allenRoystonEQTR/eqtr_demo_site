import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";
import { DropdownNavigation } from "objects/shared/DropdownNavigation";

// ==================================================================================================
//
//  Region selector
//
// ==================================================================================================

//==================================================================================================
//	Event handler
//==================================================================================================
function click(indicator, text) {
    return function clickHandler() {
        indicator.innerHTML = text.innerHTML;
    };
}

function triggerClick(toggleView) {
    return function triggerClickHandler() {
        if (toggleView.hidden === false) {
            toggleView.hidden = true;
        }
        else {
            toggleView.hidden = false;
        }
    };
}

//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function overlayDOMReady() {

    const containers = toArray(document.querySelectorAll("[data-navigation='nav-region'], [data-navigation='nav-region-mobile']"));

    //  Desktop and mobile close action
    containers.forEach(function iterateDropdownCollection(container) {
        //  Navigation container
        const buttons = toArray(container.querySelectorAll("[data-navigation-item]:not([data-navigation-trigger])"));
        const indicator = container.querySelector("[data-region-indicator]");

        if (buttons.length > 0) {
            buttons.forEach(function iterateRegionButtons(button) {
                const text = button.querySelector("[data-region]");
                button.addEventListener("click", click(indicator, text));
            });
        }

        //  Instantiate dropdown nav
        const dropdown = new DropdownNavigation(container);
        dropdown.bind();

        //  Close action
        const trigger = container.querySelector("[data-navigation-trigger]");
        trigger.addEventListener("click", triggerClick(dropdown));
        return dropdown;
    });

});