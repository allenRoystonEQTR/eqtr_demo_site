//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";

//==================================================================================================
//
//	GDPR Checkbox
//
//==================================================================================================


//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function gdprCheckboxDOMReady() {
    const gdprFields = toArray(document.querySelectorAll("[data-form-field='gdpr']"));

    if (gdprFields.length > 0) {
        gdprFields.forEach(function iterateGdprFields(element) {
            const gdprCheckbox = element.querySelector("[data-input='gdpr-checkbox']");
            const gdprRadios = element.querySelectorAll("[data-input='gdpr-radio']");

            gdprCheckbox.addEventListener("click", function toggleGdprRadio(event) {
                const value = event.target.checked;

                gdprRadios.forEach(function setRadioValues(radio) {
                    if ((value && radio.value === 'true') || ( !value && radio.value === 'false')) {
                        radio.checked = true;
                    }
                    else {
                        radio.checked = false;
                    }
                });
            });
        });
    }
});
