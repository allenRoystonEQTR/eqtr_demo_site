import { DOMReady } from "objects/shared/DOMReady";
import { BfinanceMobileNavigation } from "objects/BfinanceMobileNavigation";
import { Overlay } from "objects/shared/Overlay";

//==================================================================================================
//
//	Mobile navigation
//
//==================================================================================================

//==================================================================================================
//	DOM ready
//==================================================================================================
new DOMReady(function mobileNavReady() {

    const container = document.querySelector("[data-carousel='mobile-navigation']");
    const trigger = document.querySelector("[data-control-panel='burger']");
    const button = trigger.querySelector("[aria-controls]");

    if (container) {

        const tray = new Overlay(button);
        button.addEventListener("click", function openBurger() {
            //  Toggle our hidden state
            tray.hidden = !tray.hidden;
            trigger.setAttribute("data-navigation-hidden", tray.hidden);
        });

        //  Create our mobile navigation instance
        const carousel = new BfinanceMobileNavigation(container);
              carousel.bind();

    }

});