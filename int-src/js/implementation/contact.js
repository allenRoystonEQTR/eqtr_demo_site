//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { Tabs } from "objects/shared/Tabs";
import { BfinanceBasicCarouselJS } from "objects/BfinanceBasicCarouselJS";
import { CarouselPagination } from "objects/shared/CarouselPagination";
import { toArray } from "helpers/shared/toArray";
import { dataStore } from "helpers/globals";


//==================================================================================================
//	Private methods
//==================================================================================================
//  Our button click event handler
function tabsEvent(tabs) {
    return function tabsEventHandler() {
        const button = this;
        tabs.toggle(button);
    };
}


//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function contactDOMReady() {

    const contact = document.querySelector("[data-component='contact']");

    if (contact) {

        //  Configure tabs
        //==================================================================================================
        const buttons = toArray(contact.querySelectorAll("[aria-controls]"));
        const tabs = new Tabs(buttons);

        //TODO: Add logic to show region - requires pagesettings?
        const open = document.getElementById(contact.getAttribute("data-contact-default"));
        tabs.toggle(open);

        //  Iterate across our buttons, binding the event handler
        buttons.forEach(function iterateButtons(button) {
            button.addEventListener("click", tabsEvent(tabs));
        });

        //  Assign our tabs instance to the contact element
        dataStore.set(tabs, contact);


        //  Set up our carousel for mobile
        //==================================================================================================
        const mobile = toArray(contact.querySelectorAll("[data-carousel='contact']"));
        const pagination = toArray(contact.querySelectorAll("[data-carousel-pagination='contact']"));

        mobile.forEach(function iterateContactCarousels(element, index) {
            const template = pagination[index].querySelector(".carousel-pagination__button");

            const carousel = new BfinanceBasicCarouselJS(element);
            carousel.bindAll();
            carousel.pagination = new CarouselPagination(carousel, pagination[index], template);
        });

    }

});