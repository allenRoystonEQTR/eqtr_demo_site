//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { Resize } from "objects/shared/Resize";
import { LazyLoad } from "objects/shared/LazyLoad";
import { getBreakpoint } from "helpers/shared/getBreakpoint";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//
//	Responsively lazy load images
//
//==================================================================================================

//==================================================================================================
//	Constants
//==================================================================================================



//==================================================================================================
//	Variables & object instances
//==================================================================================================
const load = new LazyLoad();
const delay = 2000;
const multiplier = 1;
const initial = loadImages({});
const update = loadImages({ "image": "replaceImage" });
const initialBreakpoint = getBreakpoint();
let currentBreakpoint = (initialBreakpoint < 600) ? 600 : initialBreakpoint;
let pending = false;


//==================================================================================================
//	Load images
//==================================================================================================
function loadImages(callbackMap) {
    return function loadImagesLogic() {
        let property, collection;

        for (property in load.selectors) {
            if (load.selectors.hasOwnProperty(property)) {
                //  Retrieve our elements
                collection = toArray(document.querySelectorAll("["+ load.selectors[property] +"]"));

                //  Iterate across the collection & replace images as needed
                /*  eslint-disable no-loop-func */
                collection.forEach(function iterateCollection(element) {
                    load.src = element.getAttribute(load.selectors[property]) +"?mode=max&animationprocessmode=first&width="+ currentBreakpoint;
                    //  Check if we are remapping our callbacks to replace an image, instead of appending it.
                    if (callbackMap[property]) {
                        load[callbackMap[property]](element);
                    }
                    else {
                        load[property](element);
                    }
                });
                /*  eslint-enable no-loop-func */

            }
        }

    };
}


//==================================================================================================
//	Resize handler
//==================================================================================================
function resizeListener() {
    if (pending === false) {
        pending = true;
        return setTimeout(function bounce() {
            const breakpoint = getBreakpoint();

            //  Check whether the change to screen size is sufficient enough to warrant an image reload
            //TODO:  We should consider a logarithmic scale for this calculation
            if (breakpoint > currentBreakpoint * multiplier) {
                currentBreakpoint = breakpoint;
                update();
            }

            //  Reset our pending state
            pending = false;


        }, delay);
    }
    else {
        return false;
    }
}



//==================================================================================================
//	Load our intial images on DOM ready
//==================================================================================================
new DOMReady(function loadPageImages() {
    //  Load our initial images
    initial();

    //  Set up our monitoring
    const resize = new Resize(resizeListener);
    return resize;
});