import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";
import { VideoPlayer } from "objects/shared/VideoPlayer";
import { DataStore } from "objects/shared/DataStore";


//==================================================================================================
//
//	HTML 5 video player
//
//==================================================================================================
new DOMReady(function videoPlayerDOMReady() {

    const videos = toArray(document.querySelectorAll("[data-video='default']"));

    if (videos.length > 0) {

        //  Create an instance of our data store for binding
        const data = new DataStore();

        //  Iterate across our video elements
        videos.forEach(function iterateVideos(video) {

            //  Instantiate our player
            const player = new VideoPlayer(video);

            //  Configure our options
            player.disableContextMenu();
            player.poster = "/assets/images/raster/demo/team-member.jpg?mode=max&animationprocessmode=first&width=1800";
            player.bind("seeked,ended");

            //  Attach play/pause handler to our controls element
            const button = video.querySelector("[data-video-control='play-pause']");
            button.addEventListener("click", function playPauseButton(event) {

                //  Correctly toggle the video state
                if (player.playing === true) {
                    player.playing = false;
                }
                else {
                    player.playing = true;
                }

                event.stopPropagation();
            });

            //  Set our data on the element
            data.set(player, video);

            return player;
        });

    }

});