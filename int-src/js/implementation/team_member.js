//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { Overlay } from "objects/shared/Overlay";
import { BfinanceTeamCarouselJS } from "objects/BfinanceTeamCarouselJS";
import { toArray } from "helpers/shared/toArray";



//==================================================================================================
//
//	Basic carousel
//
//==================================================================================================
new DOMReady(function testimoniallDOMReady() {
    const collection = toArray(document.querySelectorAll("[data-team-carousel]"));

    collection.forEach(function iterateCarousels(element) {
        const attribute = element.getAttribute("data-team-carousel");

        const next = element.querySelector(".carousel__button--next");
        const prev = element.querySelector(".carousel__button--prev");
        const content = element.querySelector(".carousel__content");
        const carousel = new BfinanceTeamCarouselJS(content, next, prev);

        const buttons = toArray(document.querySelectorAll("[data-team-overlay='"+ attribute +"']"));

            if (buttons.length > 0) {

                buttons.forEach(function iterateButtons(button) {
                    const overlay = new Overlay(button, function teamButtonCallback(overlay){
                        const attribute = overlay.button.getAttribute("data-team-member");
                        const animation = carousel.disableJSAnimation();
                        carousel.position = carousel.map[attribute];

                        setTimeout(function enableJSAnimation() {
                            carousel.enableJSAnimation(animation);
                        }, 0);

                    });
                    overlay.bind();
                    return overlay;
                });

            }

        return carousel;
    });

});