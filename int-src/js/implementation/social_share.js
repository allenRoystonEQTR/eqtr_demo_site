import { SocialShare } from "objects/shared/SocialShare";
import { toArray } from "helpers/shared/toArray";

const shareLinks = toArray(document.querySelectorAll("[data-link='share']"));

shareLinks.forEach.call(shareLinks, function shareLinksIterator(element) {
	return new SocialShare(element);
});