import { DOMReady } from "objects/shared/DOMReady";
import { Cookie } from "objects/shared/Cookie";

//==================================================================================================
//
//	Cookie warning message
//
//==================================================================================================

//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function cookieDOMReady() {

    const element = document.querySelector("[data-component='cookie']");
    const footer = document.querySelector("footer.footer");

    if (element) {

        const cookies = new Cookie();

        if (cookies.CookiesAccepted === undefined) {
            element.setAttribute("aria-hidden", false);
            footer.classList.add('footer--with-cookie-bar');

            const close = element.querySelector("[data-cookie-warning='close']");
            close.addEventListener("click", function closeCookieWarning() {
                cookies.add("CookiesAccepted", true);
                element.setAttribute("aria-hidden", true);
                footer.classList.remove('footer--with-cookie-bar');
            });
        }

    }

});