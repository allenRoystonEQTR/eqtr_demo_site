//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";
import { GoogleMapsView } from "objects/shared/GoogleMapsView";
import { GoogleMapsMarker } from "objects/shared/GoogleMapsMarker";

//==================================================================================================
//
//	Detailed location Maps implementation
//
//==================================================================================================

//==================================================================================================
//	Private variables
//==================================================================================================


//==================================================================================================
//	Private methods
//==================================================================================================
function createMap(element, coordinates) {
    const map = new GoogleMapsView(element);
    map.center =  map.coordinatesToObject(coordinates);
    map.update();

    const icon = {
        url: '/assets/images/raster/map-pin.png',
        // This marker is 23 pixels wide by 36 pixels high.
        size: new google.maps.Size(23, 36),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0)
    };

    const options = {
        map: map,
        position: coordinates,
        icon: icon
    };

    const marker = new GoogleMapsMarker(options);
}

function setDirectionsLink(element, coordinates) {
    const basePath = "https://maps.google.com/maps/dir/?api=1";
    const directionsURL = basePath + "&destination=" + coordinates;

    const directionsLink = element.querySelector("[data-link='directions']");
    directionsLink.setAttribute("href", directionsURL);
}


//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function detailedLocationDOMReady() {
    const collection = toArray(document.querySelectorAll("[data-component='detailed-location']"));

    collection.forEach(function iterateComponents(detailedLocationComponent) {
        const componentMap = detailedLocationComponent.querySelector("[data-map]");

        const coordinates = componentMap.getAttribute("data-map-lat-lng");

        createMap(componentMap, coordinates);
        setDirectionsLink(detailedLocationComponent, coordinates);
    });
});