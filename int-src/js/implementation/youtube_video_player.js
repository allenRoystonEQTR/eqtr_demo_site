import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";
import { YouTubePlayer } from "objects/shared/YouTubePlayer";
import { DataStore } from "objects/shared/DataStore";

//==================================================================================================
//
//	HTML 5 video player
//
//==================================================================================================

//==================================================================================================
//	Private methods
//==================================================================================================
function listener(player) {
    return function controlsListener(event) {
        //  Correctly toggle the video state
        if (player.playing === false) {
            player.play();
        }
        else {
            player.pause();
        }

        event.stopPropagation();
    };
}


//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function videoPlayerDOMReady() {

    const videos = toArray(document.querySelectorAll("[data-video='youtube']"));

    if (videos.length > 0) {

        //  Create an instance of our data store for binding
        const data = new DataStore();

        //  Iterate across our video elements
        videos.forEach(function iterateVideos(video) {

            //  Instantiate our player
            const player = new YouTubePlayer(video, function playerReadyCallback(video) {
                video.bind("ready,ended");
            });

            //  Attach play/pause handler to our controls element
            const button = video.querySelector("[data-video-control='play-pause']");
            button.addEventListener("click", listener(player));

            //  Set our data on the element
            data.set(player, video);
            return player;
        });

    }

});