//==================================================================================================
//
//  Initalise our page header navigation
//
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { DropdownNavigation } from "objects/shared/DropdownNavigation";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function navDOMReady() {

    const collection = toArray(document.querySelectorAll("[data-navigation]:not([data-navigation='nav-region']):not([data-navigation='nav-region-mobile'])"));
    collection.forEach(function iterateDropdownCollection(dropdown) {
        return new DropdownNavigation(dropdown).bind();
    });

});