//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";
import { DataStore } from "objects/shared/DataStore";

//==================================================================================================
//
//	Tag Filter
//
//==================================================================================================


//==================================================================================
//	Private variables
//==================================================================================

//  Create instance of store
const store = new DataStore();

//==================================================================================================
//	Private methods
//==================================================================================================
function refreshSelect(select) {
    const option = document.createElement("option");
    option.addClass("form__option");
    option.value = "";
    option.hidden = true;
    option.textContent = "Please select";

    select.empty();
    select.appendChild(option);
    return select;
}

// Create Option
function createOption(text, value) {
    const option = document.createElement("option");
    option.addClass("form__option");
    option.textContent = text;
    option.value = value;

    return option;
}

// Parse Data
function parseData(select, currentTags) {

    //  Refresh our select
    refreshSelect(select);

    currentTags.forEach(function iterateTags(tag) {
        const text = tag.Name;
        const value = tag.Id;

        const option = createOption(text, value);

        store.set(tag.Tags, option);
        return select.appendChild(option);
    });
}

// Activate SubTag select
function activateSubTagDropdown(subtagSelectItem) {
    const label = subtagSelectItem.parentNode.querySelector("[data-form-label='select']");
    let labelText = store.get(label);
    if(!labelText){
        store.set(label.innerHTML, label);
        labelText = label.innerHTML;
    }
    subtagSelectItem.removeAttribute("disabled");
    subtagSelectItem.setAttribute("data-select-active", "true");
    subtagSelectItem.parentNode.removeClass("form__field--inactive");
    label.innerHTML = labelText;
    refreshSelect(subtagSelectItem);
}

function submit(form) {
    form.submit();
}

function click(form) {
    return function clickHandler(event) {
        event.preventDefault();
        submit(form);
    };
}


//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function tagFilterDOMReady() {
    const tagFilters = toArray(document.querySelectorAll("[data-component='tag-filter']"));
    const tags = window.tags;

    if (tagFilters.length > 0) {
        // Iterate tag filter elements
        tagFilters.forEach(function iterateTagFilters(element) {
            const form = element.querySelector("[data-form='article-filter']");

            const button = element.querySelector("[data-form-submit='article-filter']");
            button.addEventListener("click", click(form));

            if (tags && tags.length > 0) {
                const tagSelectItem = element.querySelector("[data-filter='tag']");
                const subtagSelectItem = element.querySelector("[data-filter='subtag']");

                // Delegate event (on change)
                tagSelectItem.delegate("change", "*", function changeSubtag(){
                    const currentTags = store.get(tagSelectItem.querySelector(".form__option:checked"));
                    activateSubTagDropdown(subtagSelectItem);
                    parseData(subtagSelectItem, currentTags);
                });
                parseData(tagSelectItem, tags);
            }
            else {
                element.setAttribute("aria-hidden", "true");
            }
        });
    }
});
