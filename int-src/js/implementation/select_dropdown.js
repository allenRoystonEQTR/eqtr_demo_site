//==================================================================================================
//	Dependencies
//==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { toArray } from "helpers/shared/toArray";
import { keycode } from "helpers/shared/keycode";

//==================================================================================================
//
//	Select Dropdown
//
//==================================================================================================


//==================================================================================
//	Private variables
//==================================================================================
let focused = false;

//==================================================================================================
//	Private methods
//==================================================================================================
function changeState(dropdown) {
    if (dropdown.getAttribute("data-select-open") === "false") {
        dropdown.setAttribute("data-select-open", "true");
    }
    else {
        dropdown.setAttribute("data-select-open", "false");
    }
}

function reset(dropdown) {
    dropdown.setAttribute("data-select-open", "false");
}


//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function selectDropdownDOMReady() {
    const selectFields = toArray(document.querySelectorAll("[data-form-field='select']"));

    selectFields.forEach(function iterateSelectDropdowns(element) {
        const selectItem = element.querySelector('.form__select');
        selectItem.delegate("click", "*", function changeSelectDropdownState() {
            changeState(selectItem);
        });
        selectItem.delegate("blur", "*", function changeSelectDropdownState() {
            reset(selectItem);
        });
        selectItem.delegate("focus", "*", function changeSelectDropdownState() {
            focused = true;
        });
        selectItem.delegate("keyup", "*", function changeSelectDropdownState() {
            if(focused && keycode === keycode.enter) {
                changeState(selectItem);
            }
        });
        selectItem.delegate("change", "*", function changeLabelValue(){
            element.querySelector("[data-form-label='select']").innerHTML = selectItem.options[selectItem.selectedIndex].textContent;
        });
    });

});
