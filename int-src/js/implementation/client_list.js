// //==================================================================================================
// //	Dependencies
// //==================================================================================================
import { DOMReady } from "objects/shared/DOMReady";
import { BfinanceMultiItemCarousel } from "objects/BfinanceMultiItemCarousel";
import { toArray } from "helpers/shared/toArray";
import { getBreakpoint } from "helpers/shared/getBreakpoint";
import { Resize } from "objects/shared/Resize";

//==================================================================================================
//
//	What We Do component carousel
//
//==================================================================================================


//==================================================================================================
//	Private variables
//==================================================================================================
const delay = 300;
const triggerBreakpoint = 768;
const carousels = [];
let attached = false;
let pending = false;


//==================================================================================================
//	Private methods
//==================================================================================================
function createCarousels() {
    const collection = toArray(document.querySelectorAll("[data-carousel='client-list']"));
    // const collection = toArray(document.querySelectorAll("[data-carousel='recent-article']"));
    collection.forEach(function iterateCarousels(element) {
        const next = element.querySelector(".carousel__button--next");
        const prev = element.querySelector(".carousel__button--prev");
        const content = element.querySelector(".carousel__content");

        const carousel = new BfinanceMultiItemCarousel(content, next, prev);
        carousel.bindAll();
        carousels.push(carousel);
    });
}

function reInitialiseCarousels() {
    carousels.forEach(function iterateCarousels(carousel) {
        carousel.bindAll();
    });
}

function setCarousel() {
    carousels.forEach(function iterateCarousels(carousel) {
        carousel.visible = 0.4;
        carousel.increment = 2;
        carousel.setWidths();
    });
}


//==================================================================================================
//	Resize listener
//==================================================================================================
function resizeListener() {
    if (pending === false) {

        pending = true;

        return setTimeout(function bounce() {
            const breakpoint = getBreakpoint();

            //  Max breakpoint is 768 (triggerBreakpoint)
            //  Is current breakpoint less than or equal to 768

            //  Check whether the screen size is smaller than breakpoint and instantiate carousel if it is
            if (breakpoint < triggerBreakpoint && attached === false) {
                if (carousels.length > 0) {
                    reInitialiseCarousels();
                    setCarousel();
                }
                else {
                    createCarousels();
                    setCarousel();
                }

                attached = true;
            }

            else if (breakpoint >= triggerBreakpoint && attached === true) {
                if (carousels.length > 0) {
                    carousels.forEach(function iterateCarousels(carousel) {
                        carousel.reset();
                    });
                }

                attached = false;
            }

            //  Reset our pending state
            pending = false;


        }, delay);
    }
    else {
        return false;
    }
}

//==================================================================================================
//	DOM Ready
//==================================================================================================
new DOMReady(function clientListDOMReady() {

    const initialBreakpoint = getBreakpoint();

    if (initialBreakpoint && initialBreakpoint < triggerBreakpoint) {
        createCarousels();
        setCarousel();
        attached = true;
    }

    const resize = new Resize(resizeListener);
    return resize;
});
