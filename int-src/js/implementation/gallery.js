import { DOMReady } from "objects/shared/DOMReady";
import { BfinanceBasicCarouselJS } from "objects/BfinanceBasicCarouselJS";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//
//	Gallery
//
//==================================================================================================

//==================================================================================================
//	Private methods
//==================================================================================================
function callback(textElement) {
    return function carouselCallback(carousel) {
        return textElement.innerHTML = carousel.position + 1;
    };
}

function swipe(imageCarousel, textCarousel) {
    return function swipeEvent(event) {
        if (event.deltaX > 0 && imageCarousel.position > 0) {
            imageCarousel.position = imageCarousel.position - 1;
            textCarousel.position = textCarousel.position - 1;
        }
        if (event.deltaX < 0 && imageCarousel.position < imageCarousel.last) {
            imageCarousel.position = imageCarousel.position + 1;
            textCarousel.position = textCarousel.position + 1;
        }
    };
}


//==================================================================================================
//	DOM ready logic
//==================================================================================================
new DOMReady(function galleryDOMReady() {

    const collection = toArray(document.querySelectorAll(".gallery"));

    collection.forEach(function iterateCarousels(element) {

        //  Position indicators
        const countContainer =  element.querySelector("[data-carousel-count='container']");
        const countPositionEl = countContainer.querySelector("[data-carousel-count='position']");
        const countTotalEl = countContainer.querySelector("[data-carousel-count='total']");

        //  Buttons
        const next = element.querySelector(".gallery__button--next");
        const prev = element.querySelector(".gallery__button--prev");

        //  Text carousel
        const text = element.querySelector("[data-carousel='gallery-text']");
        const textItems = text.querySelector(".carousel__content");

        const textCarousel = new BfinanceBasicCarouselJS(textItems, next, prev, callback(countPositionEl));
        textCarousel.bind();

        //  Image carousel
        const images = element.querySelector("[data-carousel='gallery-images']");
        const imageItems = images.querySelector(".carousel__content");

        const imageCarousel = new BfinanceBasicCarouselJS(imageItems, next, prev);
        imageCarousel.bind();

        //  Touch handler
        const touch = new Hammer(element);
        touch.on("swipe", swipe(imageCarousel, textCarousel));

        //  Set our initial counts & position
        if (textCarousel.items.length > 1) {
            countContainer.setAttribute("aria-hidden", false);
            countPositionEl.innerHTML = textCarousel.position + 1;
            countTotalEl.innerHTML = textCarousel.items.length;
        }

    });

});