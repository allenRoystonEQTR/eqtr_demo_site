/**
 * @module module:not
 */


/**
 * Inverts a boolean
 * @function
 * @param {Number} value - Value to be inverted
 * @returns {Boolean}
 * @example
 * let invalid = false;
 * let valid = not(invalid);
 */
export const not = function(value) {
    return value === true;
};