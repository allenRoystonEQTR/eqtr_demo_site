//==================================================================================================
//
//	Check if a value is a number
//
//==================================================================================================
/**
 * @module module:isNumber
 */

/**
 * Check if a value is of a primitive type Number
 * @function
 * @param {*} number - Value to be tested
 * @returns {Boolean}
 * @example
 * let example = 1;
 * if (isNumber(example)) {
 *  //  Do something
 * }
 * else {
 *  //  Do something else
 * }
 */
export const isNumber = function(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
};