/**
 * @module module:typeOf
 */

/**
 * Get the property typeof objects by extracting from their .toString() type
 * eg. [object Array]
 * @see https://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator/
 * @function
 * @param {*} obj - Object to be type tested
 * @returns {String}
 * @example
 * if (typeOf("hello") === "string") {
 *  //  Do something
 * }
 */
export const typeOf = function(obj) {
    return {}.toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};