import { toBoolean } from "helpers/shared/toBoolean";

//==================================================================================================
//
//	Show/Hide an element by toggling it's aria-hidden attribute
//
//==================================================================================================
/**
 * @module module:toggleHidden
 */

/**
 * Show/Hide an element by toggling it's aria-hidden attribute
 * @function
 * @param {HTMLElement} element - Element who's aria-hidden attribute should be toggled
 * @example
 * toggleHidden(document.querySelector(".element"));
 */
export const toggleHidden = function (element) {
    const attribute = toBoolean(element.getAttribute("aria-hidden"));
    element.setAttribute("aria-hidden", !attribute);

    return !attribute;
};