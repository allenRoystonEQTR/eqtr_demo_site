//==================================================================================================
//
//	Type convert boolean-like string to boolean
//
//==================================================================================================
/**
 * @module module:toBoolean
 */

/**
 * Convert boolean-like string to boolean
 * @function
 * @param {String} value - String representation of a boolean, eg. "true"
 * @returns {Boolean}
 * @example
 * let element = document.querySelector(".element");
 * let hidden = toBoolean(element.getAttribute("aria-hidden"));
 * if (hidden) {
 *  //  Do something
 * }
 */
export const toBoolean = function(value) {
    return value === "true";
};