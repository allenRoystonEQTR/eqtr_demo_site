//==================================================================================================
//
//	Array to object
//
//==================================================================================================
/**
 * @module module:collectionToObject
 */

/**
 * Convert an array-like object to an object by identifying a unique key
 * @function
 * @param {String} key - Key to be used as an identifier
 * @param {Array|NodeList} - Array-like object to be converted
 * @returns {Object}
 * @example
 * let collection = document.querySelectorAll("input");
 * let map;
 *
 * //   Using our callback
 * map = collectionToObject("name", collection, function createMap(element) {
 *      return new Validation(element);
 * });
 *
 * //   Without need for a callback (eg. mapping element attribute to elements)
 * map = collectionToObject("data-identifier", collection);
 */
export const collectionToObject = function(attribute, collection, callback) {
    const result = {};

    collection.forEach(function iterateArray(element) {
        const key = element[attribute] ? element[attribute] : elememt.getAttribute(attribute);

        if (callback) {
            return result[key] = callback(element);
        }
        else {
            return result[key] = element;
        }

    });

    return result;
};