/**
 * @module module:scrollbarWidth
 */

/**
 * Calculate the scrollbar width
 * @function
 * @returns {Number}
 * @example
 * let scrollbarWidth = getScrollBarWidth();
 */
export const getScrollBarWidth = function() {
    return window.innerWidth - document.body.clientWidth;
};
// export const getScrollBarWidth = function() {
//     const inner = document.createElement("div");
//     const outer = document.createElement("div");

//     //  Create an empty container and add it to the DOM
//     outer.style.visibility = "hidden";
//     outer.style.width = "100px";
//     document.body.appendChild(outer);

//     //  Cache our element width
//     const withoutScroll = outer.offsetWidth;

//     //  Force our scroll bar and append an inner element
//     outer.style.overflow = "scroll";
//     inner.style.width = "100%";
//     outer.appendChild(inner);

//     //  Cache our inner element width
//     const withScroll = inner.offsetWidth;

//     //  Remove temporary elements
//     outer.parentNode.removeChild(outer);

//     //  Return the difference between the outer & inner element widths as our scrollbar width
//     return withoutScroll - withScroll;

// };

/**
 * Precalculated value for scrollbar width
 * @static
 * @example
 * if (scrollbarWidth > 10) {
 *  //  Do something
 * }
 */
export const scrollbarWidth = getScrollBarWidth();