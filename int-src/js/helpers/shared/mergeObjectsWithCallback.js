//==================================================================================================
//	Dependencies
//==================================================================================================
import { typeOf } from "helpers/shared/typeOf";

/**
 * In lieu of access to Object.assign, this will merge 2 objects applying a callback to the assigned value
 * Nested objects will also be merged.
 * @module module:mergeObjectWithCallback
 */

//==================================================================================================
//	Method
//==================================================================================================
/**
 * Merge two object returning the result.
 * This is a self absorbing function which supports nested objects
 */
export const mergeObjectsWithCallback = function(destination, obj, original, callback) {
    let property;

    for (property in original) {
        if (original.hasOwnProperty(property)) {
            if (typeOf(original[property]) === "object" && original[property].constructor && original[property].constructor === Object) {
                if (original[property] === undefined) {
                    callback(destination, obj, property)(original[property]);
                }
                else {
                    obj[property] = {};
                    mergeObjectsWithCallback(destination, obj[property], original[property], callback);
                }
            }
            else {
                callback(destination, obj, property)(original[property]);
            }
        }
    }

    return original;
};