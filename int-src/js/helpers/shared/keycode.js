/**
 * @module module:keycode
 */


//==================================================================================
//	Regular expression store
//==================================================================================
/**
 * Key codes for event handling
 * @type {Object}
 * @property {RegExp} escape - Escape key {@link https://www.w3.org/TR/html5/forms.html#e-mail-state-(type=email)|HTML 5 email spec}
 */
export const keycode = {};

Object.defineProperties(keycode, {
    "escape": {
        "value": 27
    },
    "enter": {
        "value": 13
    },
    "uparrow": {
        "value": 38
    },
    "downarrow": {
        "value": 40
    },
    "leftarrow": {
        "value": 37
    },
    "rightarrow": {
        "value": 39
    },
    "space": {
        "value": 32
    },
    "tab": {
        "value": 9
    }
});