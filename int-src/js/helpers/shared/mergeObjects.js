//==================================================================================================
//	Dependencies
//==================================================================================================
import { typeOf } from "helpers/shared/typeOf";

/**
 * In lieu of access to Object.assign, this will merge 2 objects.
 * Nested objects will also be merged.
 * @module module:mergeObject
 */

//==================================================================================================
//	Method
//==================================================================================================
/**
 * Merge two object returning the result.
 * This is a self absorbing function which supports nested objects
 * @function
 * @param {Object} original - Original object to be merged with
 * @param {Object} update - Object to be used for merging
 * @returns {Object}
 * @example
 * let first = { "hello": "world" };
 * let second = { "hello again": { "hello": "world" } };
 * let result = mergeObject(first, second);
 */
export const mergeObjects = function(original, update) {
    let property;

    for (property in update) {
        if (update.hasOwnProperty(property)) {
            if (typeOf(update[property]) === "object") {

                if (original[property] === undefined) {
                    original[property] = update[property];
                }
                else {
                    mergeObjects(original[property], update[property]);
                }
            }
            else {
                original[property] = update[property];
            }
        }
    }

    return original;
};