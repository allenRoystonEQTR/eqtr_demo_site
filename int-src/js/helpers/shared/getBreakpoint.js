//==================================================================================================
//
//  getBreakpoint
//
//==================================================================================================
import { Resize } from "objects/shared/Resize";

/**
 * @module module:getBreakpoint
 */

//==================================================================================================
//	Variables
//==================================================================================================
const delay = 1000;
let pending = false;
let breakpoint = "";


//==================================================================================================
//	Get value
//==================================================================================================
function getValue() {
    return breakpoint = window.getComputedStyle(document.body, "::before").content.replace(/["']/g,'');
}


//==================================================================================================
//	Define listener
//==================================================================================================
function listener() {
    if (pending === false) {
        pending = true;
        setTimeout(function bounce() {
            pending = false;
            getValue();
        }, delay);
    }
}


//==================================================================================================
//	Attach listener
//==================================================================================================
export const resize = new Resize(listener);


//==================================================================================================
//	Return breakpoint value
//==================================================================================================
/**
 * Returns the numeric value of the breakpoint applied.
 * This follows the convention used within our CSS Guidelines.
 * ie. screen1000
 * @function
 * @returns {Number}
 */
export const getBreakpoint = function() {
    return Number(window.getComputedStyle(document.body, "::before").content.replace(/["']/g,'').toLowerCase().replace("screen", ""));
};

/**
 * Returns the literal value of the breakpoint applied.
 * This follows the convention used within our CSS Guidelines.
 * ie. screen1000
 * @function
 * @returns {String}
 */
export const getSASSMQBreakpoint = function() {
    if (breakpoint === "") {
        getValue();
    }
    return breakpoint;
};