//==================================================================================
//
//	Pretty prices
//
//==================================================================================

/**
 * @module module:prettyPrice
 */


/**
 * Converts floats to number format, also supports symbols
 * ie. 100.1 = £100.10
 * @function
 * @param {Number} price - Value to be prettified
 * @param {String} [symbol] - Symbol to be used
 * @returns {String}
 * @example
 * element.textContent = prettyPrice(100.1, "£");
 */
export const prettyPrice = function(price, symbol) {
    let result;
    price = String(price);

    if (price.indexOf(".") === -1) {
        result = price.replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    else {
        result = price.substr(0, price.indexOf(".")).replace(/(\d)(?=(\d{3})+$)/g, '$1,') + price.substr(price.indexOf("."));
    }

    if (symbol !== undefined) {
        result = symbol + result;
    }

    return result;
};