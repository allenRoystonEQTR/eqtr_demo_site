//==================================================================================================
//
//	Post method - XMLHTTP v2
//
//==================================================================================================
import { CatchError } from "objects/shared/CatchError";

/**
 * @module module:post
 */

//==================================================================================================
//	Method
//==================================================================================================
/**
 * Make XMLHttpRequest (v2) using POST.
 * @function
 * @param {String} url - URL for the resource to which data should be posted
 * @param {*} data - Data to be send in the POST
 * @param {String} [content_type] - Content type of the data to be sent, defaults to "application/x-www-form-urlencoded"
 * @param {String} [response_type] - Content type type of expected response, defaults to "text"
 * @param {String} [timeout] - Timeout value (in ms) to be used for the request
 * @param {Boolean} [cors] - Identify whether this a cross domain request which requires credentials
 * @returns {Promise}
 * @example
 * let send = post("https://myurl.com", { "results": [1,2,3,4,5] }, "application/json");
 * send.then(
 * 	function fulfill() {
 * 		//	Do something when it succeeds
 * 	},
 * 	function reject() {
 *  	//	Do something when it fails
 * 	}
 * );
 */
export const post = function(url, data, content_type, response_type, timeout, cors) {
    return new Promise(
        function ajaxPostPromise(fulfill, reject) {
            const xhr = new XMLHttpRequest();
				xhr.open('POST', url, true);

				//	Options
                xhr.setRequestHeader("Content-type", content_type || "application/x-www-form-urlencoded");
				xhr.responseType = response_type || 'text';
				xhr.withCredentials = cors || false;
				xhr.timeout = timeout || 10000;

					//	On load
					xhr.onload = function() {
						if (xhr.status === 200) {
							return fulfill(xhr.response);
						}
						else {
							return new CatchError(xhr, xhr.response, function errorHandling() {
								//	This is bound to CatchError instance by constructor
								reject(xhr, this);
							});
						}
					};

					//	On error
					xhr.onerror = function(error) {
						return new CatchError(xhr, error, function errorHandling() {
							//	This is bound to CatchError instance by constructor
							reject(xhr, this);
						});
					};

				//	Fire request
				return xhr.send(data);

        }
    );
};