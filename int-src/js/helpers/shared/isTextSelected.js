//==================================================================================================
//
//	Check whether text in an input is selected
//
//==================================================================================================
export const isInputTextSelected = function(input) {

    if (typeof input.selectionStart === "number") {

        return input.selectionStart === 0 && input.selectionEnd === input.value.length;

    }
    else if (typeof document.selection !== "undefined") {

        input.focus();
        return document.selection.createRange().text === input.value;

    }

    return false;
};