//==================================================================================================
//
//	Project element collections
//
//==================================================================================================


//==================================================================================================
//	Elements
//==================================================================================================
export const HEADER = document.querySelector(".header");
export const MAIN = document.querySelector(".main");
export const FOOTER = document.querySelector(".footer");