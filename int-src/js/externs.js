//==================================================================================================
//	Object provided within the browser environment
//==================================================================================================

/**
 * The built in browser HTMLElement object
 * @external HTMLElement
 * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement
 */

/**
 * The built in browser SVGElement object
 * @external SVGElement
 * @see https://developer.mozilla.org/en-US/docs/Web/API/SVGElement
 */



 
//==================================================================================================
//	Object provided by the JavaScript interpreter
//==================================================================================================