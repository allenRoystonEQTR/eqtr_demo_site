//==================================================================================================
//
//	BFinance Basic carousel
//
//==================================================================================================
import { BasicCarouselJS } from "objects/shared/BasicCarouselJS";


//==================================================================================================
//	Constructor
//==================================================================================================
export const BfinanceBasicCarouselJS = function(element, next, prev, callback) {
    const carousel = this;
    BasicCarouselJS.call(carousel, element, next, prev, callback);

    carousel.items.forEach(function iterateCarouselItems(item, index) {
        if (index !== carousel.position) {
            item.style.opacity = 0;
        }
    });

    carousel.setWidths();
};
BfinanceBasicCarouselJS.prototype = Object.create(BasicCarouselJS.prototype);
BfinanceBasicCarouselJS.prototype.constructor = BfinanceBasicCarouselJS;


//==================================================================================================
//	Animation
//==================================================================================================
BfinanceBasicCarouselJS.prototype.transform = function(duration) {
    const carousel = this;
    duration = (duration !== undefined) ? duration : 400;

    carousel.anime({
        targets: carousel.container,
        easing: "easeInOutSine",
        duration: (duration !== undefined) ? duration : 400,
        translateX: [
            - carousel.goto(carousel.previous) + "%",
            - carousel.goto(carousel.position) + "%"
        ]
    });
    carousel.anime({
        targets: carousel.items[carousel.position],
        easing: "easeInOutSine",
        duration: (duration !== undefined) ? duration : 250,
        opacity: [0,1]
    });
    carousel.anime({
        targets: carousel.items[carousel.previous],
        easing: "easeInOutSine",
        duration: (duration !== undefined) ? duration : 250,
        opacity: [1,0]
    });
};