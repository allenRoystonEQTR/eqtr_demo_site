//==================================================================================================
//	Dependencies
//==================================================================================================
import { BasicCarouselJS } from "objects/shared/BasicCarouselJS";


//==================================================================================================
//
//	BFinance Basic carousel
//
//==================================================================================================
export const BfinanceTeamCarouselJS = function(element, next, prev) {
    const carousel = this;
    BasicCarouselJS.call(carousel, element, next, prev);

    carousel.map = {};
    carousel.items.forEach(function itemsIteration(item, index) {
        const attribute = item.getAttribute("data-team-member");
        carousel.map[attribute] = index;
    });

    carousel.bind();
    carousel.setWidths();
};
BfinanceTeamCarouselJS.prototype = Object.create(BasicCarouselJS.prototype);
BfinanceTeamCarouselJS.prototype.constructor = BfinanceTeamCarouselJS;


//==================================================================================================
//	Basic transform
//==================================================================================================
BfinanceTeamCarouselJS.prototype.transform = function(duration) {
    const carousel = this;
    carousel.anime({
        targets: carousel.container,
        easing: "easeInOutSine",
        duration: (duration !== undefined) ? duration : 400,
        translateX: [
            - carousel.goto(carousel.previous) + "%",
            - carousel.goto(carousel.position) + "%"
        ]
    });
};