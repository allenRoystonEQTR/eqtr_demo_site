//==================================================================================================
//
//	BFinance Basic carousel
//
//==================================================================================================
import { DelegatedEvent } from "objects/shared/DelegatedEvent";


//==================================================================================================
//	Constructor
//==================================================================================================
/* eslint-disable no-unused-vars */
export const BfinanceDelegatedEvent = function(element, eventType, selector, callback) {
    const delegate = this;
    DelegatedEvent.apply(delegate, arguments);
};
BfinanceDelegatedEvent.prototype = Object.create(DelegatedEvent.prototype);
BfinanceDelegatedEvent.prototype.constructor = BfinanceDelegatedEvent;
/* eslint-enable no-unused-vars */


//==================================================================================================
//	Listener
//==================================================================================================
/**
 * Event listener added to the document to iterate through our callbacks
 */
BfinanceDelegatedEvent.prototype.listener = function(toggle, button) {
    return function listenerEvent(event) {
        const element = this;
        if (event.target.isChildOf(element) === false) {

            //  Close our tooltip if it's visible
            if (toggle.hidden === false) {
                toggle.hidden = true;
            }

            //  Update our data attributes, as per the mouseleave event
            if (prev) {
                prev.parentNode.setAttribute("data-divider", true);
            }
            button.parentNode.setAttribute("data-divider", true);

            //  Prevent default
            event.preventDefault();

        }
    };
};