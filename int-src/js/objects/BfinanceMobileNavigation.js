//==================================================================================================
//
//	BFinance Basic carousel
//
//==================================================================================================
import { NavigationCarousel } from "objects/shared/NavigationCarousel";


//==================================================================================================
//	Constructor
//==================================================================================================
export const BfinanceMobileNavigation = function(element) {
    const carousel = this;
    NavigationCarousel.call(carousel, element);
};
BfinanceMobileNavigation.prototype = Object.create(NavigationCarousel.prototype);
BfinanceMobileNavigation.prototype.constructor = BfinanceMobileNavigation;


// ==================================================================================================
// 	Event handlers
// ==================================================================================================
BfinanceMobileNavigation.prototype.clickHandler = function(position, identifier) {
    const carousel = this;
    return function click(event) {

        if (carousel.animating === false) {

            //  Set our values as required
            carousel.animating = true;
            carousel.showing.push(identifier);
            carousel.position = position;

            //  Set our supplementary text to be the same as our previous menu link text
            const backButton = carousel.items[carousel.position].querySelector("[data-mobilenav-back-text]");
            const title = carousel.views[identifier].getAttribute("data-mobilenav-title");

            if (backButton && title) {
                backButton.innerHTML = title;
            }

        }

        event.preventDefault();
    };
};