//==================================================================================================
//
//	Carousel
//
//==================================================================================================
import { ElementTemplates } from "objects/shared/ElementTemplates";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a basic carousel providing 100% width transitions
 * @constructor
 * @param {HTMLElement} container - Container element, this contains each individual view within the carousel
 * @param {Function} updateUI - Our update UI method, this contains logic to, for example, change button states based on carousel position
 * @param {Function} transform - This is the method used to transform the carousel between positions
 * @property {HTMLElement} container - Our local copy of the container HTML element
 * @property {Array} items - Array of child elements, assumed to be views - this is generated from the children of our container element
 * @property {HTMLElement} backup - Clone of our element container used for resetting
 * @property {Number} increment - Number of items to be transitioned on each movement
 * @property {Number} visible - The size of the currently active element within the carousel (ie. 0.5 = 2x items shown)
 * @property {Number} offset - Transition offset to be used if necessary
 * @property {Number} count - Number of items listed within our carousel
 * @property {Number} last - Last item in our carousel, based of a 0-indexed collection
 * @property {Function} touch - Our instance of Hammer.js is instantiated and bound to this carousel
 * @property {Number} previous - The last known position of the carousel
 * @property {Number} position - The current position of the carousel - this is a get;set; method which fires our updateUI & transform methods when it's value is changed
 */
export const Carousel = function(container, callback) {
    const carousel = this;
    let position = 0;

    //  Elements
    carousel.container = container;
    carousel.items = toArray(carousel.container.children);

    //  Callback
    carousel.callback = callback;

    //  Carousel backup element for destroy purposes
    carousel.backup = new ElementTemplates({ "container": container.cloneNode(true) });

    //  Percentage representation of the width of each carousel element, relative to page width
    carousel.increment = 1;

    //  Visible size of the current carousel item
    carousel.visible = 1;

    //  Offset for visible items when they are centered
    Object.defineProperty(carousel, "offset", {
        get: function getOffset() {
            return (carousel.visible >= 1) ? carousel.visible - 1 : 1 - carousel.visible;
        }
    });

    //  Counts
    carousel.count = carousel.items.length;
    //  Offset for visible items when they are centered
    Object.defineProperty(carousel, "last", {
        get: function getLast() {
            return (carousel.count / carousel.increment) - 1;
        }
    });

    //  Positions
    carousel.previous = 0;

    //TODO: Convert to Proxy when browser support is suitable
    Object.defineProperty(carousel, "position", {
        get: function() {
            return position;
        },
        set: function(index) {
            carousel.previous = position;
            position = index;

             if (carousel.transform) {
                carousel.transform();
            }

            if (carousel.updateUI) {
                carousel.updateUI();
            }

            if (carousel.callback) {
                carousel.callback(carousel);
            }

            return position;
        }
    });

    //  Initialise our carousel
    carousel.init();
};


//==================================================================================================
//	Prepare values
//==================================================================================================
/**
 * Our intialisation method for the carousel
 * This updates the UI if our method is available
 */
Carousel.prototype.init = function() {
    const carousel = this;

    //  Hide our UI if there is only 1 or 0 items in our carousel.
    if (carousel.items.length < 2) {
        carousel.hideUI();
    }

    //  Update our UI state
    else if (carousel.updateUI && carousel.buttons && (carousel.buttons.next && carousel.buttons.prev)) {
        carousel.updateUI(carousel);
    }

};


//==================================================================================================
//  Reset
//==================================================================================================
/**
 * Reset our carousel state to page load & initialise
 */
Carousel.prototype.reset = function() {
    const carousel = this;
    const backup = carousel.backup.container;

    carousel.previous = 0;
    carousel.position = 0;

    carousel.container.parentNode.replaceChild(backup, carousel.container);
    carousel.container = backup;
    carousel.items = toArray(carousel.container.children);
    carousel.touch = new Hammer(backup);
};


//==================================================================================================
//	Hide carousel interface
//==================================================================================================
/**
 * Hide our UI elements
 * This makes assumptions regarding object structure & utilises our standard helper class ".hidden"
 */
Carousel.prototype.hideUI = function() {
    const carousel = this;
    let property;

    if (carousel.buttons) {
        for (property in carousel.buttons) {
            if (carousel.buttons.hasOwnProperty(property) && carousel.buttons[property]) {
                carousel.buttons[property].addClass("hidden");
            }
        }
    }

};


//==================================================================================================
//	Show carousel interface
//==================================================================================================
/**
 * Show our UI elements
 * This makes assumptions regarding object structure & utilises our standard helper class ".hidden"
 */
Carousel.prototype.showUI = function() {
    const carousel = this;
    let property;

    if (carousel.buttons) {
        for (property in carousel.buttons) {
            if (carousel.buttons.hasOwnProperty(property) && carousel.buttons[property]) {
                carousel.buttons[property].removeClass("hidden");
            }
        }
    }

};


//==================================================================================================
//	Calculate the container width when elements are shown without offset
//==================================================================================================
/**
 * Calculate the container width when elements are shown WITHOUT offset
 */
Carousel.prototype.calculateWidth = function() {
    const carousel = this;

    return (carousel.count * 100) - ((carousel.count * 100) * carousel.offset);
};


//==================================================================================================
//	Calculate the container width when elements are shown with offset
//==================================================================================================
/**
 * Calculate the container width when elements are shown WITH offset
 */
Carousel.prototype.calculateOffsetWidth = function() {
    const carousel = this;

    return (carousel.count * 100) - ((carousel.last * 100) * carousel.offset);
};


//==================================================================================================
//	Calculate the item width
//==================================================================================================
/**
 * Calculate our carousel item widths as a percentage
 */
Carousel.prototype.calculateItemWidth = function() {
    const carousel = this;

    return 100 / carousel.count;
};


//==================================================================================================
//	Calculate remainder offset
//==================================================================================================
/**
 * If we are still desiring to center our element whilst showing more than 1 item
 * A decimal figure can be used for our visible property, eg. carousel.visible = 0.75 (75%)
 * This method will correctly calculate the margin offset for the first item to ensure it can be centered appropriately
 */
Carousel.prototype.calculateOffset = function() {
    const carousel = this;

    return (carousel.visible > 1) ? carousel.visible % 1 : 1 % carousel.visible;
};


//==================================================================================================
//	Set element widths
//==================================================================================================
/**
 * Calculates width of our container element, and each of it's children.
 * This method may not be required if the carousel utilises FlexBox
 * @param {Boolean} offset - Determine whether we are centering our elements with an offset
 */
Carousel.prototype.setWidths = function(offset) {
    const carousel = this;
    const width = (offset) ? carousel.calculateOffsetWidth() : carousel.calculateWidth();
    const itemWidth = carousel.calculateItemWidth();

    //  Convert to percentage & set our container width
    carousel.container.style.width = width + "%";

    //  Set our item widths
    carousel.items.forEach(function iterateCarouselItems(item) {
        item.style.width = itemWidth + "%";
    });

};


//==================================================================================================
//	Calculate transition distance
//==================================================================================================
/**
 * Default calculation to return the correct container position.
 */
Carousel.prototype.goto = function(position) {
    const carousel = this;
    return ((position * carousel.increment) / carousel.count) * 100;
};


//==================================================================================================
//	Center transition distance
//==================================================================================================
/**
 * If we are left with an offset ensure the result is a centered element
 */
Carousel.prototype.gotoCenter = function(position) {
    const carousel = this;
    const indent = carousel.calculateItemWidth() * carousel.offset;
    const centered = ((indent / 2) * -1);

    return carousel.goto(position) + centered;
};



//==================================================================================================
//	Next
//==================================================================================================
/**
 * Increments our Carousel.position property, moving the carousel to the next element
 */
Carousel.prototype.next = function() {
    const carousel = this;
    carousel.position = carousel.position + 1;
};


//==================================================================================================
//	Previous
//==================================================================================================
/**
 * Decrements our Carousel.position property, moving the carousel to the previous element
 */
Carousel.prototype.previous = function() {
    const carousel = this;
    carousel.position = carousel.position - 1;
};


//==================================================================================================
//  Update our item counts if the carousel elements have been updated
//==================================================================================================
/**
 * Re-initialise the element & number values within the carousel.
 * This can be used post-initialisation if the carousel has been altered dynamically
 */
Carousel.prototype.updateCounts = function() {
    const carousel = this;

    carousel.items = toArray(carousel.container.children);
    carousel.count = carousel.items.length;
    carousel.last = carousel.count - 1;
};


//==================================================================================================
//  Enable animations
//==================================================================================================
/**
 * Enable carousel CSS transforms
 */
Carousel.prototype.enableCSSAnimation = function() {
    const carousel = this;
    carousel.container.removeClass("no-transform");
};


//==================================================================================================
//  Disable animations
//==================================================================================================
/**
 * Disable carousel CSS transforms.
 * Force "transform: none;" to allow for transitions to be made without being animated (eg. skipping to a view element)
 */
Carousel.prototype.disableCSSAnimation = function() {
    const carousel = this;
    carousel.container.addClass("no-transform");
};


//==================================================================================================
//  Enable animations
//==================================================================================================
/**
 * Enable carousel JS transforms
 */
Carousel.prototype.enableJSAnimation = function(animation) {
    const carousel = this;
    carousel.transform = animation;
    carousel.transform();
};


//==================================================================================================
//  Disable animations
//==================================================================================================
/**
 * Disable carousel JS transforms.
 * Force "transform: none;" to allow for transitions to be made without being animated (eg. skipping to a view element)
 */
Carousel.prototype.disableJSAnimation = function() {
    const carousel = this;
    const animation = carousel.transform;
    carousel.transform = function() { return false; };
    return animation;
};