//==================================================================================================
//
//	Carousel
//
//==================================================================================================
import anime from "animejs";


//==================================================================================================
//	Private methods
//==================================================================================================
/**
 * Logic to endlessly loop the carousel.
 * @private
 * @function loop
 * @memberof CarouselEndlessJS
 * @param {CarouselEndlessJS} endless - Our instance of the endless carousel module
 */
function loop(endless) {
    return function loopCarousel() {
        if (endless.carousel.position === endless.carousel.last) {
            endless.desired = endless.first;
        }
        else if (endless.carousel.position === 0) {
            endless.desired = endless.last;
        }
        else {
            endless.desired = undefined;
        }
    };
}


/**
 * Our proxy transform method for the carousel
 * @private
 * @function transform
 * @memberof CarouselEndlessJS
 * @param {CarouselEndlessJS} endless - Our instance of the endless carousel module
 */
function transform(endless) {
    return function endlessTransform() {
        const options = endless.carousel.animationOptions();
        endless.carouselComplete = options.complete;
        options.complete = endless.complete;
        anime(options);
    };
}


/**
 * Our proxy AnimeJS complete callback
 * @private
 * @function transitionEnd
 * @memberof CarouselEndlessJS
 * @param {CarouselEndlessJS} endless - Our instance of the endless carousel module
 */
function transitionEnd(endless) {
    return function endlessTransitionEnd(anim) {
        if (endless.desired !== undefined) {
            endless.updatePosition(1);
        }

        if (endless.carouselComplete) {
            endless.carouselComplete(anim);
        }
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a carousel module which alters to DOM to produce the effect of an endless list of items
 * @constructor
 * @param {Carousel} element - The instance of {@link Carousel} to which this module is bound
 * @property {Carousel} carousel - Cached property to the instance of {@link Carousel}
 * @property {Number} visible - The number of visible elements within the carousel
 * @property {Number} last - Last item in our endless carousel differs from the base carousel, the true last position is reflected here
 * @property {Number} first - Unlike the base carousel, the first true item in our carousel will begin at position 1.
 * @property {Number} desired - This value is used by our "transitionend" event to set the desired location of our carousel
 * @property {Function} transitionEnd - Our "transitionend" event callback
 * @property {Function} carouselUpdateUI - This is the original carousel updateUI method. The base method is replaced when our endless module is bound.
 */
export const CarouselEndlessJS = function(carousel) {
    const endless = this;

    endless.carousel = carousel;
    endless.visible = 1;

    endless.last = endless.carousel.last + 1;
    endless.first = 1;  //  This may change depending on the number of visible items

    endless.complete = transitionEnd(endless);
    endless.carouselComplete = undefined;   //  This is our cached complete method from the original carousel options

    endless.transform = transform(endless);
    endless.carouselTransform = carousel.transform;

    endless.carouselUpdateUI = carousel.updateUI;
};


//==================================================================================================
//	Attach to our carousel
//==================================================================================================
/**
 * Attach our endless carousel logic to an existing carousel
 * @example
 * // Attach our endless module to the carousel
 * let carousel = new BasicCarousel(element, next, prev);
 * carousel.endless.attach();
 */
CarouselEndlessJS.prototype.attach = function() {
    const endless = this;

    //  Update our updateUI method with our passthrough
    //  We have our original method cached within our constructor
    endless.carousel.updateUI = loop(endless);

    //  Update our transform method with our passthrough
    //  This is for the purposes of tagging a complete callback
    //  We have our original complete callback cached within our constructor
    endless.carousel.transform = transform(endless);

    //  Clone our first element and place it after our last
    endless.carousel.container.appendChild(endless.carousel.items[0].cloneNode(true));

    //  Clone our last element and place it before our first
    endless.carousel.container.insertBefore(endless.carousel.items[endless.carousel.last].cloneNode(true), endless.carousel.items[0]);

    //  Update our carousel counts to reflect these DOM changes
    endless.carousel.updateCounts();

    //  Update our carousel widths to reflect these DOM changes
    endless.carousel.setWidths();

    //  Update our position to the first true non-repeated element
    endless.updatePosition(1);

    //  Update our UI to reflect carousel position changes
    endless.carouselUpdateUI(endless.carousel);

};


//==================================================================================================
//	Detach from our carousel
//==================================================================================================
/**
 * Attach our endless carousel logic to an existing carousel
 * @example
 * // Attach our endless module to the carousel
 * let carousel = new BasicCarousel(element, next, prev);
 * carousel.endless.attach();
 * carousel.endless.detach();
 */
CarouselEndlessJS.prototype.detach = function() {
    const endless = this;

    //  Update our updateUI method with our passthrough
    //  We have our original method cached within our constructor
    endless.carousel.updateUI = endless.carouselUpdateUI;

    //  Update our transform method with our passthrough
    //  This is for the purposes of tagging a complete callback
    //  We have our original complete callback cached within our constructor
    endless.carousel.transform = endless.carouselTransform;

    //  Clone our first element and place it after our last
    endless.carousel.container.removeChild(endless.carousel.items[0]);

    //  Clone our last element and place it before our first
    endless.carousel.container.removeChild(endless.carousel.items[endless.carousel.last]);

    //  Update our carousel counts to reflect these DOM changes
    endless.carousel.updateCounts();

    //  Update our carousel widths to reflect these DOM changes
    endless.carousel.setWidths();

    //  Update our position to the first true non-repeated element
    //  Note our assumption to return to the first position within our carousel - this may need reviewed
    endless.updatePosition(0);

    //  Update our UI to reflect carousel position changes
    endless.carouselUpdateUI(endless.carousel);

};


//==================================================================================================
//	Update our carousel position without the use of transforms
//==================================================================================================
/**
 * Update our carousel position without the use of transforms
 * @param {Number} position - The desired new position of the carousel
 */
CarouselEndlessJS.prototype.updatePosition = function(position) {
    const endless = this;
    const animation = endless.carousel.disableJSAnimation();

    endless.carousel.previous = endless.carousel.position;
    endless.carousel.position = position;
    endless.carousel.container.style.left = - carousel.goto(position) +"%";

    setTimeout(function delayReactivation() {
        endless.carousel.enableJSAnimation(animation);
    }, 0);
};