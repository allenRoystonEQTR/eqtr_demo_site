//==================================================================================================
//
//  History API
//
//==================================================================================================
/**
 * List of state callbacks we wish to fire when the browser history is popped
 * @private
 * @memberof HistoryAPI
 */
const stateCallbacks = [];


//==================================================================================================
//	Bind to state change event
//==================================================================================================
/**
 * Event handler for the window.onpopstate event
 * When our HistoryAPI method is imported into a project we attach this event to the window object
 * @private
 * @memberof HistoryAPI
 * @param {Event} event - Event object passed to the callback when the popstate event is fired.
 */
function stateChangeEvent(event) {
    stateCallbacks.forEach(function stateChange(callback) {
        if (event.state) {
            callback(JSON.parse(event.state));
        }
        else {
            callback(event.state);
        }
    });
    event.preventDefault();
}

if (!window.onpopstate) {
    window.onpopstate = stateChangeEvent;
}


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * Wrapper for HistoryAPI implementations
 * To correctly implement this object (as with all History API implementations) an understanding of both page state, and when changes in said state occurs, is required.
 * @constructor
 * @param {Function} callback - Callback to be fired when History state changes. The current state (as an {Object}) is passed through this callback when it is called.
 * @property {History} history - Local reference to window.history
 * @property {Object} state - The current page state we wish to populate the browser history with
 * @property {String} url - The current URL we wish to associate our page state with
 * @property {Number} index - The array index of our callback within all page history callbacks.
 * @property {Function} callback - A local copy of our callback method.
 * @example
 * let history = new HistoryAPI(function callback(state) { console.log(state); });
 */
export const HistoryAPI = function(callback) {
    const browser = this;

    browser.history = window.history;
    browser.state = {};
    browser.url = undefined;
    browser.index = undefined;
    browser.callback = callback;

    stateCallbacks.push(callback);
};


//==================================================================================================
//	Add our callback to the queue
//==================================================================================================
/**
 * Method to add our callback to the queue
 */
HistoryAPI.prototype.attach = function() {
    const browser = this;

    if (browser.index === undefined) {
        browser.index = stateCallbacks.length;
        stateCallbacks.push(browser.callback);
    }

    return browser;

};

//==================================================================================================
//	Remove our callback to the queue
//==================================================================================================
/**
 * Method to remove our callback from the queue
 */
HistoryAPI.prototype.detach = function() {
    const browser = this;

    if (browser.index !== undefined) {
        stateCallbacks.splice(browser.index, 1);
        browser.index = undefined;
    }

    return browser;
};


//==================================================================================================
//	Push state
//==================================================================================================
/**
 * Creates a new history entry with our current state
 * @param {String} url - URL with which to push our state to browser history
 */
HistoryAPI.prototype.pushState = function(url) {
    const browser = this;
    browser.url = url;
    browser.history.pushState(JSON.stringify(browser.state), document.title, url);
};

//==================================================================================================
//	Parse state
//==================================================================================================
/**
 * Retrieve the state reflected in browser history for our current URL
 */
HistoryAPI.prototype.parseState = function() {
    const browser = this;
    return browser.history.state;
};

//==================================================================================================
//	Replace state
//==================================================================================================
/**
 * Replace the state for the current URL without appending a history item
 * @param {String} url - URL with which to replace the current history item
 */
HistoryAPI.prototype.replaceState = function(url) {
    const browser = this;
    browser.url = url;
    browser.history.replaceState(JSON.stringify(browser.state), document.title, url);
};