import { typeOf } from "helpers/shared/typeOf";
import { toBoolean } from "helpers/shared/toBoolean";

//==================================================================================================
//
//	Video player
//
//==================================================================================================

//==================================================================================================
//	Lookup table for readable readyState events
//==================================================================================================
/**
 * @see https://developers.google.com/youtube/iframe_api_reference#onStateChange
 */
const playerState = {
    "-1": "unstarted",
    "0": "ended",
    "1": "playing",
    "2": "paused",
    "3": "buffering",
    "5": "video cued"
};

const instances = [];

//==================================================================================================
//	Constructor
//==================================================================================================
export const YouTubePlayer = function(container, implementation, options) {
    const video = this;

    //  Element
    video.container = container;
    video.element = container.querySelector("[data-video-container]");
    video.controls = container.querySelector("[data-video-control]");

    //  Youtube
    video.id = container.getAttribute("data-youtube-id");
    video.frameId = video.element.id;
    video.options = (options) ? options : {
        videoId: video.id,
        playerVars: {
            controls: 0
        },
        events: {
            'onReady': video.ready()
        }
    };

    //  Poster
    video.poster = container.querySelector("[data-video-poster='youtube']");

    //  Youtube Iframe API service
    video.service = undefined;

    //  Implementation
    video.implementation = implementation;

    //  Player state
    Object.defineProperty(video, "state", {
        get: function getLoop() {
            return playerState[String(video.service.getPlayerState())];
        }
    });

    //  Playing
    let playing = false;
    Object.defineProperty(video, "playing", {
        get: function getPlaying() {
            return playing;
        },
        set: function setPlaying(value) {
            if (value === true || value === false) {
                if (value) {
                    video.service.playVideo();
                }
                else {
                    video.service.pauseVideo();
                }
                playing = value;
            }
            return playing;
        }
    });

    //  Looping
    let loop = false;
    Object.defineProperty(video, "loop", {
        get: function getLoop() {
            return loop;
        },
        set: function setLoop(value) {
            if (value === true || value === false) {
                video.service.setLoop(value);
                loop = value;
            }
            return loop;
        }
    });

    //  Autoplay
    let autoplay = video.element.getAttribute("autoplay");
    Object.defineProperty(video, "autoplay", {
        get: function getAutoplay() {
            return autoplay;
        },
        set: function setAutoplay(value) {
            if (typeOf(value) === "string") {
                video.element.setAttribute("autoplay", value);
                autoplay = value;
            }
            return autoplay;
        }
    });

    //  Poster
    let hidePoster = toBoolean(video.poster.getAttribute("aria-hidden"));
    Object.defineProperty(video, "hidePoster", {
        get: function getPoster() {
            return hidePoster;
        },
        set: function setPoster(value) {
            if (hidePoster !== value) {
                video.poster.setAttribute("aria-hidden", value);
                hidePoster = value;
            }
            return hidePoster;
        }
    });

    instances.push(video);
    video.init();
};


//==================================================================================================
//	Bind our handlers
//==================================================================================================
YouTubePlayer.prototype.init = function() {
    const video = this;

    window.onYouTubeIframeAPIReady = function createService(event) {

        instances.forEach(function (video) {

            video.service = new YT.Player(video.frameId, video.options);

            if (video.implementation) {
                video.implementation(video, video.service, event);
            }

            if (video.autoplay === true) {
                video.play();
            }

            if (video.service.loaded) {
                video.controls.setAttribute("aria-hidden", false);
            }

        });

    };

    return video;
};


//==================================================================================================
//	Bind our handlers
//==================================================================================================
YouTubePlayer.prototype.bind = function(listeners) {
    const video = this;
    let eventName;

    if (typeOf(listeners === "string")) {
        if (listeners.indexOf(",") !== -1) {
            listeners.split(",").forEach(function iterateListeners(listener) {
                eventName = video.normaliseEvents(listener);
                return video.service.addEventListener(eventName, video[listener.trim()]());
            });
        }
        else {
            eventName = video.normaliseEvents(listeners);
            video.element.addEventListener(eventName, video[listeners]());
        }

    }

    if (YT.loaded) {
        return video.controls.setAttribute("aria-hidden", false);
    }

    return video;
};


//==================================================================================================
//	Event handler - When video becomes ready to play
//==================================================================================================
YouTubePlayer.prototype.ready = function() {
    const video = this;

    return function readyEvent(event) {
        if (event.data === YT.PlayerState.CUED) {
            video.controls.setAttribute("aria-hidden", false);
        }
    };
};


//==================================================================================================
//	Ready call
//==================================================================================================
YouTubePlayer.prototype.ready = function() {
    const video = this;
    return function readyEvent(event) {
        if (event.data === YT.PlayerState.CUED) {
            video.controls.setAttribute("aria-hidden", false);
        }
    };
};


//==================================================================================================
//	Event handler - Action to take when video ends
//==================================================================================================
YouTubePlayer.prototype.ended = function() {
    const video = this;
    return function endedListener(event) {
        if (event.data === YT.PlayerState.ENDED) {
            video.pause();
        }
    };
};

//==================================================================================================
//	Event handler - Action to take when video is paused
//==================================================================================================
YouTubePlayer.prototype.paused = function() {
    const video = this;
    return function endedListener(event) {
        if (event.data === YT.PlayerState.PAUSED) {
            video.pause();
        }
    };
};


//==================================================================================================
//	Play
//==================================================================================================
YouTubePlayer.prototype.play = function() {
    const video = this;

    video.hidePoster = true;
    video.controls.setAttribute("aria-hidden", false);
    video.container.setAttribute("data-video-playing", true);

    return video.playing = true;
};


//==================================================================================================
//	Pause
//==================================================================================================
YouTubePlayer.prototype.pause = function() {
    const video = this;

    video.hidePoster = false;
    video.controls.setAttribute("aria-hidden", false);
    video.container.setAttribute("data-video-playing", false);

    return video.playing = false;
};



//==================================================================================================
//	Normalise our naming convention for events with those by the YT player
//==================================================================================================
YouTubePlayer.prototype.normaliseEvents = function(eventName) {
    return "onStateChange";
};