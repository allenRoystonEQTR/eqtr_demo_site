//==================================================================================================
//
//	Dropdown navigation
//
//==================================================================================================
import { ToggleView } from "objects/shared/ToggleView";
import { dataStore } from "helpers/globals";
import { keycode } from "helpers/shared/keycode";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Blur event handler & timeout callback
//  The timeout is required to allow the document.activeElement to change
//==================================================================================================
function blurDelay(navigation, names) {
    return function blurTimeoutDelay() {

        //  Check for the upmost navigation item
        //  As the we are going in reverse through the DOM by using parents(), we need to flip our array
        const parents = document.activeElement.parents("[data-navigation]").reverse().map(function parentDropdownNames(parent) {
            return parent.getAttribute("data-navigation");
        });

        let count = names.length;
        while(count) {
            count = count - 1;
            if (navigation.name === names[count]) {
                if (navigation.name !== parents[count]) {
                    navigation.hidden = true;
                }
            }
        }

    };
}

function blur(navigation, element) {
    const parents = element.parents("[data-navigation]");
    //  As the we are going in reverse through the DOM by using parents(), we need to flip our array
    const names = parents.reverse().map(function listNames(element) {
        return element.getAttribute("data-navigation");
    });

    return function blurHandler(event) {
        /* eslint-disable no-invalid-this */
        //  Context of *this* within our event handler is the HTMLElement event origin
        setTimeout(blurDelay(navigation, names, this), 10);
        /* eslint-enable no-invalid-this */
        event.stopPropagation();
    };
}


//==================================================================================================
//	Hover handler
//==================================================================================================
function hover(navigation, hidden) {
    return function hoverListener() {
        return navigation.hidden = hidden;
    };
}


//==================================================================================================
//	Key event handlers to open dropdown lists
//==================================================================================================

//  Use of the enter key to open all navigation trigger elements
function keyOpen(navigation) {
    const firstElement = navigation.view.querySelector("[data-navigation-item]");

    return function keyOpenHandler(event) {
        const key = event.keyCode;

        if (key === keycode.enter) {

            if (navigation.hidden === true) {
                navigation.hidden = false;
                firstElement.focus();
            }
            else {
                navigation.hidden = true;
                navigation.trigger.focus();
            }

            event.stopPropagation();
            event.preventDefault();
        }

    };
}


//  Use of the down arrow to open all navigation primary triggers
function keyOpenDown(navigation) {
    const firstElement = navigation.view.querySelector("[data-navigation-item]");

    return function keyOpenDownHandler(event) {
        const key = event.keyCode;

        if (key === keycode.downarrow) {

            navigation.hidden = false;
            firstElement.focus();

            event.stopPropagation();
            event.preventDefault();
        }

    };
}


//  Show or hide the element on left arrow press
function keyLeft(navigation, hidden) {
    const firstElement = navigation.view.querySelector("[data-navigation-item]");

    return function keyLeftHandler(event) {
        const key = event.keyCode;

        if (key === keycode.leftarrow) {

            navigation.hidden = hidden;

            if (hidden) {
                navigation.trigger.focus();
            }
            else {
                if (firstElement) {
                    firstElement.focus();
                }
            }

            event.stopPropagation();
            event.preventDefault();
        }

    };
}

//  Show or hide the element on right arrow press
function keyRight(navigation, hidden) {
    const firstElement = navigation.view.querySelector("[data-navigation-item]");

    return function keyRightHandler(event) {
        const key = event.keyCode;

        if (key === keycode.rightarrow) {

            navigation.hidden = hidden;

            if (hidden) {
                navigation.trigger.focus();
            }
            else {
                if (firstElement) {
                    firstElement.focus();
                }
            }

            event.stopPropagation();
            event.preventDefault();
        }

    };
}


//  Use of the keys to navigate the navigation sub items
function keyNavigate(navigation, element) {

    let nextElement = element.parentElement.nextElementSibling;
        nextElement = nextElement ? nextElement.querySelector("[data-navigation-item='"+ navigation.name +"']") : nextElement;

    let previousElement = element.parentElement.previousElementSibling;
        previousElement = previousElement ? previousElement.querySelector("[data-navigation-item='"+ navigation.name +"']") : previousElement;

    let firstElement = element.parentElement.parentElement.firstElementChild;
        firstElement = firstElement ? firstElement.querySelector("[data-navigation-item='"+ navigation.name +"']") : firstElement;

    let lastElement = element.parentElement.parentElement.lastElementChild;
        lastElement = lastElement ? lastElement.querySelector("[data-navigation-item='"+ navigation.name +"']") : lastElement;


    return function keyNavigateHandler(event) {
        const key = event.keyCode;

        //  Down arrow
        if (key === keycode.uparrow) {

            if (previousElement) {
                previousElement.focus();
            }
            else {
                if (lastElement) {
                    lastElement.focus();
                }
            }

            event.stopPropagation();
            event.preventDefault();
        }

        //  Up arrow
        if (key === keycode.downarrow) {

            if (nextElement) {
                nextElement.focus();
            }
            else {
                if (firstElement) {
                    firstElement.focus();
                }
            }

            event.stopPropagation();
            event.preventDefault();
        }

        //  Escape to close
        if (key === keycode.escape) {
            navigation.hidden = true;

            navigation.trigger.focus();

            event.stopPropagation();
            event.preventDefault();
        }

    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
export const DropdownNavigation = function(container, updateUI) {
    const navigation = this;

    //  Properties
    navigation.name = container.getAttribute("data-navigation");
    navigation.container = container;
    navigation.updateUI = updateUI;

    //  Inheritance
    ToggleView.call(
        navigation,
        container.querySelector("[data-navigation-trigger='"+ container.getAttribute("data-navigation") +"']"),
        navigation.callback
    );

};
DropdownNavigation.prototype = Object.create(ToggleView.prototype);
DropdownNavigation.prototype.constructor = DropdownNavigation;


//==================================================================================================
//	Bind events
//==================================================================================================
DropdownNavigation.prototype.bind = function() {
    const navigation = this;
    const primary = navigation.container.querySelector("[data-navigation-parent='"+ navigation.name +"']");
    const sub = navigation.container.querySelector("[data-navigation-item][data-navigation-trigger='"+ navigation.name +"']:not([data-navigation-parent])");
    const links = toArray(navigation.container.querySelectorAll("[data-navigation-item='"+ navigation.name +"']"));

    navigation.container.addEventListener("mouseover", hover(navigation, false));
    navigation.container.addEventListener("mouseout", hover(navigation, true));
    navigation.trigger.addEventListener("keydown", keyOpen(navigation));

    links.forEach(function iterateLinks(link) {
        link.addEventListener("blur", blur(navigation, link));
        link.addEventListener("keydown", keyNavigate(navigation, link));

        if (sub) {
            link.addEventListener("keydown", keyLeft(navigation, true));
        }
    });

    if (primary) {
        primary.addEventListener("keydown", keyOpenDown(navigation));
    }

    if (sub) {
        sub.addEventListener("keydown", keyRight(navigation, false));
    }


    return navigation.init();
};


//==================================================================================================
//	Initial logic
//==================================================================================================
DropdownNavigation.prototype.init = function() {
    const navigation = this;

    dataStore.set(navigation, navigation.container);
};


//==================================================================================================
//	Callback to be fired on update
//==================================================================================================
DropdownNavigation.prototype.callback = function() {
    const navigation = this;

    if (navigation.updateUI) {
        navigation.updateUI(navigation);
    }

};