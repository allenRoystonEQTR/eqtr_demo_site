//==================================================================================================
//
//	AjaxService
//
//==================================================================================================
import { get } from "helpers/shared/get";
import { post } from "helpers/shared/post";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is an XMLHttpRequest wrapper which provides easy management of web service calls.
 * It also provided additional features for the deferral of requests made in get/set scenarios.
 * @constructor
 * @param {String} path - URL for the web service
 * @param {String} [domain] - Domain the web service is currently hosted on. Defaults to current domain & any passed value should ignore protocol eg. //eqtr.com
 * @property {String} domain - Domain to be used for our web service
 * @property {String} url - Full path to our web service
 * @property {Boolean} pending - Used by our .defer() prototype to indicate whether we have a request pending
 */
export const AjaxService = function(path, domain) {
	const service = this;

	service.domain = domain ? domain : "//"+ window.location.hostname;
	service.url = service.domain + path;
	service.pending = false;
};


//==================================================================================================
//	Get
//==================================================================================================
/**
 * AjaxService request method
 * This is a higher order function which should be instantiated and locally bound
 * @param {Function} fulfill - Fulfill callback for our AJAX promise
 * @param {Function} reject - Reject callback for our AJAX promise
 * @return {AjaxService~ajaxRequest}
 */
AjaxService.prototype.get = function(fulfill, reject) {
	const service = this;

	/**
	 * Send a GET request to the web service specified in our constructor
	 * @inner
	 * @async
	 * @function AjaxService.get~ajaxRequest
	 * @param {String} queryString - Query string to be send to our web service
	 * @param {Function} [overrideFulfill] - Override your fulfill method by passing a function
	 * @param {Function} [overrideReject] - Override your reject method by passing a function
	 * @example
	 * //	Make a request to our web service
		let service = new AjaxService("/api/data");
		let request = service.get(fulfill, reject);
		request("?q=*.*", callback);
		*/
	return function ajaxRequest(queryString, overrideFulfill, overrideReject) {
		const url = queryString !== undefined ? service.url + queryString : service.url;
		const request = get(url);

			request.then(
				function ajaxGetFulfill(response) {
					if (overrideFulfill) {
						overrideFulfill(response, request);
					}
					else {
						fulfill(response, request);
					}
				},
				function ajaxGetReject(reason) {
					if (overrideReject) {
						overrideReject(reason, request);
					}
					else {
						reject(reason, request);
					}
				}
			);
			return request;
	};

};


//==================================================================================================
//	Post
//==================================================================================================
/**
 * AjaxService post method
 * This is a higher order function which should be instantiated and locally bound
 * @param {Function} fulfill - Fulfill callback for our AJAX promise
 * @param {Function} reject - Reject callback for our AJAX promise
 * @param {String} [mimeType] - MIME type of the posted data, defaults to "application/x-www-form-urlencoded"
 * @return {AjaxService~ajaxPost}
 */
AjaxService.prototype.post = function(fulfill, reject, mimeType) {
	const service = this;

	/**
	* Send a POST request to the web service specified in our constructor
	* @inner
	* @async
	* @function AjaxService~ajaxPost
	* @param {String} queryString - Query string to be send to our web service
	 * @param {Function} [overrideFulfill] - Override your fulfill method by passing a function
	 * @param {Function} [overrideReject] - Override your reject method by passing a function
	* @example
		//	Make a request to our web service
		let service = new AjaxService("/api/data");
		let post = service.post(fulfill, reject, "application/json");
		post({ "data": "value" }, callback);
	*/
	return function ajaxPost(value, overrideFulfill, overrideReject) {
		const request = post(service.url, value, mimeType);
			request.then(
				function ajaxPostFulfill(response) {
					if (overrideFulfill) {
						overrideFulfill(response, request);
					}
					else {
						fulfill(response, request);
					}
				},
				function ajaxPostReject(reason) {
					if (overrideReject) {
						overrideReject(reason, request);
					}
					else {
						reject(reason, request);
					}
				}
			);
			return request;
	};

};


//==================================================================================================
//	Defer
//==================================================================================================
/**
 * AjaxService defer method
 * This is a higher order function which should be instantiated and locally bound
 * It provides the deferral of simultaneous requests within our instantiated object
 * @param {Function} callback - Method to be called within a deferred state
 * @param {Number} [time] - Time to delay our request by, this defaults to (and should usually be set as) 0
 * @return {AjaxService~deferredCall}
 */
AjaxService.prototype.defer = function defer(callback, time) {
	const service = this;

	time = time !== undefined ? time : 0;

	/**
	* All arguments passed to this methods are sent to the encapsulated method
	* @inner
	* @async
	* @function AjaxService~deferredCall
	* @example
	* //	Defer requests made to our web service until the stack has cleared (append to our event queue)
		let service = new AjaxService("/api/data");
		let request = service.defer(service.request(fulfill, reject, "application/json"));
		request({ "data": "value" }, callback);
	*/
	return function deferredCall() {
		let args;

		if (service.pending === false) {
			service.pending = true;
			args = arguments;
		}
		else {
			//	Always use the latest arguments
			args = arguments;
		}

		const action = new Promise(function deferPromise(fulfill, reject) {
			return setTimeout(function() {
				if (service.pending === true) {
					service.pending = false;
					fulfill(action);
				}
				else {
					reject(action);
				}
			}, time);
		});

		action.then(
			function fulfillCallback() {
				return callback.apply(service, args);
			},
			function rejectCallback() {
				return false;
			}
		);

	};
};