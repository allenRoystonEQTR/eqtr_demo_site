//==================================================================================
//
//	ValueValidation value
//
//==================================================================================
import { regexp } from "helpers/shared/regexp";


//==================================================================================
//	Constructor
//==================================================================================
/**
 * ValueValidation value using pre-set options
 * @constructor
 * @property {Object} options - Object containing general options for validation
 * @property {Number} options.minLength - Tested minimum length of a value
 * @property {Number} options.maxLength - Tested maximum length of a value
 * @property {Object} regexp - Our {@link module:regexp} object
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("email");
 * let value = element.value;
 * if (valid.email(value)) {
 *  //  Do something
 * }
 */
export const ValueValidation = function() {
	const validation = this;

    validation.options = {
        "minLength": 3,
        "maxLength": 50,
    };

    Object.defineProperty(validation, "regexp", {
        get: function getRegexp() {
            return regexp;
        }
    });

};


//==================================================================================
//	Required
//==================================================================================
/**
 * Check for a non-empty string
 * @param {String} value - Value to be tested
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("email");
 * let value = element.value;
 * if (valid.required(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.required = function(value) {
	return value !== "";
};


//==================================================================================
//	Minimum length
//==================================================================================
/**
 * Check length of a string exceeds a value
 * @param {String} value - Value to be tested
 * @param {Number} [minimum] - Count to be used (defaults to {@link ValueValidation.options.minLength})
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("email");
 * let value = element.value;
 * if (valid.minLength(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.minLength = function(value, minimum) {
    const validation = this;

	minimum = minimum ? minimum : validation.options.minLength;
	return value.length > minimum;
};


//==================================================================================
//	Maximum length
//==================================================================================
/**
 * Check length of a string does not exceed a value
 * @param {String} value - Value to be tested
 * @param {Number} [maximum] - Count to be used (defaults to {@link ValueValidation.options.maxLength})
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("email");
 * let value = element.value;
 * if (valid.maxLength(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.maxLength = function(value, maximum) {
    const validation = this;

	maximum = maximum ? maximum : validation.options.maxLength;
	return value.length < maximum;
};


//==================================================================================
//	Time
//==================================================================================
/**
 * Check a value matches the time format "00:00"
 * @param {String} value - Value to be tested
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("time");
 * let value = element.value;
 * if (valid.time(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.time = function(value) {
    const validation = this;
	return validation.regexp.time.test(value);
};


//==================================================================================
//	Currency
//==================================================================================
/**
 * Check a value matches the currency format "00.00"
 * @param {String} value - Value to be tested
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("price");
 * let value = element.value;
 * if (valid.currency(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.currency = function(value) {
    const validation = this;
	return validation.regexp.currency.test(value);
};


//==================================================================================
//	Email
//==================================================================================
/**
 * Check a value is a valid email address
 * @param {String} value - Value to be tested
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("email");
 * let value = element.value;
 * if (valid.email(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.email = function(value) {
    const validation = this;
    return validation.regexp.email.test(value);
};


//==================================================================================
//	Web address
//==================================================================================
/**
 * Check a value is a valid URL
 * @param {String} value - Value to be tested
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("url");
 * let value = element.value;
 * if (valid.url(value)) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.url = function(value) {
    const validation = this;
    return validation.regexp.url.test(value);
};


//==================================================================================
//	Postcodes
//==================================================================================
/**
 * Check a value is a valid postcode from a specific region
 * @param {String} value - Value to be tested
 * @param {String} [country] - Country against which the postcode should be validated. Defaults to UK.
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("url");
 * let value = element.value;
 * if (valid.postcode(value, "UK")) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.postcode = function(value, country) {
    const validation = this;
    country = country ? country : "UK";
    return validation.regexp.postcode[country].test(value);
};


//==================================================================================
//	Postcodes
//==================================================================================
/**
 * Check a value is a valid postcode from a specific region
 * @param {String} value - Value to be tested
 * @param {String} [country] - Country against which the postcode should be validated. Defaults to UK.
 * @returns {Boolean}
 * @example
 * let valid = new ValueValidation();
 * let element = document.getElementById("url");
 * let value = element.value;
 * if (valid.postcode(value, "UK")) {
 *  //  Do something
 * }
 */
ValueValidation.prototype.intlpostcode = function(value) {
    const validation = this;
    return validation.regexp.intlpostcode.test(value);
};