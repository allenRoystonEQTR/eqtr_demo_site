//==================================================================================================
//
//	Carousel
//
//==================================================================================================

//==================================================================================================
//	Private methods
//==================================================================================================
/**
 * Logic to endlessly loop the carousel.
 * @private
 * @function loop
 * @memberof CarouselEndlessCSS
 * @param {CarouselEndlessCSS} endless - Our instance of the endless carousel module
 */
function loop(endless) {
    return function loopCarousel() {
        if (endless.carousel.position === endless.carousel.last) {
            endless.desired = endless.first;
        }
        else if (endless.carousel.position === 0) {
            endless.desired = endless.last;
        }
        else {
            endless.desired = undefined;
        }
    };
}

/**
 * Our event handler for the required transitionend event.
 * @private
 * @function transitionEnd
 * @memberof CarouselEndlessCSS
 * @param {CarouselEndlessCSS} endless - Our instance of the endless carousel module
 */
function transitionEnd(endless) {
    return function endlessTransitionEnd(event) {
        if (endless.desired !== undefined) {
            endless.updatePosition(endless.desired);
        }
        endless.carouselUpdateUI(endless.carousel);
        event.preventDefault();
    };
}

//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a carousel module which alters to DOM to produce the effect of an endless list of items
 * @constructor
 * @param {Carousel} element - The instance of {@link Carousel} to which this module is bound
 * @property {Carousel} carousel - Cached property to the instance of {@link Carousel}
 * @property {Number} visible - The number of visible elements within the carousel
 * @property {Number} last - Last item in our endless carousel differs from the base carousel, the true last position is reflected here
 * @property {Number} first - Unlike the base carousel, the first true item in our carousel will begin at position 1.
 * @property {Number} desired - This value is used by our "transitionend" event to set the desired location of our carousel
 * @property {Function} transitionEnd - Our "transitionend" event callback
 * @property {Function} carouselUpdateUI - This is the original carousel updateUI method. The base method is replaced when our endless module is bound.
 */
export const CarouselEndlessCSS = function(carousel) {
    const endless = this;

    endless.carousel = carousel;
    endless.visible = 1;

    endless.last = endless.carousel.last + 1;
    endless.first = 1;  //  This may change depending on the number of visible items

    endless.desired = undefined;

    endless.transitionEnd = transitionEnd(endless);

    endless.carouselUpdateUI = carousel.updateUI;
};


//==================================================================================================
//	Attach to our carousel
//==================================================================================================
/**
 * Attach our endless carousel logic to an existing carousel
 * @example
 * // Attach our endless module to the carousel
 * let carousel = new BasicCarousel(element, next, prev);
 * carousel.endless.attach();
 */
CarouselEndlessCSS.prototype.attach = function() {
    const endless = this;

    //  Update our updateUI method with our passthrough
    //  We have our original method cached within our constructor
    endless.carousel.updateUI = loop(endless);

    //  Clone our first element and place it after our last
    endless.carousel.container.appendChild(endless.carousel.items[0].cloneNode(true));

    //  Clone our last element and place it before our first
    endless.carousel.container.insertBefore(endless.carousel.items[endless.carousel.last].cloneNode(true), endless.carousel.items[0]);

    //  Update our carousel counts to reflect these DOM changes
    endless.carousel.updateCounts();

    //  Update our carousel widths to reflect these DOM changes
    endless.carousel.setWidths();

    //  Update our position to the first true non-repeated element
    endless.updatePosition(1);

    //  Update our UI to reflect carousel position changes
    endless.carouselUpdateUI(endless.carousel);

    //  Set our transitionend event
    endless.carousel.container.addEventListener("transitionend", endless.transitionEnd);

};


//==================================================================================================
//	Detach from our carousel
//==================================================================================================
/**
 * Attach our endless carousel logic to an existing carousel
 * @example
 * // Attach our endless module to the carousel
 * let carousel = new BasicCarousel(element, next, prev);
 * carousel.endless.attach();
 * carousel.endless.detach();
 */
CarouselEndlessCSS.prototype.detach = function() {
    const endless = this;

    //  Update our updateUI method with our passthrough
    //  We have our original method cached within our constructor
    endless.carousel.updateUI = endless.carouselUpdateUI;

    //  Clone our first element and place it after our last
    endless.carousel.container.removeChild(endless.carousel.items[0]);

    //  Clone our last element and place it before our first
    endless.carousel.container.removeChild(endless.carousel.items[endless.carousel.last]);

    //  Update our carousel counts to reflect these DOM changes
    endless.carousel.updateCounts();

    //  Update our carousel widths to reflect these DOM changes
    endless.carousel.setWidths();

    //  Update our position to the first true non-repeated element
    //  Note our assumption to return to the first position within our carousel - this may need reviewed
    endless.updatePosition(0);

    //  Update our UI to reflect carousel position changes
    endless.carouselUpdateUI(endless.carousel);

    //  Set our transitionend event
    endless.carousel.container.removeEventListener("transitionend", endless.transitionEnd);

};


//==================================================================================================
//	Update our carousel position without the use of transforms
//==================================================================================================
/**
 * Update our carousel position without the use of transforms
 * @param {Number} position - The desired new position of the carousel
 */
CarouselEndlessCSS.prototype.updatePosition = function(position) {
    const endless = this;

    endless.carousel.disableCSSAnimation();

    endless.carousel.previous = endless.carousel.position;
    endless.carousel.position = position;

    setTimeout(function delayReactivation() {
        endless.carousel.enableCSSAnimation();
    }, 0);
};