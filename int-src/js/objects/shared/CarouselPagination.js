//==================================================================================================
//
//	Carousel pagination
//
//==================================================================================================
import { ElementTemplates } from "objects/shared/ElementTemplates";


//==================================================================================================
//	Private methods
//==================================================================================================
/**
 * Click listener for each of our pagination elements
 * @private
 * @function click
 * @memberof CarouselPagination
 * @param {CarouselPagination} pagination - Our instance of the endless carousel module
 * @param {Number} index - The collection position for the element to which this event is bound
 */
function click(pagination, index) {
    return function paginationClick(event) {
        if (index !== pagination.carousel.position) {
            pagination.carousel.position = index;
        }
        event.preventDefault();
    };
}


/**
 * This update UI method will replace the carousel native method to ensure elements are updated.
 * @private
 * @function updateUI
 * @memberof CarouselPagination
 * @param {CarouselPagination} pagination - Our instance of the endless carousel module
 */
function updateUI(pagination) {
    return function paginationUpdateUI() {
        pagination.collection[pagination.carousel.previous].setAttribute("data-active", "false");
        pagination.collection[pagination.carousel.position].setAttribute("data-active", "true");
        pagination.carouselUpdateUI.call(pagination.carousel);
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a basic carousel providing 100% width transitions
 * @constructor
 * @param {Carousel} carousel - Instance of our carousel this object is bound to
 * @param {HTMLElement} container - Container element which will contain our pagination elements
 * @param {HTMLElement} template - Example pagination element to be repeated
 * @property {Carousel} carousel - Our local reference for the carousel instance
 * @property {HTMLElement} container - Our local reference to the pagination container
 * @property {ElementTemplates} template - Instance of {@link ElementTemplates} which stores our pagination element for cloning
 * @property {Function} carouselUpdateUI - This is the original carousel updateUI method. The base method is replaced when our endless module is bound.
 * @property {Array} collection - An array storing all of our pagination elements
 */
export const CarouselPagination = function(carousel, container, template) {
    const pagination = this;

    pagination.carousel = carousel;
    pagination.container = container;
    pagination.template = new ElementTemplates({ "element": template });

    pagination.updateUI = updateUI(pagination);
    pagination.carouselUpdateUI = carousel.updateUI;

    pagination.collection = new Array(pagination.carousel.count);

    pagination.max = false;

    pagination.attach();
};


//==================================================================================================
//	Initial logic
//==================================================================================================
/**
 * Method to be used when our pagination is created
 */
CarouselPagination.prototype.attach = function() {
    const pagination = this;

    if (pagination.carousel.count < 2) {
        pagination.hideUI();
        pagination.carousel.container.setAttribute("data-carousel-pagination", "false");
    }
    else {
        pagination.carousel.updateUI = pagination.updateUI;
        pagination.container.empty();
        pagination.container.appendChild(pagination.generate());
        pagination.showUI();
        pagination.carousel.container.setAttribute("data-carousel-pagination", "true");
    }
};


//==================================================================================================
//	Hide UI
//==================================================================================================
/**
 * Method to hide our pagination UI
 */
CarouselPagination.prototype.hideUI = function() {
    const pagination = this;
    pagination.container.addClass("hidden");
};


//==================================================================================================
//	Show UI
//==================================================================================================
/**
 * Method to show our pagination UI
 */
CarouselPagination.prototype.showUI = function() {
    const pagination = this;
    pagination.container.removeClass("hidden");
};


//==================================================================================================
//	Proxy update UI method
//==================================================================================================
/**
 * Our generate method will produce DOM elements to be appended to our pagination container
 */
CarouselPagination.prototype.generate = function() {
    const pagination = this;
    const fragment = document.createDocumentFragment();
    let clone;

    let position = 0;
    const count = pagination.carousel.count;

    for (position; position < count; position += 1) {

        clone = pagination.template.element;
        pagination.collection[position] = clone;

        if (position === pagination.carousel.position) {
            clone.setAttribute("data-active", "true");
        }

        clone.addEventListener("click", click(pagination, position));

        fragment.appendChild(clone);

    }

    return fragment;
};