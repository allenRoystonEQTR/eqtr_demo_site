//==================================================================================================
//
//	Google places API
//
//==================================================================================================
import { CatchError } from "objects/shared/CatchError";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * Wrapper for Google Map's class library
 * @constructor
 * @param {Function} fulfill - Fulfill callback for the AJAX promise
 * @param {Function} reject - Reject callback for the AJAX promise
 * @property {Object} service - Instances of google.maps objects
 * @property {Function} service.geocoder - Instance of Google Maps geocoder service (https://developers.google.com/maps/documentation/javascript/reference#Geocoder)
 * @property {Function} service.autocomplete - Instance of Google Maps geocoder service (https://developers.google.com/maps/documentation/javascript/reference#AutocompleteService)
 * @property {Object} query - Store of last used query parameters
 * @property {String} query.location - Last Geocoder location query sent to the web service
 * @property {String} query.place - Last Geocoder place ID query sent to the web service
 * @property {String} query.predictions - Last Autocomplete predictions query sent to the web service
 * @property {Object} results - Last results received from the web service
 * @property {Object} results.location - Last results received from Geocoder location lookup
 * @property {Object} results.place - Last results received from Geocoder place ID lookup
 * @property {Object} results.predictions - Last results received from Autocomplete predictions lookup
 * @property {String} country - Country code (ISO ALPHA-2) to be applied within componentRestrictions
 * @property {String} language - Language code (ISO 639-1) to be applied to relevant services
 * @property {Function} fulfill - Our local copy of the Fulfill callback
 * @property {Function} reject - Our local copy of the Reject callback
 */
export const GoogleMapsAPI = function(fulfill, reject) {
    const search = this;

    search.service = {
        "geocoder": new google.maps.Geocoder(),
        "autocomplete": new google.maps.places.AutocompleteService()
    };

    search.query = {
        "location": "",
        "place": ""
    };
    search.results = {};

    search.country = "GB";
    search.language = "en";

    search.fulfill = fulfill;
    search.reject = reject;
};


//	============================================================================
//	Retrieve location data
//	============================================================================
/**
 * Query Google's location lookup service
 * @param {String} query - Query parameters, likely user input
 * @example
 * let geo = new GoogleMapsAPI(fulfill, reject);
 * geo.getLocation("58 Elliot");
 */
GoogleMapsAPI.prototype.getLocation = function(query) {
    const search = this;

    //  Ajax call handled by Google framework
    search.service.geocoder.geocode(
        {
            "address": query,
            "componentRestrictions": {
                "country": search.country
            }
        },
        function geocodePromise(results, status) {
            //	Cache our latest query
            search.query.location = query;

            //	Resolve or reject the promise
            if (results) {
                search.results.location = results;
                return search.fulfill(results, "location");
            }
            else {
                return new CatchError(query, error, function errorHandling() {
                    /* eslint-disable no-invalid-this */
                    //	this bound to CatchError by constructor
                    search.reject(this, search, results, status, "location");
                    /* eslint-enable no-invalid-this */
                });
            }
        }
    );

    return search;

};


//	============================================================================
//	Retrieve location data
//	============================================================================
/**
 * Query Google's PlaceID lookup service
 * @param {String} placeID - Google Maps place ID to lookup
 * @example
 * let geo = new GoogleMapsAPI(fulfill, reject);
 * geo.getPlaceFromId("ChIJp-bvnypEiEgR4zF3yq9HDwU");
 */
GoogleMapsAPI.prototype.getPlaceFromId = function(placeID) {
    const search = this;

    //  Ajax call handled by Google framework
    search.service.geocoder.geocode(
        {
            "placeId": placeID
        },
        function geocodePromise(results, status) {
            //	Cache our latest query
            search.query.place = placeID;

            //	Resolve or reject the promise
            if (results) {
                search.results.place = results;
                return search.fulfill(results, status, "place");
            }
            else {
                return new CatchError(query, error, function errorHandling() {
                    /* eslint-disable no-invalid-this */
                    //	this bound to CatchError by constructor
                    search.reject(this, search, results, status, "place");
                    /* eslint-enable no-invalid-this */
                });
            }
        }
    );

    return search;

};


//	============================================================================
//	Retrieve location data
//	============================================================================
/**
 * Query Google's Autocomplete place predictions service
 * @param {String} query - String for lookup
 * @example
 * let geo = new GoogleMapsAPI(fulfill, reject);
 * geo.getPredictions("58 Elliot");
 */
GoogleMapsAPI.prototype.getPredictions = function(query) {
    const search = this;

    //  Ajax call handled by Google framework
    search.service.autocomplete.getPlacePredictions(
        {
            "input": query,
            "types": ["geocode"],
            "componentRestrictions": {
                "country": search.country
            },
            "language": search.language
        },
        function predictionsPromise(results, status) {
            //	Cache our latest query
            search.query.predictions = query;

            //	Resolve or reject the promise
            if (results) {
                search.results.predictions = results;
                return search.fulfill(results, "predictions");
            }
            else {
                return new CatchError(query, error, function errorHandling() {
                    /* eslint-disable no-invalid-this */
                    //	this bound to CatchError by constructor
                    search.reject(this, place, results, status, "predictions");
                    /* eslint-enable no-invalid-this */
                });
            }
        }
    );

    return search;
};