//==================================================================================================
//
//	Delegated events
//
//==================================================================================================

//==================================================================================================
//	Private methods & variables
//==================================================================================================
//  HTMLElement => { "eventType": [ callback() ] }
const DELEGATES = new WeakMap();



//==================================================================================================
//	Constructor
//==================================================================================================
export const DelegatedEvent = function(element, eventType, selector, callback) {
    const delegate = this;

    delegate.element = element;
    delegate.eventType = eventType;
    delegate.selector = selector;
    delegate.callback = callback;
    delegate.handler = delegate.listener();

    delegate.bind();
};


//==================================================================================================
//	Initial logic
//==================================================================================================
DelegatedEvent.prototype.bind = function() {
    const delegate = this;
    let eventStore = DELEGATES.get(delegate.element);

    //  If we do not have an existing event store for this element, create one
    if (!eventStore) {
        eventStore = {};
        DELEGATES.set(delegate.element, eventStore);
        //  Attach an attribute to reflect the presence of delegated events
        delegate.element.setAttribute("event-delegate", "true");
    }

    //  If we do not have an entry for this event type, create one & attach our event listener
    if (!eventStore[delegate.eventType]) {
        eventStore[delegate.eventType] = [delegate];
        delegate.element.addEventListener(delegate.eventType, delegate.handler);
    }

    //  Otherwise push our callback into the queue
    else {
        eventStore[delegate.eventType].push(delegate);
    }

};


//==================================================================================================
//	Listener
//==================================================================================================
/**
 * Event listener added to the document to iterate through our callbacks
 */
DelegatedEvent.prototype.listener = function() {
    const delegate = this;

    //  Return our event listener
    return function delegateListener(event) {
        const element = this;
        const parent = event.target.firstParent(delegate.selector);

        if (parent) {
            const eventStore = DELEGATES.get(delegate.element);

            eventStore[delegate.eventType].forEach(function iterateCallbacks(delegate) {
                if (delegate.selector && event.target.matches(delegate.selector)) {
                    delegate.callback.call(event.target, event, delegate);
                }
                else {
                    delegate.callback.call(element, event, delegate);
                }
            });

        }
    };
};