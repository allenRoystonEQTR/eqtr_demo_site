import { mergeObjectsWithCallback } from "helpers/shared/mergeObjectsWithCallback";
import { mergeObjects } from "helpers/shared/mergeObjects";
import { typeOf } from "helpers/shared/typeOf";

//==================================================================================================
//
//	Google map
//
//==================================================================================================
/**
 * Control options: {@link https://developers.google.com/maps/documentation/javascript/controls}
 * Style reference: {@link https://developers.google.com/maps/documentation/javascript/style-reference}
 */


//==================================================================================================
//	Private methods
//==================================================================================================
function standard(options) {

    //==================================================================================================
    //	Initial state & options
    //==================================================================================================

    //  Zoom
    options.zoom = 15;
    options.maxZoom = 18;

    //  Position
    options.center = {
        lat: 55.861668,
        lng: -4.278822
    };

    //  Look & feel
    options.styles = [];

    //  Interactions
    options.scrollwheel = false;
    options.gestureHandling = "cooperative";


    // //==================================================================================================
    // //	UI
    // //==================================================================================================

    //  Full screen
    options.fullscreenControl = false;
    options.fullscreenControlOptions = {
        "position": google.maps.ControlPosition.TOP_RIGHT
    };

    //  Map type
    options.mapTypeControl = false;
    options.mapTypeControlOptions = {
        "style": google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        "position": google.maps.ControlPosition.TOP_LEFT,
        "mapTypeIds": ["roadmap", "terrain"]
    };

    //  Rotations
    options.rotateControl = false;
    options.rotateControlOptions = {
        "position": google.maps.ControlPosition.LEFT_BOTTOM
    };

    //  Scaling
    options.scaleControl = false;
    options.scaleControlOptions = {
        "position": google.maps.ControlPosition.RIGHT_CENTER
    };

    //  Street view
    options.streetViewControl = false;
    options.streetViewControlOptions = {
        "position": google.maps.ControlPosition.RIGHT_BOTTOM
    };

    //  Zoom control
    options.zoomControl = false;
    options.zoomControl = {
        "position": google.maps.ControlPosition.BOTTOM_RIGHT
    };


    return options;

}

//  Set properties of instance according to options provided
function set(map, object, property) {
    return function setProperty(value) {
        Object.defineProperty(object, property, {
            enumerable: true,
            get: function getProperty() {
                return value;
            },
            set: function setProperty(update) {
                value = update;

                if (map.pending === false) {
                    map.pending = true;

                    setTimeout(function updateMap() {
                        map.update();
                        map.pending = false;
                    }, 0);

                }

                return value;
            }
        });
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
export const GoogleMapsView = function(element, options) {
    const map = this;

    //  Set our element
    Object.defineProperty(map, "element", {
        writable: true,
        value: element
    });

    Object.defineProperty(map, "service", {
        writable: true,
        value: new google.maps.Map(element)
    });

    Object.defineProperty(map, "pending", {
        writable: true,
        value: false
    });

    mergeObjectsWithCallback(map, map, mergeObjects(standard({}), options), set);
    map.update();
};


//	============================================================================
//	Update options
//	============================================================================
GoogleMapsView.prototype.update = function() {
    const map = this;

    map.service.setOptions(map);

    return map;
};


//	============================================================================
//	Normalise coordinates object
//	============================================================================
GoogleMapsView.prototype.coordinatesToObject = function(coords) {

    //  Comma delimited string
    if (typeOf(coords) === "string") {
        const parts = coords.split(",");

        return {
            lat: Number(parts[0]),
            lng: Number(parts[1])
        };
    }

    //  Object representation
    if ( (coords.lat && typeOf(coords.lat !== "function")) && (coords.lng && typeOf(coords.lng) !== "function") ) {
        return coords;
    }

    //  google.maps.LatLng
    if ( (coords.lat && typeOf(coords.lat === "function")) && (coords.lng && typeOf(coords.lng) === "function") ) {
        return {
            lat: coords.lat(),
            lng: coords.lng()
        };
    }

    //  console.warn("No valid coordinates found.");
    return false;

};


//	============================================================================
//	Coordinates to string representation
//	============================================================================
GoogleMapsView.prototype.coordinatesToString = function(coords) {

    //  Comma delimited string
    if (typeOf(coords) === "string") {
        return coords;
    }

    //  Object representation
    if ( (coords.lat && typeOf(coords.lat !== "function")) && (coords.lng && typeOf(coords.lng) !== "function") ) {
        return coords.lat +","+ coords.lng;
    }

    //  google.maps.LatLng
    if ( (coords.lat && typeOf(coords.lat === "function")) && (coords.lng && typeOf(coords.lng) === "function") ) {
        return coords.lat() +","+ coords.lng();
    }

    //  console.warn("No valid coordinates found.");
    return false;

};


//	============================================================================
//	Normalise coordinates object
//	============================================================================
GoogleMapsView.prototype.coordinatesToLatLng = function(coords) {

    //  Comma delimited string
    if (typeOf(coords) === "string") {
        const parts = coords.split(",");
        return new google.maps.LatLng(parts[0], parts[1]);
    }

    //  Object representation
    if ( (coords.lat && typeOf(coords.lat !== "function")) && (coords.lng && typeOf(coords.lng) !== "function") ) {
        return new google.maps.LatLng(coords.lat, coords.lng);
    }

    //  google.maps.LatLng
    if ( (coords.lat && typeOf(coords.lat === "function")) && (coords.lng && typeOf(coords.lng) === "function") ) {
        return coords;
    }

    //  console.warn("No valid coordinates found.");
    return false;

};


//	============================================================================
//	Parse coords string
//	============================================================================
GoogleMapsView.prototype.setCoordinates = function(coords) {
    const map = this;

    coords = map.normaliseCoordinates(coords);

    map.center.lat = coords.lat;
    map.center.lng = coords.lng;

    return map;
};


//	============================================================================
//	Parse coords string
//	============================================================================
GoogleMapsView.prototype.zoomTo = function(coords, zoom) {
    const map = this;

    coords = map.normaliseCoordinates(coords);

    map.center.lat = coords.lat;
    map.center.lng = coords.lng;

    if (zoom !== undefined) {
        map.zoom = zoom;
    }

    return map;
};