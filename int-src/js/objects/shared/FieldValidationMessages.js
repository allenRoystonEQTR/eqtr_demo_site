//==================================================================================================
//
//	Validation message
//
//==================================================================================================
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Private methods
//==================================================================================================
function defineText(store, element, text) {
    return function define() {
        return Object.defineProperty(store, "text", {
            get: function getText() {
                return text;
            },
            set: function setText(value) {
                text = value;
                element.innerHTML = value;
                return text;
            }
        });
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
export const FieldValidationMessages = function(field) {
    const message = this;
    let required = false;

    message.field = field;
    message.positive = {};
    message.negative = {};

    //TODO: Convert to Proxy when browser support is suitable
    Object.defineProperty(message, "showRequired", {
        get: function getRequired() {
            return required;
        },
        set: function setRequired(value) {
            required = value;
            message.negative.required.element.setAttribute("aria-hidden", !value);
            return value;
        }
    });

    message.init();
};



//==================================================================================================
//	Initial setup logic
//==================================================================================================
FieldValidationMessages.prototype.init = function() {
    const message = this;

    const positive = toArray(message.field.parent.querySelectorAll("[data-positive-validate-message]"));
    const negative = toArray(message.field.parent.querySelectorAll("[data-negative-validate-message]"));

    positive.forEach(function iteratePositive(element) {
        const test = element.getAttribute("data-positive-validate-message");
        message.positive[test] = {};
        message.positive[test]["element"] = element;
        defineText(message.positive[test], element, element.innerHTML)();
    });

    negative.forEach(function iteratePositive(element) {
        const test = element.getAttribute("data-negative-validate-message");
        message.negative[test] = {};
        message.negative[test]["element"] = element;
        defineText(message.negative[test], element, element.innerHTML)();
    });

};


//==================================================================================================
//	Check
//==================================================================================================
FieldValidationMessages.prototype.check = function() {
    const message = this;

    if (message.field.pass !== true) {

        const empty = message.field.element.value === "" ? true : false;

        if (message.field.required === true && empty === true) {
            message.hidePositive();
            message.hideNegative();
            message.showRequired = empty;
            message.field.parent.setAttribute("data-invalid", true);
        }

        else {

            if (empty !== true) {

                let valid = 0;
                message.field.tests.forEach(function iterateTests(test) {
                    const result = message.field.status[test];

                    if (result) {
                        valid = valid + 1;
                    }

                    if (message.negative[test]) {
                        message.negative[test].element.setAttribute("aria-hidden", result);
                    }

                });

                if (valid === message.field.tests.length) {
                    message.hidePositive();
                }
                else {
                    message.field.parent.setAttribute("data-invalid", true);
                }

            }

            else {
                message.hidePositive();
                message.hideNegative();
                message.field.parent.setAttribute("data-invalid", "");
            }

        }

    }

};


//==================================================================================================
//	Blur
//==================================================================================================
FieldValidationMessages.prototype.blur = FieldValidationMessages.prototype.check;


//==================================================================================================
//	Change
//==================================================================================================
FieldValidationMessages.prototype.change = FieldValidationMessages.prototype.check;


//==================================================================================================
//	Keyup event scenario
//==================================================================================================
FieldValidationMessages.prototype.input = function() {
    const message = this;
    const empty = message.field.element.value === "" ? true : false;

    if (message.field.required && empty === true) {
        message.field.parent.setAttribute("data-invalid", true);
        message.hidePositive();
        message.hideNegative();
        message.showRequired = empty;
    }
    else {
        if (empty !== true) {
            if (message.field.pass !== true) {
                message.hidePositive();
                message.hideNegative();
                message.field.parent.setAttribute("data-invalid", "");
            }
            else {
                message.field.parent.setAttribute("data-invalid", false);
                message.field.tests.forEach(function iterateTests(test) {
                    if (message.positive[test]) {
                        message.positive[test].element.setAttribute("aria-hidden", false);
                    }
                });
            }
        }
        else {
            message.hidePositive();
            message.hideNegative();
            message.field.parent.setAttribute("data-invalid", "");
        }
    }

};


//==================================================================================================
//	Hide all positive
//==================================================================================================
FieldValidationMessages.prototype.hidePositive = function() {
    const message = this;
    let property;

    for (property in message.positive) {
        if (message.positive.hasOwnProperty(property)) {
            message.positive[property].element.setAttribute("aria-hidden", true);
        }
    }

};


//==================================================================================================
//	Hide all negative
//==================================================================================================
FieldValidationMessages.prototype.hideNegative = function() {
    const message = this;
    let property;

    for (property in message.negative) {
        if (message.negative.hasOwnProperty(property)) {
            message.negative[property].element.setAttribute("aria-hidden", true);
        }
    }

};


//==================================================================================================
//	Add required
//==================================================================================================
FieldValidationMessages.prototype.addRequired = function() {
    const message = this;
    message.negative["required"] = {};
    message.negative["required"]["element"] = element;
    defineText(message.negative["required"], element, element.innerHTML)();
};


//==================================================================================================
//	Remove required
//==================================================================================================
FieldValidationMessages.prototype.removeRequired = function() {
    const message = this;
    delete message.negative["required"];
};