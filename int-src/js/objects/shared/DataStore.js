//==================================================================================================
//
//	Data store with HTMLElement binding
//
//==================================================================================================
import { typeOf } from "helpers/shared/typeOf";


//==================================================================================================
//	Private variables & methods
//==================================================================================================
/**
 * WeakMap store of all key:data
 * @private
 * @memberof DataStore
 */
const DATA_STORE = new WeakMap();


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a data store module which allows for the binding of data to any type of key.
 * Example use cases include the binding of instantiated objects to elements.
 * @constructor
 * @param {*} [key] - The key to be used for our data, this can be any data type (eg. HTMLElement)
 * @param {*} [value] - The data to be bound
 * @property {*} key - Our local copy of the key parameter passed to our constructor
 * @property {*} value - Our local copy of the value parameter passed to our constructor
 * @property {WeakMap} store - We expose our project-wide data store to instances of this object
 * @example
 * let carousel = new Carousel(el);
 * let data = new DataStore();
 * data.set(el, carousel);
 */
export const DataStore = function(key, value) {
    const data = this;

    data.key = key;
    data.value = value;
    data.store = DATA_STORE;

    if (key !== undefined && value !== undefined) {
        data.set(value, key);
    }

};


//==================================================================================================
//	Prepare values
//==================================================================================================
/**
 * Returns the data associated with a key
 * @param {*} [key] - An optional key argument can be passed to get data, if not provided the instantiated object value is used
 */
DataStore.prototype.get = function(key) {
    const data = this;
    if (key !== undefined) {
        data.key = key;
    }
    return DATA_STORE.get(data.key);
};


//==================================================================================================
//	Next
//==================================================================================================
/**
 * Sets data
 * @param {*} value - Value to be set
 * @param {*} [key] - An optional key argument can be passed to set data, if not provided the instantiated object value is used
 */
DataStore.prototype.set = function(value, key) {
    const data = this;
    const keyType = typeOf(key);

    if (value !== undefined) {
        data.value = value;
    }

    if (key !== undefined) {
        if (keyType.indexOf("html") !== -1 || keyType.indexOf("svg") !== -1) {
            key.setAttribute("contains-data", "true");
        }
    }
    else {
        key = data.key;
    }

    return DATA_STORE.set(key, value);
};


//==================================================================================================
//	Previous
//==================================================================================================
/**
 * Deletes an entry from our key store
 *  @param {*} [key] - An optional key argument can be passed to delete data, if not provided the instantiated object value is used
 */
DataStore.prototype.delete = function(key) {
    const data = this;
    if (key === data.key) {
        data.value = null;
    }
    return DATA_STORE.delete(key ? key : data.key);
};