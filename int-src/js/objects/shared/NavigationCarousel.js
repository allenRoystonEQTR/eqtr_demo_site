//==================================================================================================
//
//	BFinance mobile navigation
//
//==================================================================================================
import { BasicCarouselJS } from "objects/shared/BasicCarouselJS";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Constructor
//==================================================================================================
export const NavigationCarousel = function(element) {
    const carousel = this;
    BasicCarouselJS.call(carousel, element, undefined, undefined);

    //  Initial carousel view used for reset
    carousel.initial = carousel.container.querySelector("[data-mobilenav][aria-hidden='false']").getAttribute("data-mobilenav");

    //  This array is supplemented by our animations
    carousel.showing = [carousel.initial];

    //  Our animating property is toggled in our event handlers & animations
    carousel.animating = false;

    //  These properties are set in our implementation
    carousel.triggers = {};
    carousel.views = {};

    //  Set all our non active items to opacity 0
    carousel.items.forEach(function iterateCarouselItems(item, index) {
        if (index !== carousel.position) {
            item.style.opacity = 0;
        }
    });

    carousel.setWidths();
};
NavigationCarousel.prototype = Object.create(BasicCarouselJS.prototype);
NavigationCarousel.prototype.constructor = NavigationCarousel;


//==================================================================================================
//	Prepare & event handlers
//==================================================================================================
NavigationCarousel.prototype.bind = function() {
    const carousel = this;

    //  Set up our intitial view
    const intialview = carousel.items[0].querySelector("[data-mobilenav]");
    carousel.views[intialview.getAttribute("data-mobilenav")] = intialview;

        //  Iterate across our carousel items and catalogue key elements, attaching appropriate handlers
    carousel.items.forEach(function iterateCarouselItems(item, index) {
        const triggerCollection = toArray(item.querySelectorAll("[data-mobilenav-trigger]"));

        //  Iterate across our trigger elements
        triggerCollection.forEach(function iterateTriggers(trigger) {
            const identifier = trigger.getAttribute("data-mobilenav-trigger");
            const view = carousel.container.querySelector("[data-mobilenav='"+ identifier +"']");

            //  Update our references
            carousel.triggers[identifier] = trigger;
            carousel.views[identifier] = view;
            //  Attach our event handlers
            trigger.addEventListener("click", carousel.clickHandler(index + 1, identifier));
        });

        //  Configure our back button
        const backButton = item.querySelector("[data-mobilenav-back]");
        if (backButton) {
            backButton.addEventListener("click", carousel.backHandler(index - 1));
        }

    });

};


//==================================================================================================
//	Animations
//==================================================================================================
NavigationCarousel.prototype.transform = function() {
    const carousel = this;
    const timeline = carousel.anime.timeline();
    const currentView = carousel.views[carousel.showing[carousel.position]];
    const duration = 400;

    if (carousel.animating === true) {

        if (currentView) {
            currentView.setAttribute("aria-hidden", false);
        }

        timeline.add({
            targets: carousel.container,
            easing: "easeOutQuad",
            duration: duration,
            translateX: [
                - carousel.goto(carousel.previous) + "%",
                - carousel.goto(carousel.position) + "%"
            ]
        })
        .add({
            targets: carousel.items[carousel.position],
            easing: "easeOutQuad",
            duration: duration,
            offset: "-="+ duration,
            opacity: [
                0,
                1
            ]
        })
        .add({
            targets: carousel.items[carousel.previous],
            easing: "easeOutQuad",
            duration: duration,
            offset: "-="+ duration * 2,
            opacity: [
                1,
                0
            ],
            complete: function animeEnd() {
                if (carousel.toHide && carousel.toHide !== carousel.initial) {
                    carousel.views[carousel.toHide].setAttribute("aria-hidden", true);
                }
                carousel.toHide = "";
                carousel.animating = false;
            }
        });
    }

};


//==================================================================================================
//	Event handlers
//==================================================================================================
NavigationCarousel.prototype.clickHandler = function(position, identifier) {
    const carousel = this;
    return function click(event) {
        if (carousel.animating === false) {
            carousel.animating = true;
            carousel.showing.push(identifier);
            carousel.position = position;
        }
        event.preventDefault();
    };
};

NavigationCarousel.prototype.backHandler = function(position) {
    const carousel = this;
    return function backHandler(event) {
        if (carousel.animating === false) {
            carousel.animating = true;
            carousel.toHide = carousel.showing.pop();
            carousel.position = position;
        }
        event.preventDefault();
    };
};