//==================================================================================================
//
//	Carousel timer object
//
//==================================================================================================
import { CatchError } from "objects/shared/CatchError";


//==================================================================================================
//	Private methods
//==================================================================================================
/**
 * Our setTimeout wrapper method. This contains default logic for the timer which can be overriden by the {@link CarouselTimer} logic property.
 * @private
 * @function timed
 * @memberof CarouselTimer
 * @param {Carousel} carousel - Our carousel instance
 * @param {CarouselTimer} timer - Our instance of {@link CarouselTimer}
 */
function timed(carousel, timer) {
    return function timedCarouselUpdate() {

        timer.timeout = setTimeout(function carouselTimer() {

            if (timer.logic) {
                timer.logic(carousel, timer);
            }
            else {
                if (carousel.position === carousel.last) {
                    carousel.position = 0;
                }
                else {
                    carousel.position = carousel.position + 1;
                }
            }

            //  Call self
            timer.method();

        }, timer.interval);

    };
}

//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a carousel module which transitions the carousel after a defined time period
 * @constructor
 * @param {Carousel} element - The instance of {@link Carousel} to which this module is bound
 * @property {Carousel} carousel - Cached property to the instance of {@link Carousel}
 * @property {Number} interval - The setTimeout interval (miliseconds) before our carousel transitions. This defaults to 10 seconds.
 * @property {Function} timeout - The instance of timeout currently set in the event queue
 * @property {Function} method - The method by which our timer is created
 * @property {Function} [logic] - A callback with custom logic to be fired within our setTimeout
 */
export const CarouselTimer = function(carousel) {
    const timer = this;

    timer.carousel = carousel;
    timer.interval = 10000;

    timer.timeout = undefined;

    timer.method = timed(carousel, timer);
    timer.logic = undefined;  //  Optional argument
};


//==================================================================================================
//	Start timer
//==================================================================================================
/**
 * Attach our carousel timer
 * @param {Number} [interval] - Define the interval for our timer
 * @param {Function} [method] - Define custom setTimeout logic
 * @example
 * // Attach our timer
 * let carousel = new BasicCarousel(element, next, prev);
 * carousel.timer.attach(1000);
 */
CarouselTimer.prototype.attach = function(interval, method) {
    const timer = this;

    if (interval === undefined && timer.interval === undefined) {
        return new CatchError(timer, function nointerval() {
            /* eslint-disable no-console */
            console.warn("No interval set on carousel timer.");
            /* eslint-enable no-console */
        });
    }

    if (interval) {
        timer.interval = interval;
    }

    if (method) {
        carousel.method = method;
    }

    return timer.method();
};


//==================================================================================================
//	Stop timer
//==================================================================================================
/**
 * Detach our carousel timer
 * @example
 * // Detach our timer
 * let carousel = new BasicCarousel(element, next, prev);
 * carousel.timer.attach(1000);
 * carousel.timer.detach();
 */
CarouselTimer.prototype.detach = function() {
    const timer = this;
    clearTimeout(timer.timeout);
    timer.timeout = undefined;
};