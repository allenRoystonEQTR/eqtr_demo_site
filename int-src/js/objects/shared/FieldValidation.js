//==================================================================================================
//
//	Form Validation
//
//==================================================================================================
import { toBoolean } from "helpers/shared/toBoolean";
import { dataStore } from "helpers/globals";


//==================================================================================================
//	Change handlers
//==================================================================================================
function blur(field) {
    return function blurHandler() {
        field.check();
        field.messaging.blur();
    };
}

function change(field) {
    return function changeHandler() {
        field.check();
        field.messaging.change();
    };
}

function input(field) {
    return function inputHandler() {
        field.check();
        field.messaging.input();
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
export const FieldValidation = function(element, callback) {
    const field = this;
    const tests = element.getAttribute("data-validate-as");
    let required = toBoolean(element.getAttribute("aria-required"));

    field.element = element;
    field.name = element.name;
    field.tag = element.tagName.toLowerCase();
    field.parent = document.querySelector("[data-validate-for='"+ element.name +"']");
    field.required = toBoolean(element.getAttribute("aria-required"));
    field.tests = tests ? tests.replace(" ", "").split(",") : [];
    field.status = {};
    field.pass = false;

    field.callback = callback;

    field.validation = undefined;
    field.messaging = undefined;

    //TODO: Convert to Proxy when browser support is suitable
    Object.defineProperty(field, "required", {
        get: function getRequired() {
            return required;
        },
        set: function setRequired(value) {
            required = value;

            field.element.setAttribute("aria-required", value);
            if (value === true) {
                field.status["required"] = false;
                field.messages.addRequired();
            }
            else {
                delete field.status["required"];
                field.messages.removeRequired();
            }

            return value;
        }
    });

    Object.defineProperty(field, "active", {
        get: function getActive() {
            return field.element.getAttribute("data-validate-active");
        },
        set: function setActive(value) {
            field.element.setAttribute("data-validate-active", value);
            return value;
        }
    });

    field.init();
};


//==================================================================================================
//	Set up our validation
//==================================================================================================
FieldValidation.prototype.setValidation = function(Validation, Messaging) {
    const field = this;
    field.validation = new Validation();
    field.messaging = new Messaging(field);
};


//==================================================================================================
//	Initial logic
//==================================================================================================
FieldValidation.prototype.init = function() {
    const field = this;

    //  Populate field.messages & field.status with each test
    field.tests.forEach(function createStatus(test) {
        field.status[test] = false;
    });

    //  If this is a required field, also include it's message
    if (field.required === true) {
        field.status["required"] = false;
    }

    //  Set our event handlers
    field.element.addEventListener("change", change(field));
    field.element.addEventListener("input", input(field));
    field.element.addEventListener("blur", blur(field));

    //  Bind our data
    dataStore.set(field, field.element);

    //  Return our instance
    return field;
};


//==================================================================================================
//	Check
//==================================================================================================
FieldValidation.prototype.check = function() {
    const field = this;

    //  Check our field against each of it's assigned tests
    if (field.active !== false) {

        const empty = field.element.value === "" ? true : false;

        //  If our field is required & is empty
        if (field.required === true && empty === true) {
            field.status.required = field.validation.required(field.element.value);
            field.pass = false;
        }

        //  Otherwise
        else {

            //  If our field is not required & empty, allow to pass
            if (field.required === false && empty === true) {
                field.pass = true;
            }

            //  Otherwise if the field is not empty, check the content & pass accordingly
            else {

                let valid = 0;
                let result;

                //  Iterate across our tests
                field.tests.forEach(function iterateTests(test) {

                    result = field.validation[test](field.element.value);
                    field.status[test] = result;

                    if (result) {
                        valid = valid + 1;
                    }

                });

                //  Set our pass parameter
                field.pass = valid === field.tests.length ? true : false;

            }

        }

    }

    //  Fire our callback if one has been specified
    if (field.callback) {
        field.callback(field);
    }

    //  Allow for our status to be returned, can be used for assignment if needed
    return field.pass;

};