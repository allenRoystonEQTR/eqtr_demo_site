//==================================================================================================
//
//	Form Validation
//
//==================================================================================================
import { toArray } from "helpers/shared/toArray";
import { dataStore } from "helpers/globals";


//==================================================================================================
//	Constructor
//==================================================================================================
export const FormValidation = function(element, callback) {
    const form = this;
    let ready = false;

    form.element = element;
    form.callback = callback;

    form.button = element.querySelector("[data-form-submit='"+ element.getAttribute("data-form") +"']");

    form.fields = toArray(element.querySelectorAll("[data-validate-as]"));
    form.validation = undefined;

    //TODO: Convert to Proxy when browser support is suitable
    Object.defineProperty(form, "ready", {
        get: function getReady() {
            return ready;
        },
        set: function setReady(value) {
            if (ready !== value) {
                ready = value;
            }
            if (form.callback) {
                form.callback(form);
            }
        }
    });

    dataStore.set(form, form.element);
};


//==================================================================================================
//	Check form status
//==================================================================================================
FormValidation.prototype.setValidation = function(FieldValidation, ValueValidation, Messaging) {
    const form = this;

    form.validation = form.fields.map(function setValidation(field) {
        const validation = new FieldValidation(field);
            validation.setValidation(ValueValidation, Messaging);

        return validation;
    });

};


//==================================================================================================
//	Check form status
//==================================================================================================
FormValidation.prototype.check = function() {
    const form = this;
    let valid = 0;

    //  Iterate across our fields
    form.validation.forEach(function iterateValidation(field) {
        if (field.active !== false) {

            field.check();
            field.messaging.check();

            if (field.pass === true) {
                valid = valid + 1;
            }

        }
    });

    //  Set our ready value
    form.ready = valid === form.validation.length ? true : false;

    //  Return our state in case there is a desire to use function within an assignment
    return form.ready;

};


//==================================================================================================
//	Generate "application/x-www-form-urlencoded" string
//==================================================================================================
FormValidation.prototype.urlencoded = function() {
    const form = this;

    const query = form.validation.map(function iterateFields(field) {
        if (field.element.value !== "") {
            return encodeURIComponent(field.name) +"="+ encodeURIComponent(field.element.value);
        }
    });

    if (query.length > 0) {
        return query.join("&");
    }

    return false;

};


//==================================================================================================
//	Generate JSON object
//==================================================================================================
FormValidation.prototype.json = function() {
    const form = this;
    const response = {};

    form.validation.forEach(function iterateFields(field) {
        if (field.element.value !== "") {
            return response[field.name] = field.element.value;
        }
    });

    if (Object.keys(response).length > 0) {
        return JSON.stringify(response);
    }

    return false;

};