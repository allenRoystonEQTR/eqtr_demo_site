//==================================================================================================
//
//	Dynamic Builder
//
//==================================================================================================
import { Builder } from "objects/shared/Builder";
import { typeOf } from "helpers/shared/typeOf";


//==================================================================================================
//	Private methods
//==================================================================================================
/**
 * Assigns a get;set; to our state object.
 * @private
 * @function assignValue
 * @memberof DynamicBuilder
 * @param {DynamicBuilder} builder - Instance of our {@link DynamicBuilder}
 * @param {Object} data - Data object
 */
function assignValue(builder, data) {
    return function assignElementValue(object, property) {
        Object.defineProperty(object, property, {
            enumerable: true,
            get: function getElement() {
                return data[property];
            },
            set: function setElement(update) {
                data[property] = update;

                if (builder.pending === false) {
                    builder.pending = true;

                    setTimeout(function buildDOMElement() {
                        //  Create our element clone
                        const clone = builder.element.cloneNode(true);

                        //  We replace our element to reduce DOM writes;
                        //  Note the use of builder.data here rather than the passed data variable
                        //  This allows for nested objects using the safe assumption the element will always be fully replaced by changes for performance reasons.
                        builder.element.parentNode.replaceChild(builder.populate(clone, builder.data), builder.element);
                        builder.element = clone;

                        //  Reset our pending attribute
                        builder.pending = false;
                    });

                }

                return data[property];
            }
        });
    };
}

//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * Extending our Builder object, the DynamicBuilder reflects state.
 * When state changes the DOM is updated.
 * @constructor
 * @extends Builder
 * @param {HTMLElement} element - Template element to be used for population
 * @param {Object} data - Values used to populate our element. This is converted into a state object and assigned to our object's data property.
 * @property {HTMLElement} element - Element passed to our constructor
 * @property {Object} data - Current element state. When changed values in this update the DOM.
 * @property {Boolean} pending - Used to infer whether value updates are pending. This creates a performant approach to DOM updates, limiting the number of writes.
 * @example
 * let builder = new DynamicBuilder(document.querySelector(".template"), { key: "value" });
 * let fragment = document.createDocumentFragment();
 * fragment.appendChild(builder.populate());
 * document.body.appendChild(fragment);
 */
export const DynamicBuilder = function(element, data) {
    const builder = this;
    Builder.call(builder);

    builder.element = element;
    builder.data = builder.createState(data);
    builder.pending = false;
};

DynamicBuilder.prototype = Object.create(Builder.prototype);
DynamicBuilder.prototype.constructor = DynamicBuilder;


//==================================================================================================
//	Create dynamic object which updates our element when values change
//==================================================================================================
/**
 * Populates the {@link DynamicBuilder}.data property with a state object.
 * This support nested objects by being a self absorbing function.
 * @param {Object} data - Initial data used for population
 */
DynamicBuilder.prototype.createState = function(data) {
    const builder = this;
    const assign = assignValue(builder, data);
    const result = {};
    let property;

    for (property in data) {
        if (data.hasOwnProperty(property)) {
            if (typeOf(data[property]) === "object") {
                result[property] = builder.createState(data[property]);
            }
            else {
                assign(result, property);
            }
        }
    }

    return result;
};