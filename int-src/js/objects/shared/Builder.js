//==================================================================================================
//
//	Builder
//
//==================================================================================================
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * A builder object which can provide population of DOM elements (eg. clones)
 * A callback convention is used for the type of data & associated prototype.
 * ie.  "text" => Builder.prototype.text
 * When extending, default callbacks can be overwritten and additional callbacks added
 * @constructor
 * @property {Object} attributes - Contains key:value pairs mapping element attributes to callbacks
 * @property {String} delimiter - Concatenation characters to be used when multiple data keys are in use (eg. "make+model")
 * @example
 * let results = [{ key: "value" }, { key: "value" }];
 * let template = document.querySelector(".template");
 * let builder = new Builder();
 * let fragment = document.createDocumentFragment();
 * results.forEach(function iterateResults(result) {
 *   fragment.appendChild(
 *     //  Populate our element with data
 *     builder.populate(template.cloneNode(true), result);
 *   );
 * });
 * document.body.appendChild(fragment);
 */
export const Builder = function() {
    const builder = this;

    //  The keys for this attributes map to an object prototype by convention
    builder.attributes = {
        "text": "data-field",
        "link": "data-field-link",
        "image": "data-field-image",
        "background": "data-field-background"
    };

    builder.delimiter = " ";

};


//==================================================================================================
//	Populate method
//==================================================================================================
/**
 * Data population method.
 * This will iterate over a passed element, looking for data attributes and calling the relevant callback defined by the keys in builder.attributes
 * @param {String} element - HTMLElement used for population
 * @param {String} data - Data used for population
 * @param {Array|NodeList} [collection] - Optional list of elements to be iterated upon
 */
Builder.prototype.populate = function(element, data, collection) {
    const builder = this;
    const types = Object.keys(builder.attributes);
    let attributes = [];
    let key, selectors;

    //  If we do do not have values passed, assume these are properties on our object
    element = element ? element : builder.element;
    data = data ? data : builder.data;

    //  Create DOM selectors from our data attributes
    if (collection === undefined) {
        selectors = types.map((key) => {
            attributes.push(builder.attributes[key]);
            return "["+ builder.attributes[key] +"]";
        });
        collection = toArray(element.querySelectorAll(selectors.join(", ")));
    }
    else {
        attributes = Object.keys(builder.attributes).map((key) => builder.attributes[key]);
    }

    //  Iterate across our collection & fire a callback, named by convention
    collection.forEach((element) => {
        let count = attributes.length;
        let nested, split, result;

        while(count) {
            count = count - 1;
            key = element.getAttribute(attributes[count]);
            //  Check the element has our attribute
            if (key) {

                //  If we are looking for a single value
                if (key.indexOf("+") === -1) {
                    //  Check we have data matching our key, and that a suitable prototype for population exists
                    if (data[key] && builder[types[count]]) {
                        builder[types[count]](element, data[key]);
                    }
                }

                //  For multi-valued attributes, we split off "+"
                else {
                    split = key.split("+");
                    nested = split.length;
                    result = new Array(nested);

                    //  Iterate across our values, pushing them to a fixed length array
                    while(nested) {
                        nested = nested - 1;
                        if (data[split[nested]]) {
                            result[nested] = data[split[nested]];
                        }
                    }

                    //  Join our values with the builder delimiter
                    if (builder[types[count]]) {
                        builder[types[count]](element, result.join(builder.delimiter));
                    }
                }

            }

        }
    });

    return element;
};


//==================================================================================================
//	Default text prototype
//==================================================================================================
/**
 * Default callback for text population
 * @param {String} element - HTMLElement to be populated
 * @param {String} value - Value used for population
 */
Builder.prototype.text = function(element, value) {
    element.innerHTML = value;
};


//==================================================================================================
//	Default link prototype
//==================================================================================================
/**
 * Default callback for populating the HREF attribute of an anchor tag
 * @param {String} element - HTMLElement to be populated
 * @param {String} value - Value used for population
 */
Builder.prototype.link = function(element, value) {
    element.href = value;
};


//==================================================================================================
//	Default image prototype
//==================================================================================================
/**
 * Default callback for image population
 * @param {String} element - HTMLElement to be populated
 * @param {String} value - Value used for population
 */
Builder.prototype.image = function(element, value) {
    element.src = value;
};


//==================================================================================================
//	Default background prototype
//==================================================================================================
/**
 * Default callback for background image population
 * @param {String} element - HTMLElement to be populated
 * @param {String} value - Value used for population
 */
Builder.prototype.background = function(element, value) {
    element.style.backgroundImage = "url("+ value +")";
};