//==================================================================================================
//
//	Basic 100% width carousel
//
//==================================================================================================
import { Carousel } from "objects/shared/Carousel";
import anime from "animejs";
import Hammer from "hammerjs";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a basic carousel providing 100% width transitions
 * @constructor
 * @extends Carousel
 * @param {HTMLElement} element - Container element, this contains each individual view within the carousel
 * @param {HTMLElement} next - The next button element used for binds
 * @param {HTMLElement} previous - The previous button element used for binds
 * @param {Function} [callback] - Callback fired when a carousel position change occurs
 * @property {Function} callback - Our local instance of callback parameter
 * @property {Object} buttons - Object which contains our button references
 * @property {Boolean} hasButtons - Indicates whether the carousel has button UI elements. This may not be the case for mobile exclusive carousels.
 * @property {HTMLElement} buttons.next - Our local reference to the passed next button
 * @property {HTMLElement} buttons.prev - Our local reference to the passed previous button
 * @example
 * let container = document.querySelector(".carousel");
 * let next = document.querySelector(".carousel__button--next");
 * let previous = document.querySelector(".carousel__button--prev");
 * let carousel = new BasicCarouselJS(container, next, prev);
 * carousel.setWidths();
 */
export const BasicCarouselJS = function(element, next, prev, callback) {
    const carousel = this;

    //  Inheritance
    Carousel.call(carousel, element, callback);

    //  Buttons
    carousel.buttons = {
        "next": next,
        "prev": prev
    };
    carousel.hasButtons = carousel.buttons.next && carousel.buttons.prev ? true : false;

    //  Animation library
    carousel.anime = anime;

    //  Fire base class intitial logic
    carousel.init();
};

BasicCarouselJS.prototype = Object.create(Carousel.prototype);
BasicCarouselJS.prototype.constructor = BasicCarouselJS;



//==================================================================================================
//	Bind UI events
//==================================================================================================
/**
 * Binds events to our UI.
 * This method is invoked by our constructor, and could be treated as an init() method
 */
BasicCarouselJS.prototype.bind = function() {
    const carousel = this;


    //  Check carousel length
    if (carousel.items.length > 1) {

        //  Bind our event handlers
        //  Next button
        if (carousel.buttons.next) {
            carousel.buttons.next.addEventListener("click", function(event) {
                const position = carousel.position + 1;

                if (position <= carousel.last) {
                    carousel.position = position;
                }

                event.preventDefault();
            });
        }

        //  Previous button
        if (carousel.buttons.prev) {
            carousel.buttons.prev.addEventListener("click", function(event) {
                const position = carousel.position - 1;

                if (position >= 0) {
                    carousel.position = position;
                }

                event.preventDefault();
            });
        }

    }

};


//==================================================================================================
//	Bind touch UI events
//==================================================================================================
/**
 * Binds touch handler events to our UI.
 */
BasicCarouselJS.prototype.bindTouch = function() {
    const carousel = this;

    const touch = new Hammer(carousel.container);
    touch.on("swipe", carousel.swipe(carousel));

    return carousel;
};


//==================================================================================================
//	Bind all UI listeners
//==================================================================================================
/**
 * Binds all handlers.
 */
BasicCarouselJS.prototype.bindAll = function() {
    const carousel = this;

    carousel.bind();
    carousel.bindTouch();

    return carousel;
};


//==================================================================================================
//	Private methods
//==================================================================================================
/**
 * Animation method which is fired when a position change occurs
 */
BasicCarouselJS.prototype.transform = function() {
    const carousel = this;
    carousel.anime(carousel.animationOptions());
};

//==================================================================================================
//	Update UI state on position change
//==================================================================================================
/**
 * Default update UI method which is fired when a position change occurs
 */
BasicCarouselJS.prototype.updateUI = function() {
    const carousel = this;

    if (carousel.hasButtons === true) {

        //  UI state for first carousel position
        if (carousel.position === 0) {
            carousel.buttons.prev.addClass("inactive");
        }
        else {
            carousel.buttons.prev.removeClass("inactive");
        }

        //  UI state for last carousel position
        if (carousel.position === carousel.count - 1) {
            carousel.buttons.next.addClass("inactive");
        }
        else {
            carousel.buttons.next.removeClass("inactive");
        }

    }
};


//==================================================================================================
//	Animation options
//==================================================================================================
/**
 * This provided a default set of animation options for our basic carousel
 * @returns {Object} - Returns an options object used by AnimeJS
 */
BasicCarouselJS.prototype.animationOptions = function() {
    const carousel = this;
    return {
        targets: carousel.container,
        easing: "easeOutCirc",
        duration: 200,
        translateX: [
            - carousel.goto(carousel.previous) + "%",
            - carousel.goto(carousel.position) + "%"
        ]
    };
};


//==================================================================================================
//	Default swipe method for touch screen displays
//==================================================================================================
/**
 * Default touch event handler for direction movement
 * @returns {Function}
 */
BasicCarouselJS.prototype.swipe = function() {
    const carousel = this;
    return function swipeEvent(event) {
        if (event.deltaX > 0 && carousel.position > 0) {
            carousel.position = carousel.position - 1;
        }
        if (event.deltaX < 0 && carousel.position < carousel.last) {
            carousel.position = carousel.position + 1;
        }
    };
};