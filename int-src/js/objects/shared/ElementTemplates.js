//==================================================================================================
//
//	Element Templates
//
//==================================================================================================

//==================================================================================================
//	Private methods & variables
//==================================================================================================
/**
 * Assigns a get;set; to the instance of our object
 * @private
 * @function assignElement
 * @memberof ElementTemplates
 * @param {String} property - Name of the element to be assigned
 * @param {HTMLElement|SVGElement} element - Element which will be cloned
 */
function assignElement(property, element) {
    return function assignElementValue(object) {
        Object.defineProperty(object, property, {
            enumerable: true,
            get: function getElement() {
                return element.cloneNode(true);
            },
            set: function setElement(value) {
                templates.map[property] = value;
                element = value;
            }
        });
    };
}

/**
 * Iterates across our element map to create a fresh collection of references for cloning
 * @private
 * @function cloneElements
 * @memberof ElementTemplates
 * @param {Object} object - Object to assign our cloned elements to
 * @param {Object} map - Element map from which to retrieve our elements for cloning, it advised that all keys be capitalised (eg. CAROUSEL)
 */
function cloneElements(object, map) {
    let property;
    for (property in map) {
        if (map.hasOwnProperty(property)) {
            assignElement(property, map[property])(object);
        }
        else {
            templates[property] = false;
        }
    }

    return object;
}


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a clone store, which with literal assignments provides a fresh clone
 * @constructor
 * @param {Object} map - Object containing string keys & element values
 * @example
 * let templates = new ElementTemplates({
 *   CAROUSEL: document.querySelector(".templates .carousel")
 * });
 * let myCarouselClone = templates.CAROUSEL;
 */
export const ElementTemplates = function(map) {
    const templates = this;

    cloneElements(templates, map);
};


//==================================================================================================
//	Add element to templates
//==================================================================================================
/**
 * This adds an element to clone store, after the object has already been instantiated
 * @param {String} property - Name of the element to be assigned
 * @param {HTMLElement|SVGElement} element - Element which will be cloned
 */
ElementTemplates.prototype.add = function(property, element) {
    const templates = this;
    const define = assignElement(property, element);
    define(templates);
};


//==================================================================================================
//	Add element to templates
//==================================================================================================
/**
 * This removes an element from the clone store
 * @param {String} property - Key of the element to be removed
 */
ElementTemplates.prototype.remove = function(property) {
    const templates = this;

    delete templates[property];
};