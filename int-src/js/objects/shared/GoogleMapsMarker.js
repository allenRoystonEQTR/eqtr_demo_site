import { typeOf } from "helpers/shared/typeOf";

//==================================================================================================
//
//	Google map markers
//  Designed to be used with our Google Map View object
//
//==================================================================================================
/**
 * Markers: {@link https://developers.google.com/maps/documentation/javascript/custom-markers}
 * Symbols: {@link https://developers.google.com/maps/documentation/javascript/symbols}
 * Icons: {}
 */


//==================================================================================================
//	Constructor
//  Note: This object has been designed to benefit enumeration, only reflecting properties required by google.maps.Marker
//==================================================================================================
export const GoogleMapsMarker = function(options, info, infoOffset) {
    const marker = this;
    let pending = false;
    let pin = undefined;
    let listener = undefined;

    //  Ensure our position is a LatLng object
    options.position = options.map.coordinatesToLatLng(options.position);

    //  Ensure our InfoWindow pixel offset is an instance of Size()
    infoOffset = (infoOffset) ? new google.maps.Size(infoOffset.x, infoOffset.y) : undefined;

    //  Current view update state
    Object.defineProperty(marker, "pending", {
        get: function getInformation() {
            return pending;
        },
        set: function setInformation(value) {
            pending = value;
            return pending;
        }
    });

    //  Instance of GoogleMap Marker
    Object.defineProperty(marker, "pin", {
        get: function getPin() {
            return pin;
        },
        set: function setPin(value) {
            pin = value;
            return pin;
        }
    });

    //  GoogleMapsView instance
    Object.defineProperty(marker, "map", {
        enumerable: true,
        get: function getMap() {
            return options.map.service;
        },
        set: function setMap(value) {
            options.map = value;
            marker.update();
            return options.map;
        }
    });

    //  Location
    Object.defineProperty(marker, "position", {
        enumerable: true,
        get: function getPosition() {
            return options.position;
        },
        set: function setPosition(value) {
            options.position = map.coordinatesToLatLng(value);
            marker.update();
            return options.position;
        }
    });

    //  Icon
    Object.defineProperty(marker, "icon", {
        enumerable: true,
        get: function getIcon() {
            return options.icon;
        },
        set: function setIcon(value) {
            options.icon = value;
            marker.update();
            return options.icon;
        }
    });

    //  Icon
    Object.defineProperty(marker, "label", {
        enumerable: true,
        get: function getLabel() {
            return options.label;
        },
        set: function setLabel(value) {
            options.label = value;
            marker.update();
            return options.label;
        }
    });

    //  Title
    Object.defineProperty(marker, "title", {
        enumerable: true,
        get: function getTitle() {
            return options.title;
        },
        set: function setTitle(value) {
            options.title = value;
            marker.update();
            return options.title;
        }
    });


    //  Information markup
    Object.defineProperty(marker, "info", {
        get: function getInfo() {
            return info;
        },
        set: function setInfo(value) {
            info = value;
            marker.infoWindow = info;
            return info;
        }
    });

    //  Information markup
    Object.defineProperty(marker, "infoOffset", {
        get: function getInfoOffset() {
            return infoOffset;
        },
        set: function setInfoOffset(value) {
            infoOffset = new google.maps.Size(value.x, value.y);
            return infoOffset;
        }
    });

    //  Information panel
    let infoWindow;
    Object.defineProperty(marker, "infoWindow", {
        get: function getInfoWindow() {
            return infoWindow;
        },
        set: function setInfoWindow(value) {
            infoWindow = new google.maps.InfoWindow({
                content: value,
                position: marker.position,
                pixelOffset: marker.infoOffset
            });
            return infoWindow;
        }
    });

    //  Information panel event
    Object.defineProperty(marker, "listener", {
        get: function getHandler() {
            return listener;
        },
        set: function setHandler(value) {
            listener = value;
            return listener;
        }
    });


    //  Initialise our marker
    marker.init();
};


//	============================================================================
//	Set our intial states
//	============================================================================
GoogleMapsMarker.prototype.init = function() {
    const marker = this;

    //  Create our pin
    marker.update();

    //  Create our info panel
    if (marker.info !== undefined) {
        marker.infoWindow = marker.info;
    }

    return marker;
};


//	============================================================================
//	Add/update marker
//	============================================================================
GoogleMapsMarker.prototype.update = function() {
    const marker = this;

    if (marker.pending === false) {

        marker.pending = true;

        setTimeout(function updatePinTimeout() {

            //  Build our options
            const options = {};
            let property;

            for (property in marker) {
                if (marker.hasOwnProperty(property) && marker[property] !== undefined) {
                    options[property] = marker[property];
                }
            }

            //  Remove any pre-existing instance
            if (marker.pin !== undefined) {
                marker.remove();
            }

            //  Create our pin
            marker.pin = new google.maps.Marker(options);

            //  Attach our listener if one exists
            if (marker.info) {
                marker.addInfoWindow();
            }

            //  Update our pending state
            marker.pending = false;

        }, 0);

    }

    return marker;
};


//	============================================================================
//	Remove marker
//	============================================================================
GoogleMapsMarker.prototype.remove = function() {
    const marker = this;

    //  Remove our infoWindow if we have one
    marker.removeInfoWindow();

    //  Remove our pin and unset our object property
    marker.pin.setMap(null);
    marker.pin = undefined;


    return marker;
};


//	============================================================================
//	Add info window
//	============================================================================
GoogleMapsMarker.prototype.addInfoWindow = function() {
    const marker = this;

    //  Create our info panel
    if (marker.info !== undefined) {
        marker.infoWindow = marker.info;

        //  Preserve our click listener if one is not already set
        if (marker.listener !== undefined) {
            google.maps.event.removeHandler();
        }

        marker.addListener();

    }

    return marker;
};


//	============================================================================
//	Remove info window
//	============================================================================
GoogleMapsMarker.prototype.removeInfoWindow = function() {
    const marker = this;

    //  Create our info panel
    if (marker.listener !== undefined) {
        marker.infoWindow.close();
        marker.removeListener();
    }

    return marker;
};


//	============================================================================
//	Add click event to open info window
//	============================================================================
GoogleMapsMarker.prototype.addListener = function() {
    const marker = this;

    marker.listener = marker.pin.addListener("click", function clickHandler() {
        marker.infoWindow.setContent(marker.info);
        marker.infoWindow.open(marker.map, marker.instance);
    });

    return marker;
};

//	============================================================================
//	Remove click event to open info window
//	============================================================================
GoogleMapsMarker.prototype.removeListener = function() {
    const marker = this;

    google.maps.event.removeListener(marker.listener);
    marker.listener = undefined;

    return marker;
};