//==================================================================================================
//
//	Image lazy loading
//
//==================================================================================================
import { mergeObjects } from "helpers/shared/mergeObjects";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Private methods & variables
//==================================================================================================
/**
 * Callback to be fired for background images when the image resource has been loaded.
 * @private
 * @function background
 * @memberof LazyLoad
 * @param {HTMLElement} element - Element to which our background will be applied
 * @param {LazyLoad} lazyload - Instance of LazyLoad to be used
 */
function background(element, lazyload) {
    const src = lazyload.src;
    return function backgroundImage() {
        element.style.backgroundImage = "url("+ src +")";

        if (lazyload.callback) {
            lazyload.callback(element, lazyload);
        }
    };
}


/**
 * Callback for HTMLImageElements when the image resource has been loaded.
 * @private
 * @function insert
 * @memberof LazyLoad
 * @param {HTMLElement} parent - Parent element within which our HTMLImageElement should be inserted
 * @param {LazyLoad} lazyload - Instance of LazyLoad to be used
 */
function insert(image, parent, lazyload) {
    return function appendImage() {
        parent.insertBefore(image, parent.firstChild);

        if (lazyload.callback) {
            lazyload.callback(image, parent, lazyload);
        }
    };
}


/**
 * Callback for HTMLImageElements when the image resource is to be replaced.
 * @private
 * @function replace
 * @memberof LazyLoad
 * @param {HTMLElement} element - Element which should be replaced by our new HTMLImageElement
 * @param {HTMLElement} parent - Parent element within which our HTMLImageElement should be inserted
 * @param {LazyLoad} lazyload - Instance of LazyLoad to be used
 */
function replace(element, parent, lazyload) {
    return function appendImage() {
        const image = this;

        // if there is already an element in parent, replace otherwise insert within
        if (element) {
            parent.replaceChild(image, element);
        }
        else {
            parent.insertBefore(image, parent.firstChild);
        }

        if (lazyload.callback) {
            lazyload.callback(image, parent, lazyload);
        }
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * Asynchronously load images & backgrounds and apply them to DOM elements
 * @constructor
 * @param {String} src - HTMLImageElement src or background-image url to be applied
 * @param {Function} callback - Callback to fired when image is loaded
 * @property {String} src - Our local reference for the HTMLImageElement src or background-image url to be applied
 * @property {Function} callback - Our local reference for the callback to be called when image is loaded
 * @property {Object} selectors - Data attributes which dictate functionality to be applied against an HTMLElement
 * @property {String} selectors.image - Attribute to be used when an HTMLImageElement will be inserted into the DOM
 * @property {String} selectors.background - Attribute to be used when an background image will be applied to the element
 * @property {String} options - Data attribute which contains our JSON stringified HTMLElement properties to be applied
 */
export const LazyLoad = function(src, callback) {
    const lazyload = this;

    lazyload.src = src;
    lazyload.callback = callback;

    lazyload.selectors = {
        "image": "data-lazyload-image",
        "background": "data-lazyload-background"
    };

    lazyload.options = "data-lazyload-options";
};


//==================================================================================================
//	Background
//==================================================================================================
/**
 * Apply our image as a background
 * @param {HTMLElement} element - Element to which the background should be applied
 * @example
 * let lazyload = new LazyLoad("/images/product.jpg");
 * lazy.background(document.querySelector(".product__background"));
 */
LazyLoad.prototype.background = function(element) {
    const lazyload = this;

    const image = new Image();
    image.src = lazyload.src;
    image.onload = background(element, lazyload);
};


//==================================================================================================
//	Append image
//==================================================================================================
/**
 * Insert an HTMLImageElement as the first child
 * @param {HTMLElement} parent - Parent element within which our HTMLImageElement should be inserted
 * @param {Object} [options] - HTMLImageElement properties to be applied in lieu of a data-attribute
 * @example
 * let lazyload = new LazyLoad("/images/product.jpg");
 * lazy.image(document.querySelector(".product__image"));
 */
LazyLoad.prototype.image = function(parent, options) {
    const lazyload = this;
    let property;

    const image = new Image();
    image.src = lazyload.src;

    options = options ? options : JSON.parse(parent.getAttribute(lazyload.options));

    if (options) {
        for (property in options) {
            if (options.hasOwnProperty(property)) {
                image[property] = options[property];
            }
        }
    }

    image.onload = insert(image, parent, lazyload);
};


//==================================================================================================
//	Replace image
//  We assume the image has been appended
//==================================================================================================
/**
 * Replace the first child of an HTMLElement
 * We assume this first child will be the HTMLImageElement inserted via {@link LazyLoad.prototype.image}
 * @param {HTMLElement} parent - Parent element within which our HTMLImageElement should be replaced
 * @param {Object} [options] - HTMLImageElement properties to be applied in lieu of a data-attribute
 * @example
 * let lazyload = new LazyLoad("/images/product.jpg");
 * lazy.replaceImage(document.querySelector(".product__image"));
 */
LazyLoad.prototype.replaceImage = function(parent, options) {
    const lazyload = this;
    let property;

    const image = new Image();
    image.src = lazyload.src;

    options = options ? options : JSON.parse(parent.getAttribute(lazyload.options));

    if (options) {
        for (property in options) {
            if (options.hasOwnProperty(property)) {
                image[property] = options[property];
            }
        }
    }

    image.onload = replace(parent.firstChild, parent, lazyload);
};


//==================================================================================================
//	Multiple backgrounds
//==================================================================================================
/**
 * Apply backgrounds to all images on the page which contain the data-attribute specified in our object
 * @param {NodeList} [collection] - Array-like collection of nodes to which backgrounds should be applied
 * @example
 * let lazyload = new LazyLoad();
 * lazy.pageBackgrounds();
 */
LazyLoad.prototype.pageBackgrounds = function(collection) {
    const lazyload = this;

    collection = collection ? toArray(collection) : toArray(document.querySelectorAll("["+ lazyload.selectors.background +"]"));

    collection.forEach(function iterateCollection(element) {
        const image = new Image();
        image.src = "url("+ lazyload.src +")";
        image.onload = background(element, lazyload);
    });

};


//==================================================================================================
//	Multiple images
//==================================================================================================
/**
 * Apply backgrounds to all images on the page which contain the data-attribute specified in our object
 * @param {NodeList} [collection] - Array-like collection of nodes to which backgrounds should be applied
 * @param {Object} [options] - Object containing key:value pairs of HTMLImageElement properties & values
 * @example
 * let collection = document.querySelectorAll(".product");
 * let options = { "className": "product__image" };
 * let lazyload = new LazyLoad();
 * lazy.pageBackgrounds(collection, options);
 */
LazyLoad.prototype.pageAppend = function(collection, options) {
    const lazyload = this;

    collection = collection ? collection : toArray(document.querySelectorAll("["+ lazyload.selectors.image +"]"));

    collection.forEach(function iterateCollection(parent) {
        let property;
        const optionsAttribute = JSON.parse(parent.getAttribute(lazyload.options));
        const image = new Image();
        image.src = lazyload.src;

        if (optionsAttribute && options) {
            options = mergeObjects(options, optionsAttribute);
        }
        else {
            options = options ? options : JSON.parse(parent.getAttribute(lazyload.options));
        }

        if (options) {
            for (property in options) {
                if (options.hasOwnProperty(property)) {
                    image[property] = options[property];
                }
            }
        }

        image.onload = insert(parent, lazyload);
    });

};