//==================================================================================================
//
//	Tabs
//
//==================================================================================================
import { ToggleView } from "objects/shared/ToggleView";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * Tabs component, managed a list of CTA's and associated views.
 * @constructor
 * @param {NodeList} triggers - Collection of elements which will trigger a change in view
 * @param {Function} callback - Callback to be fired when a view is changed
 * @property {Array} triggers - Local instance of triggers argument converted to an {Array} from {NodeList}
 * @property {Array} views - Array of view elements which ties to our triggers collection, views are identified by the "aria-controls" attribute
 * @property {Array} toggles - Array of {@link ToggleView} instance which maps to our cta collection
 * @property {String} activeClass - Class to be used to toggle the state of our view elements. This defaults to ".hidden".
 */
export const Tabs = function(triggers, callback) {
    const tabs = this;
    let activeClass = "active";

    tabs.callback = callback;
    tabs.triggers = toArray(triggers);
    tabs.views = [];
    tabs.toggles = [];

    //TODO: Convert to Proxy when browser support is suitable
    Object.defineProperty(tabs, "activeClass", {
        get: function getActiveClass() {
            return activeClass;
        },
        set: function getActiveClass(value) {
            activeClass = value;
            tabs.triggers.forEach(function iteratePairs(cta, index) {
                tabs.toggles[index].activeClass = value;
            });
            return activeClass;
        }
    });

    tabs.init();
};


//==================================================================================================
//	Prepare our element collections
//==================================================================================================
/**
 * Initial logic for our tabs object, fired when it's instantiated.
 */
Tabs.prototype.init = function() {
    const tabs = this;

    tabs.triggers.forEach(function iteratePairs(cta, index) {
        tabs.toggles[index] = new ToggleView(cta, tabs.callback);
        tabs.views[index] = tabs.toggles[index].view;
    });

    tabs.toggles.forEach(function iterateToggles(toggle) {
        toggle.activeClass = tabs.activeClass;
    });
};


//==================================================================================================
//	Alternate state
//==================================================================================================
/**
 * Toggles the state of a particular CTA
 * @param {HTMLElement} triggers - Element who's view should be toggled
 */
Tabs.prototype.toggle = function(cta) {
    const tabs = this;
    let count = tabs.views.length;

    while(count) {
        count = count - 1;

        if (tabs.triggers[count] !== cta) {
            tabs.toggles[count].hidden = true;
        }
        else {
            if (tabs.triggers[count] === cta && tabs.toggles[count].hidden !== false) {
                tabs.toggles[count].hidden = false;
            }
            else {
                tabs.toggles[count].hidden = true;
            }
        }

    }
};
