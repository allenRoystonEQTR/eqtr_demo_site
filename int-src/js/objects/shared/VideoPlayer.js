import { typeOf } from "helpers/shared/typeOf";

//==================================================================================================
//
//	Video player
//
//==================================================================================================

//==================================================================================================
//	Constructor
//==================================================================================================
export const VideoPlayer = function(container) {
    const video = this;
    const element = container.querySelector("video");
    const sourceMP4 = element.querySelector("source[type='video/mp4'");
    const sourceWebM = element.querySelector("source[type='video/webm'");
    const sourceOgg = element.querySelector("source[type='video/ogg'");


    //  Element
    video.container = container;
    video.element = element;

    //  Context handler
    video.contextHandler = undefined;

    //  Playing
    let playing = false;
    Object.defineProperty(video, "playing", {
        get: function getPlaying() {
            return playing;
        },
        set: function setPlaying(value) {
            if (value === true || value === false) {
                if (value) {
                    video.play();
                }
                else {
                    video.pause();
                }
                playing = value;
            }
            return playing;
        }
    });


    //  Sources (MP4)
    let mp4 = (sourceMP4) ? sourceMP4.src : undefined;
    Object.defineProperty(video, "mp4", {
        get: function getMP4() {
            return mp4;
        },
        set: function setMP4(value) {
            if (typeOf(value) === "string" && sourceMP4) {
                sourceMP4.src = value;
                mp4 = value;
            }
            return mp4;
        }
    });

    //  Sources (WebM)
    let webm = (sourceWebM) ? sourceWebM.src : undefined;
    Object.defineProperty(video, "webm", {
        get: function getWebM() {
            return webm;
        },
        set: function setWebM(value) {
            if (typeOf(value) === "string" && sourceWebM) {
                sourceWebM.src = value;
                webm = value;
            }
            return webm;
        }
    });

    //  Sources (WebM)
    let ogg = (sourceOgg) ? sourceOgg.src : undefined;
    Object.defineProperty(video, "ogg", {
        get: function getOgg() {
            return ogg;
        },
        set: function setOgg(value) {
            if (typeOf(value) === "string" && sourceOgg) {
                sourceOgg.src = value;
                ogg = value;
            }
            return ogg;
        }
    });

    //  Looping
    let loop = video.element.getAttribute("loop");
    Object.defineProperty(video, "loop", {
        get: function getLoop() {
            return loop;
        },
        set: function setLoop(value) {
            if (value === true || value === false) {
                video.element.setAttribute("loop", value);
                loop = value;
            }
            return loop;
        }
    });

    //  Autoplay
    let autoplay = video.element.getAttribute("autoplay");
    Object.defineProperty(video, "autoplay", {
        get: function getAutoplay() {
            return autoplay;
        },
        set: function setAutoplay(value) {
            if (typeOf(value) === "string") {
                video.element.setAttribute("autoplay", value);
                autoplay = value;
            }
            return autoplay;
        }
    });

    //  Preloading
    let preload = video.element.getAttribute("preload");
    Object.defineProperty(video, "preload", {
        get: function getPreload() {
            return preload;
        },
        set: function setPreload(value) {
            if (value === "none" || value === "metadata" || value === "auto") {
                video.element.setAttribute("preload", value);
                preload = value;
            }
            return preload;
        }
    });

    //  Poster
    let poster = video.element.getAttribute("poster");
    Object.defineProperty(video, "poster", {
        get: function getPoster() {
            return poster;
        },
        set: function setPoster(value) {
            if (typeOf(value) === "string") {
                video.element.setAttribute("poster", value);
                poster = value;
            }
            return poster;
        }
    });
};


//==================================================================================================
//	Bind our handlers
//==================================================================================================
VideoPlayer.prototype.bind = function(listeners) {
    const video = this;

    if (typeOf(listeners === "string")) {

        if (listeners.indexOf(",") !== -1) {
            listeners.split(",").forEach(function iterateListeners(listener) {
                return video.element.addEventListener(listener, video[listener.trim()]());
            });
        }
        else {
            video.element.addEventListener(listeners, video[listeners]());
        }

    }

    return video;
};


//==================================================================================================
//	Disable context menu
//==================================================================================================
VideoPlayer.prototype.disableContextMenu = function() {
    const video = this;

    if (video.contextHandler === undefined) {

        video.contextHandler = function(event) {
            event.preventDefault();
            return false;
        };
        video.element.addEventListener("contextmenu", video.contextHandler);

        return true;
    }

    return false;
};


//==================================================================================================
//	Enable context menu
//==================================================================================================
VideoPlayer.prototype.enableContextMenu = function() {
    const video = this;

    if (video.contextHandler) {
        video.element.removeEventListener("contextmenu", video.contextHandler);
        video.contextHandler = undefined;
        return true;
    }

    return false;
};


//==================================================================================================
//	Event handlers
//==================================================================================================
VideoPlayer.prototype.seeked = function() {
    const video = this;
    return function seekListener(event) {
        video.play();
        event.stopPropagation();
    };
};

VideoPlayer.prototype.ended = function() {
    const video = this;
    return function endedListener(event) {
        video.load();
        video.playing = false;
        event.stopPropagation();
    };
};


//==================================================================================================
//	Play
//==================================================================================================
VideoPlayer.prototype.play = function() {
    const video = this;
    video.container.setAttribute("data-video-playing", true);
    return video.element.play();
};


//==================================================================================================
//	Pause
//==================================================================================================
VideoPlayer.prototype.pause = function() {
    const video = this;
    video.container.setAttribute("data-video-playing", false);
    return video.element.pause();
};


//==================================================================================================
//	Pause
//==================================================================================================
VideoPlayer.prototype.load = function() {
    const video = this;
    video.container.setAttribute("data-video-playing", false);
    return video.element.load();
};


//==================================================================================================
//	Play
//==================================================================================================
VideoPlayer.prototype.showPoster = function() {
    const video = this;

    const mp4 = video.mp4;
    const webm = video.webm;
    const ogg = video.ogg;

    if (mp4) {
        video.mp4 = "";
        video.mp4 = mp4;
    }

    if (webm) {
        video.webm = "";
        video.webm = webm;
    }

    if (ogg) {
        video.ogg = "";
        video.ogg = ogg;
    }

    return video;
};