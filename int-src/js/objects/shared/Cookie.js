//==================================================================================================
//
//	Wrapper for browser cookies
//
//==================================================================================================
/**
 * Merges the current document.cookie keys & values into our constructor as properties
 * @private
 * @function merge
 * @memberof Cookie
 * @param name - Name of cookie
 * @param value - Value of cookie
 */
function merge(name) {
    return function mergeCookie(cookie) {
        if (cookie[name] === undefined) {
            Object.defineProperty(cookie, name, {
                get: function getCookie() {
                    const cookies = document.cookie.split("; ");
                    let parts;
                    let count = cookies.length;

                    while(count) {
                        count = count - 1;
                        parts = cookies[count].split("=");
                        if (parts[0] === name) {
                            return parts[1];
                        }
                    }

                    return null;
                },
                set: function setCookie(value) {
                    const cookies = document.cookie.split("; ");
                    let parts;

                    let count = cookies.length;

                    while(count) {
                        count = count - 1;

                        parts = cookies[count].split("=");

                        if (parts[0] === name) {
                            cookie.delete(name);

                            parts[1] = value;
                            cookies[count] = parts.join("=");
                            document.cookie = cookies.join("; ");
                            return value;
                        }
                    }

                    return null;
                }
            });
        }
    };
}


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * Cookie store which is dynamically populated with current document.cookie values when instantiated
 * @constructor
 * @property {Number} expiry - Default expiry time (in days) for new cookies, this is only used if no Date() object is passed to our .add prototype
 * @property {String} path - Default path for new cookies, this is only used if no path is passed to our .add prototype
 */
export const Cookie = function() {
    const cookie = this;

    cookie.expiry = 30;
    cookie.path = "/";

    cookie.bake();
};


//==================================================================================================
//	Update cookie values
//==================================================================================================
/**
 * Merges the current cookies to our object instance
 */
Cookie.prototype.bake = function() {
    const cookie = this;
    const cookies = document.cookie.split("; ");

    cookies.forEach(function iterateCookies(entry) {
        const parts = entry.split("=");
        if (cookie[parts[0]] === undefined) {
            merge(parts[0], parts[1])(cookie);
        }
    });

    return cookie;
};


//==================================================================================================
//	Add cookie
//==================================================================================================
/**
 * Cookie store which is dynamically populated with current document.cookie values when instantiated
 * @param {String} name - Name of our cookie to be set
 * @param {String} value - Value of the cookie to be set
 * @param {Date} [expires] - Instance of Date() set to the expiry of the cookie, if not provided this is set to now + Cookie.expiry
 * @param {String} [path] - Path for the cookie, if not provided this is set to Cookie.path ("/")
 */
Cookie.prototype.add = function(name, value, expires, path) {
    const cookie = this;

    // Use default expires if none provided
    if (expires === undefined) {
        expires = new Date();
        expires.setDate(expires.getDate() + cookie.expiry);
    }

    // Use default path if none provided
    if (path === undefined) {
        path = cookie.path;
    }

    //  Set our cookie
    document.cookie = name +"="+ value +"; expires="+ expires.toUTCString() +"; path="+ cookie.path;

    //  Bake our cookie
    return cookie.bake();
};


//==================================================================================================
//	Delete cookie
//==================================================================================================
/**
 * Delete a cookie
 * @param {String} name - Name of the cookie to be deleted
 */
Cookie.prototype.delete = function(name) {
    const cookie = this;
    const expires = new Date();
    expires.setDate(expires.getDate() - 1);
    return cookie.add(name, "", expires);
};