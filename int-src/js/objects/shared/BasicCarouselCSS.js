//==================================================================================================
//
//	Basic 100% width carousel
//
//==================================================================================================
import { Carousel } from "objects/shared/Carousel";
import { CarouselTimer } from "objects/shared/CarouselTimer";
import { CarouselEndlessCSS } from "objects/shared/CarouselEndlessCSS";


//==================================================================================================
//	Constructor
//==================================================================================================
/**
 * This is a basic carousel providing 100% width transitions
 * @constructor
 * @extends Carousel
 * @param {HTMLElement} element - Container element, this contains each individual view within the carousel
 * @param {HTMLElement} next - The next button element used for binds
 * @param {HTMLElement} previous - The previous button element used for binds
 * @param {Function} [callback] - Callback fired when a carousel position change occurs
 * @property {Function} callback - Our local instance of callback parameter
 * @property {Object} buttons - Object which contains our button references
 * @property {Boolean} hasButtons - Indicates whether the carousel has button UI elements. This may not be the case for mobile exclusive carousels.
 * @property {HTMLElement} buttons.next - Our local reference to the passed next button
 * @property {HTMLElement} buttons.prev - Our local reference to the passed previous button
 * @property {Function} touch - Our instance of Hammer.js is instantiated and bound to this property
 * @example
 * let container = document.querySelector(".carousel");
 * let next = document.querySelector(".carousel__button--next");
 * let previous = document.querySelector(".carousel__button--prev");
 * let carousel = new BasicCarouselCSS(container, next, prev);
 */
export const BasicCarouselCSS = function(element, next, prev, callback) {
    const carousel = this;

    //  Inheritance
    Carousel.call(carousel, element, callback);

    //  Callback
    carousel.callback = callback;

    //  Buttons
    carousel.buttons = {
        "next": next,
        "prev": prev
    };
    carousel.hasButtons = carousel.buttons.next && carousel.buttons.prev ? true : false;

    //  Extensions
    carousel.timer = new CarouselTimer(carousel);
    carousel.endless = new CarouselEndlessCSS(carousel);

};

BasicCarouselCSS.prototype = Object.create(Carousel.prototype);
BasicCarouselCSS.prototype.constructor = BasicCarouselCSS;


//==================================================================================================
//	Animation
//==================================================================================================
/**
 * Animation method which is fired when a position change occurs
 */
BasicCarouselCSS.prototype.transform = function() {
    const carousel = this;
    carousel.container.style.transform = "translateX(-"+ carousel.goto(carousel.position) + "%)";
};

//==================================================================================================
//	Update UI state on position change
//==================================================================================================
/**
 * Default update UI method which is fired when a position change occurs
 */
BasicCarouselCSS.prototype.updateUI = function() {
    const carousel = this;

    if (carousel.hasButtons) {

        //  UI state for first carousel position
        if (carousel.position === 0) {
            carousel.buttons.prev.addClass("inactive");
        }
        else {
            carousel.buttons.prev.removeClass("inactive");
        }

        //  UI state for last carousel position
        if (carousel.position === carousel.count - 1) {
            carousel.buttons.next.addClass("inactive");
        }
        else {
            carousel.buttons.next.removeClass("inactive");
        }

    }
};


//==================================================================================================
//	Bind UI events
//==================================================================================================
/**
 * Binds events to our UI.
 * This method is invoked by our constructor, and could be treated as an init() method
 */
BasicCarouselCSS.prototype.bind = function() {
    const carousel = this;

    //  Check carousel length
    if (carousel.items.length > 1) {

        //  Bind our event handlers
        //  Next button
        carousel.buttons.next.addEventListener("click", function(event) {
            const position = carousel.position + 1;

            if (position <= carousel.last) {
                carousel.position = position;
            }

            event.preventDefault();
        });

        //  Previous button
        carousel.buttons.prev.addEventListener("click", function(event) {
            const position = carousel.position - 1;

            if (position >= 0) {
                carousel.position = position;
            }

            event.preventDefault();
        });

        carousel.touch.on("swipe", carousel.swipe(carousel));
    }

    //  If we have only 1 item in our carousel, hide the ui
    else {
        let property;
        for (property in carousel.buttons) {
            if (carousel.buttons.hasOwnProperty(property) && carousel.buttons[property]) {
                carousel.buttons[property].addClass("hidden");
            }
        }
    }

    return carousel;
};


//==================================================================================================
//	Bind touch UI events
//==================================================================================================
/**
 * Binds touch handler events to our UI.
 */
BasicCarouselJS.prototype.bindTouch = function() {
    const carousel = this;

    const touch = new Hammer(carousel.container);
    touch.on("swipe", carousel.swipe(carousel));

    return carousel;
};


//==================================================================================================
//	Bind all UI listeners
//==================================================================================================
/**
 * Binds all handlers.
 */
BasicCarouselJS.prototype.bindAll = function() {
    const carousel = this;

    carousel.bind();
    carousel.bindTouch();

    return carousel;
};


//==================================================================================================
//	Default swipe method for touch screen displays
//==================================================================================================
/**
 * Default touch event handler for direction movement
 * @returns {Function}
 */
BasicCarouselCSS.prototype.swipe = function() {
    const carousel = this;
    return function swipeEvent(event) {
        if (event.deltaX > 0 && carousel.position > 0) {
            carousel.position = carousel.position - 1;
        }
        if (event.deltaX < 0 && carousel.position < carousel.last) {
            carousel.position = carousel.position + 1;
        }
    };
};