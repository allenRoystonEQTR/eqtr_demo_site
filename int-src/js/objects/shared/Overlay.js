//==================================================================================
//
//	Overlay
//
//==================================================================================
import { toBoolean } from "helpers/shared/toBoolean";
import { toArray } from "helpers/shared/toArray";
import { keycode } from "helpers/shared/keycode";


//==================================================================================
//	Private variables
//==================================================================================
const instances = [];
const body = document.querySelector(".wrapper");


//==================================================================================
//	Private methods
//==================================================================================
function buttonClick(overlay) {
    return function buttonClickHandler(event) {
        overlay.hidden = !overlay.hidden;
        return event.preventDefault();
    };
}

function closeClick(overlay) {
    return function closeClickHandler(event) {
        overlay.hidden = true;
        return event.preventDefault();
    };
}

function closeAll() {
    instances.forEach(function overlays(overlay) {
        overlay.hidden = true;
    });
}


//==================================================================================
//	Page-level actions for accessibility
//==================================================================================
body.delegate("keyup", "*", function overlayClose(event) {
    if (event.keyCode === keycode.escape) {
        closeAll();
    }
});


//==================================================================================
//	Constructor
//==================================================================================
export const Overlay = function(button, callback) {
    const overlay = this;

    overlay.name = button.getAttribute("aria-controls");
    overlay.button = button;
    overlay.element = document.getElementById(overlay.name);
    overlay.shadow = overlay.element.querySelector("[data-overlay-shadow='"+ overlay.name +"']");
    overlay.close = toArray(overlay.element.querySelectorAll("[data-overlay-close='"+ overlay.name +"']"));

    overlay.scrollpos = undefined;

    overlay.callback = callback;

    Object.defineProperty(overlay, "hidden", {
        get: function getState() {
            return toBoolean(overlay.element.getAttribute("aria-hidden"));
        },
        set: function setState(value) {
            //  Get the current element hidden state
            const currentState = toBoolean(overlay.element.getAttribute("aria-hidden"));

            //  Update our element attributes as required
            if (currentState !== value) {

                //  Show or hide our element
                overlay.element.setAttribute("aria-hidden", !currentState);

                //  Fire our callback if it exists
                if (overlay.callback) {
                    overlay.callback(overlay);
                }

                //  Update our page
                overlay.pageUpdate(value);

            }

            return !currentState;
        }
    });

};


//==================================================================================
//	Bind our event and initialise any other logic
//==================================================================================
Overlay.prototype.bind = function() {
    const overlay = this;

    instances.push(overlay);

    overlay.button.addEventListener("click", buttonClick(overlay));
    overlay.close.forEach(function iterateClose(element) {
        element.addEventListener("click", closeClick(overlay));
    });

    return overlay;
};


//==================================================================================
//	Update our page when the overlay is shown or hidden
//==================================================================================
Overlay.prototype.pageUpdate = function(hidden) {
    const overlay = this;

    //  Prevent the body element becoming scrollable & reset our scroll position
    if (hidden === false) {
        overlay.scrollpos = document.documentElement.scrollTop;
        body.addClass("no-scroll");
    }
    else {
        body.removeClass("no-scroll");
        document.documentElement.scrollTop = overlay.scrollpos;
    }

};