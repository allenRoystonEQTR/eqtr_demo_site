//==================================================================================================
//
//	Tooltips
//
//==================================================================================================
import { ToggleView } from "objects/shared/ToggleView";
import { keycode } from "helpers/shared/keycode";
import { toArray } from "helpers/shared/toArray";


//==================================================================================================
//	Private variables
//==================================================================================================
const instances = [];
const body = document.querySelector(".wrapper");


//==================================================================================================
//	Private methods
//==================================================================================================
function closeAll() {
    instances.forEach(function overlays(tooltips) {
        return tooltips.closeAll();
    });
}

function update(tooltips, name, state) {
    tooltips.toggles[name].hidden = state;
    tooltips.containers[name].setAttribute(tooltips.activeAttribute, state);
}


//==================================================================================================
//	Delegated events for accessibility
//==================================================================================================
body.delegate("keyup", "*", function tooltipClose(event) {
    if (event.keyCode === keycode.escape) {
        closeAll();
    }
});
body.delegate("click", "*", function tooltipClose() {
    closeAll();
});


//==================================================================================================
//	Constructor
//==================================================================================================
export const Tooltips = function(parent, callback) {
    const tooltips = this;
    let current = "";

    tooltips.parent = parent;

    tooltips.selector = "data-tooltip";
    tooltips.activeAttribute = "data-tooltip-hidden";

    tooltips.callback = callback;

    //  tooltips.selector.value => {HTMLElement}
    tooltips.containers = {};

    //  tooltips.selector.value => {ToggleView}
    tooltips.toggles = {};

    //TODO: Convert to Proxy when browser support is suitable
    Object.defineProperty(tooltips, "current", {
        get: function getActive() {
            return current;
        },
        set: function setActive(key) {
            //  If the current tooltip isn't the one to be shown
            if (current !== key) {

                //  Show our new tooltip
                if (tooltips.toggles[key]) {
                    update(tooltips, key, false);
                }

                //  Close the old tooltip
                if (tooltips.toggles[current]) {
                    update(tooltips, current, true);
                }

                //  Update our state value
                current = key;
            }

            //  If the current tooltip is the same as the one being actived
            else if (current === key) {

                //  Close the tooltip
                if (tooltips.toggles[current]) {
                    update(tooltips, current, true);
                }

                //  Reset our state value
                current = "";
            }

            if (tooltips.callback) {
                tooltips.callback(tooltips);
            }

            return current;
        }
    });

    instances.push(tooltips);
};


// ==================================================================================
// 	Event binds
// ==================================================================================
Tooltips.prototype.bind = function() {
    const tooltips = this;

    tooltips.parent.delegate("click", "*", tooltips.delegate());

    return tooltips.init();
};

// ==================================================================================
// 	Event binds
// ==================================================================================
Tooltips.prototype.init = function() {
    const tooltips = this;
    const collection = toArray(tooltips.parent.querySelectorAll("["+ tooltips.selector +"]"));

    collection.forEach(function iterateCollection(element) {
        const attribute = element.getAttribute(tooltips.selector);

        tooltips.containers[attribute] = element;
        tooltips.toggles[attribute] = new ToggleView(element.querySelector("[aria-controls]"));
    });

    return tooltips;
};


// ==================================================================================
// 	Delegate listener
// ==================================================================================
Tooltips.prototype.delegate = function() {
    const tooltips = this;

    return function tooltipsDelegate(event) {
        const parent = event.target.firstParent("["+ tooltips.selector +"]");
        if (parent) {
            const attribute = parent.getAttribute(tooltips.selector);

            // Check if navigation is open.
            const navIsOpen = document.querySelector('[data-navigation-hidden="false"]');
            if(navIsOpen !== null){
                const navButton = document.querySelector('#trigger-nav-burger-menu');
                navButton.click();
            }


            //  Note: Only the tooltip trigger can close the tooltip. This allows interaction within child elements.
            //  If the trigger for our active tooltip has been clicked, close the tooltip
            if (attribute === tooltips.current && event.target === tooltips.toggles[attribute].trigger) {
                tooltips.current = attribute;
            }

            //  If a different tooltip has been clicked, open that tooltip
            else if (attribute !== tooltips.current) {
                tooltips.current = parent.getAttribute(tooltips.selector);
            }

        }
        else {
            tooltips.current = "";
        }

        event.stopPropagation();
    };
};


// ==================================================================================
// 	Close all tooltips
// ==================================================================================
Tooltips.prototype.closeAll = function() {
    const tooltips = this;

    Object.keys(tooltips.toggles).forEach(function (key) {
        if (tooltips.toggles[key].hidden === false) {
            tooltips.toggles[key].hidden = true;
        }
    });

    return tooltips.current = "";
};