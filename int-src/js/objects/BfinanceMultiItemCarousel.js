//==================================================================================================
//
//	BFinance Basic carousel
//
//==================================================================================================
import { BasicCarouselJS } from "objects/shared/BasicCarouselJS";


//==================================================================================================
//	Constructor
//==================================================================================================
export const BfinanceMultiItemCarousel = function(element, next, prev) {
    const carousel = this;
    BasicCarouselJS.call(carousel, element, next, prev);

    carousel.visible = 1;

    //  Initialise our carousel widths using centering (offset)
    carousel.setWidths();
};
BfinanceMultiItemCarousel.prototype = Object.create(BasicCarouselJS.prototype);
BfinanceMultiItemCarousel.prototype.constructor = BfinanceMultiItemCarousel;


// ==================================================================================================
// 	Center transition distance for all carousel items apart from the first
// ==================================================================================================
BfinanceMultiItemCarousel.prototype.gotoCenter = function(position) {
    const carousel = this;
    const indent = carousel.calculateItemWidth() * carousel.offset;
    const centered = ((indent / 2) * -1);

    if (carousel.position === 0) {
        return carousel.goto(position);
    }
    else {
        return carousel.goto(position) + centered;
    }
};


//==================================================================================================
//	Private methods
//==================================================================================================
BfinanceMultiItemCarousel.prototype.transform = function(duration) {
    const carousel = this;

    duration = (duration !== undefined) ? duration : 200;
    carousel.anime({
        targets: carousel.container,
        easing: "easeOutQuint",
        duration: (duration !== undefined) ? duration : 200,
        translateX: [
            - carousel.gotoCenter(carousel.previous) + "%",
            - carousel.gotoCenter(carousel.position) + "%"
        ]
    });
};
