//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
let fs = require("fs");
let urijs = require("urijs");
let CrawlKit = require('crawlkit');
let GenericAnchors = require('crawlkit/finders/genericAnchors');
let AccessibilityTest = require("crawlkit-runner-accessibility-developer-tools");


//  ============================================================================================================
//  Arguments
//  ============================================================================================================
let website = "https://eqtr.com";


//  ============================================================================================================
//  Helpers
//  ============================================================================================================
let LinkFinder = function() {};
LinkFinder.prototype.getRunnable = function() { return GenericAnchors; };
LinkFinder.prototype.urlFilter = function(url) {
  if (urijs(url).domain() !== urijs(website).domain()) {
    return false;
  }
  else {
    return url;
  }
};


//  ============================================================================================================
//  Crawler
//  ============================================================================================================

let crawler = new CrawlKit(website);
crawler.addRunner("accessibility", new AccessibilityTest());
crawler.setFinder(new LinkFinder());
crawler.crawl()
  .then(
      function output(results) {
          return fs.writeFile(
              "./crawlkit_results_"+ Date.now() +".json",
              JSON.stringify(results, true, 2),
              function writeError(error) {
                  return console.log(error);
              }
          );
      },
      function error(error) {
          return console.log(error);
      }
  );