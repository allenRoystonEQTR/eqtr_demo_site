const http = require("http");

const port = 8080;
const host = "127.0.0.1";

const server = http.createServer(function(req, res) {

    if (req.method == "POST") {
    	let incoming = "";

        req.on("data", function(data) {
           incoming = incoming + data;
        });

        req.on("end", function() {
	        res.writeHead(200, {
	        	"Content-Type": req.headers["content-type"],
                "Access-Control-Allow-Methods": "GET, POST, OPTION",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "Content-Type"
	        });
        	res.end(incoming)
        });

    }

    else {
        res.writeHead(200, {
        	"Content-Type": (req.headers["content-type"]) ? req.headers["content-type"] : "text/html;charset=UTF-8",
            "Access-Control-Allow-Methods": "GET, POST, OPTION",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Content-Type"
        });
        res.end("200");
    }

});

server.listen(port, host);
console.log("//===========================================================================");
console.log("//  STARTING ECHO SERVER ON "+ host +":"+ port);
console.log("//===========================================================================");