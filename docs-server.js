var connect = require("connect"),
	serve_static = require("serve-static"),
	logger = require("morgan");
	//	compression = require("compression");

var directory = "int-src/documentation/js/shared";

	console.log("- Creating server instance.");
var server = connect(),
	port = 8080;

	console.log("- Attaching logger.");
	server.use(logger("combined"));

	// console.log("- Using file compression.");
	// server.use(compression({
	// 		level: 9,
	// 		threshold: 128,
	// 		filter: function(req) {
	// 			if (req.headers["accept-encoding"].indexOf("gzip") !== -1) {
	// 				return true;
	// 			}
	// 		}
	// }));

	console.log("- Static content: ./"+ directory);
	server.use(serve_static(directory));

	console.log("- Starting server on port "+ port);
	server.listen(port);