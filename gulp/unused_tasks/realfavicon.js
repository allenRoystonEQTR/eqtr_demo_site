//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

    var gulp = require("gulp");

	var fs = require("fs");
	var realFavicon = require ("gulp-real-favicon");


//  ============================================================================================================
//  Tasks
//  ============================================================================================================

	// Generate the icons. This task takes a few seconds to complete.
	// You should run it at least once to create the icons. Then,
	// you should run it whenever RealFaviconGenerator updates its
	// package (see the check-for-favicon-update task below).
	gulp.task("realfavicon-prepare", function(complete) {
		realFavicon.generateFavicon({
			masterPicture: "int-src/images/favicon/favicon.svg",
			dest: "dist",
			iconsPath: "/",
			design: {
				ios: {
					pictureAspect: "noChange",
					assets: {
						ios6AndPriorIcons: false,
						ios7AndLaterIcons: true,
						precomposedIcons: false,
						declareOnlyDefaultIcon: true
					}
				},
				desktopBrowser: {},
				windows: {
					pictureAspect: "noChange",
					backgroundColor: "#ffffff",
					onConflict: "override",
					assets: {
						windows80Ie10Tile: false,
						windows10Ie11EdgeTiles: {
							small: false,
							medium: true,
							big: false,
							rectangle: false
						}
					}
				},
				androidChrome: {
					pictureAspect: "noChange",
					themeColor: "#ffffff",
					manifest: {
						name: "BuildTools",
						display: "browser",
						orientation: "notSet",
						onConflict: "override",
						declared: true
					},
					assets: {
						legacyIcon: false,
						lowResolutionIcons: false
					}
				},
				safariPinnedTab: {
					pictureAspect: "silhouette",
					themeColor: "#ffffff"
				}
			},
			settings: {
				scalingAlgorithm: "Mitchell",
				errorOnImageTooSmall: false
			},
			markupFile: "int-src/images/favicon/faviconData.json"
		}, function() {
			complete();
		});
	});

	// Inject the favicon markups in your HTML pages. You should run
	// this task whenever you modify a page. You can keep this task
	// as is or refactor your existing HTML pipeline.
	gulp.task("realfavicon", ["realfavicon-prepare"], function() {
		gulp.src([ "int-src/images/favicon/*.hbs" ])
			.pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync("int-src/images/favicon/faviconData.json")).favicon.html_code))
			.pipe(gulp.dest("int-src/html/components/favicon"));
	});

	// Check for updates on RealFaviconGenerator (think: Apple has just
	// released a new Touch icon along with the latest version of iOS).
	// Run this task from time to time. Ideally, make it part of your
	// continuous integration system.
	// gulp.task("check-for-favicon-update", function(done) {
	// 	var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
	// 	realFavicon.checkForUpdates(currentVersion, function(err) {
	// 		if (err) {
	// 			throw err;
	// 		}
	// 	});
	// });