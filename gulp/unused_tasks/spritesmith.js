//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

    var gulp = require("gulp");

    var buffer = require("vinyl-buffer");
    var merge = require("merge-stream");

    var spritesmith = require("gulp.spritesmith");
    //var imagemin = require("gulp-imagemin");
    //var csso = require("gulp-csso");



//  ============================================================================================================
//  Tasks
//  ============================================================================================================

      gulp.task("spritesmith", function () {

        var output = gulp.src("int-src/images/spritesmith/files/*.png")
            .pipe(spritesmith({
              "padding": 5,
              "imgName": "sprite.png",
              "imgPath": "../assets/images/sprite.png",
              "cssName": "sprite.css"
            }));

        var image = output.img
          .pipe(buffer())
          //.pipe(imagemin([imagemin.gifsicle(), imagemin.jpegtran(), imagemin.optipng(), imagemin.svgo()], { "verbose": true }))
          .pipe(gulp.dest("int-dist/assets/images"));

        var css = output.css
          //.pipe(csso())
          .pipe(gulp.dest("int-src/css/generated"));

        return merge(image, css);

      });