//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

    var gulp = require("gulp");
    var gutil = require("gulp-util");
    var concat = require("gulp-concat");

    var sass = require("gulp-sass");
    var csso = require("gulp-csso");
    var sourcemaps = require("gulp-sourcemaps");

    var postcss = require("gulp-postcss");    
    var autoprefixer = require("autoprefixer");

    var newer = require("gulp-newer");


    //  Custom error logging
    function LOG_ERROR(error) {
        if (error.codeFrame) {
            console.log("\n");
            console.log(error.codeFrame);
            console.log("\n");
            console.log(error.loc);
            console.log(error.filename);
            console.log(error.SyntaxError);
            console.log("\n");
        }
        else {
            console.log(error);
        }
        //  Important for Browserify
        this.emit("end");
    }


    //  Check environment
    var isProduction = (gutil.env.prod);

    //  Clean on --prod
    var preRequisites = (isProduction) ? ["cleanCSS"] : undefined;

    //  PostCSS tasks
    var processors = [
        autoprefixer({ "browsers": ["last 2 versions", "ie >= 9"]} )
    ];

    //  SASS Priority
    var priority = [
      "int-src/css/*.scss",
      "int-src/css/core/**/*.scss",
      "int-src/css/native-elements/**/*.scss",
      "int-src/css/components/**/*.scss",
      "int-src/css/generated/**/*.scss", "int-src/css/generated/**/*.css"
    ];


//  ============================================================================================================
//  Tasks
//  ============================================================================================================

    //  During --prod all file checking & caching is disabled
    gulp.task("sass", preRequisites, function() {
        return gulp.src(["int-src/css/sheets/*.scss"])
          .pipe(isProduction ? gutil.noop() : newer("sass"))
          .pipe(sass({
            "sourceMaps": "none",
            "precision": 10
          })
          .on("error", sass.logError))
          .pipe(postcss(processors)).on("error", LOG_ERROR)
          .pipe(isProduction ? sourcemaps.init({ "loadMaps": false }) : gutil.noop())
          .pipe(isProduction ? csso() : gutil.noop()).on("error", LOG_ERROR)
          .pipe(isProduction ? sourcemaps.write("./", { "includeContent": true }) : gutil.noop())
          .pipe(gulp.dest("int-dist/assets/css"));
    });
