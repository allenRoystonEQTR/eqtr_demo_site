//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

    var gulp = require("gulp");

    var svgSprite = require("gulp-svg-sprite");



//  ============================================================================================================
//  Tasks
//  ============================================================================================================
    gulp.task("svg-sprite", function() {

      //  Tweak dest paths

      return gulp.src("int-src/images/svg-store/files/*.svg")
        .pipe(svgSprite({
          "dest": ".",
          "log": null,
          "shape": {
            "spacing": {
              "padding": 1,
              "box": "content"
            },
            "transform": ["svgo"],
            "meta": null,
            "align": null
          },
          "svg": {
            "xmlDeclaration": true,
            "doctypeDeclaration": true,
            "namespaceIDs": true,
            "dimensionAttributes": true
          },
          "mode": {
          "css": {
              "dest": ".",
              "bust": false,
              "sprite": "assets/images/svg-sprite.svg",
              "render": {
                "scss": {
                  "dest": "../int-src/css/site/generated/svg-sprite",
                  "template": "int-src/images/svg-store/svg-sprite_template.scss"
                }
              },
              "dimensions": true,
              "common": "svg-icon"              
            }
          }
      }))
      .on("error", console.log)
      .pipe(gulp.dest("int-dist"));

  });