//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

	import gulp from "gulp";
	import del from "del";


//  ============================================================================================================
//  Tasks
//  ============================================================================================================

	gulp.task("clean:html", function cleanHTML() {
		return del([
			"int-dist/**/*.html"
		]);
	});

	gulp.task("clean:css", function cleanCSS() {
		return del([
			"int-dist/assets/css/**.css",
			"int-dist/assets/css/**.css.map"
		]);
	});

	gulp.task("clean:js", function cleanJS() {
		return del([
			"int-dist/assets/js/**.js",
			"int-dist/assets/js/**.js.map"
		]);
	});


	gulp.task("clean:docs:js:shared", function cleanJS() {
		return del([
			"int-src/documentation/js/shared"
		]);
	});

	gulp.task("clean:docs:html", function cleanJS() {
		return del([
			"int-src/documentation/css"
		]);
	});