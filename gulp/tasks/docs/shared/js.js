// //  ============================================================================================================
// //  Dependencies
// //  ============================================================================================================

// // import gulp from "gulp";
// // import jsdoc from "gulp-jsdoc3";


// //  ============================================================================================================
// //  Tasks
// //  ============================================================================================================

//     let shared = {
//         "tags"      : {
//             "allowUnknownTags" : true
//         },
//         "plugins": [
//             "plugins/markdown"
//         ],
//         "source"    : {
//           "include" : [
//             "./int-src/documentation/js/shared.md",
//             "./int-src/js/**/*.*"
//           ],
//           "excludePattern": "(node_modules/)"
//         },
//         "templates" : {
//             "name": "Equator Front-end Component Library",
//             "cleverLinks": false,
//             "monospaceLinks": false,
//             "dateFormat": "ddd MMM Do YYYY",
//             "outputSourceFiles": true,
//             "outputSourcePath": true,
//             "systemName": "Equator Front-end Component Library",
//             "footer": "",
//             "copyright": "<p><a href='https://www.eqtr.com' title='Equator Scotland Ltd.'>Equator Scotland Ltd.</a></p>",
//             "navType": "vertical",
//             // //  Theme
//             // "theme": "cosmo",
//             "linenums": true,
//             "collapseSymbols": false,
//             "inverseNav": true
//         },
//         "markdown": {
//             "parser": "gfm",
//             "hardwrap": true
//         },
//         "opts": {
//             "private": true,
//             "recurse": true,
//             "lenient": true,
//             "destination": "./int-src/documentation/js/shared",
//             //  3rd party theme
//             "template": "node_modules/postman-jsdoc-theme/tmpl"
//         }
//     };

//     // // Compile Scripts
//     // gulp.task("docs:js:atlas", ["clean:docs:js:atlas"], function documentScripts(complete) {
//     //     gulp.src(
//     //         [
//     //             "./int-src/js/externs.js",
//     //             "./int-src/js/**/atlas/*.js"
//     //         ],
//     //         { "read": false }
//     //     )
//     //     .pipe(jsdoc(atlasSettings))
//     //     .on("end", complete);
//     // });