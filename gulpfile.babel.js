//  ============================================================================================================
//  Task imports
//  ============================================================================================================

import gulp from "gulp";
import require from "require-dir";

// Grab Relevent Tasks
require("./gulp/tasks/", { recurse: true });


//  ============================================================================================================
//  Clean
//  ============================================================================================================
gulp.task("clean", ["clean:html", "clean:css", "clean:js"]);


//  ============================================================================================================
//  Default
//  ============================================================================================================
gulp.task("default", ["assemble", "sass", "symbols:web", "images:svg", "images:raster", "videos", "browserify"]);


//  ============================================================================================================
//  Development
//  ============================================================================================================
gulp.task("dev", ["default"], function defaultTask() {
	gulp.start("watch");
});


//  ============================================================================================================
//  Documentation
//  ============================================================================================================
gulp.task("docs:build", ["docs:js:shared", "docs:html"]);

gulp.task("docs:dev", ["docs:build"], function docsTask() {
	gulp.start("watch:docs");
});



//  ============================================================================================================
//  Watch tasks
//  ============================================================================================================
gulp.task("watch", ["browsersync"], function watch() {
	gulp.watch("int-src/**/*.hbs", ["watch:assemble"]);
	gulp.watch("int-src/scss/**/*.scss", ["watch:sass"]);
	gulp.watch("int-src/js/**/*.js", ["watch:browserify"]);
});

gulp.task("watch:docs", ["browsersync"], function watch() {
	gulp.watch("int-src/**/*.hbs", ["watch:docs:html"]);
	gulp.watch("int-src/scss/**/*.scss", ["watch:docs:html"]);
	gulp.watch("int-src/js/**/*.js", ["watch:docs:js:shared"]);
});