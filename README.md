# EQTR DEMO SITE
This is the front-end repository for the EQTR Demo site, based off the components found in the BFinance project.  Note that
bugs that are present in BFinance are most likely here too.

### Preview
![Snapshot 1](/preview/one.png)
![Snapshot 2](/preview/two.png)
![Snapshot 3](/preview/three.png)

### Setup
``` javascript
$ git clone https://allenRoystonEQTR@bitbucket.org/allenRoystonEQTR/eqtr_demo_site.git
$ cd eqtr_demo_site
$ npm install
$ npm run build
$ npm run dev
```

### Documentation
None (yet)


### Notes
There are some bugs in the carousal functionality that carried over to this repo.  I haven't had time to patch them up,
but they have been removed/hidden from the most incompatiable components.


###  Testing
None (yet)


### Dependencies
* NodeJS
* Gulp
* Sense of humor


### Original Site
* Graeme Clark
* Panagiotis Baralidis
* Crawford Jolly
* Rob Foxx


### Conversion by
* Allen Royston